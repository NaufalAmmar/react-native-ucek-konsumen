export const component = {
    state: {
        root: null,
        dashboard: null,
        beranda: null,
        riwayat: null,
        profile: null,
        riwayatSaya: null,
        list_tagihan: null,
        user: null,
        sign_in: null,
        order: null,
        location: null,
    },
    reducers: {
        setRoot(state, payload) {
            state.root = payload;
            return state;
        },
        setBeranda(state, payload) {
            state.beranda = payload;
            return state;
        },
        setDashboard(state, payload) {
            state.dashboard = payload;
            return state;
        },
        
        setProfile(state, payload) {
            state.profile = payload;
            return state;
        },
        setListTagihan(state, payload) {
            state.list_tagihan = payload;
            return state;
        },
        setRiwayat(state, payload) {
            state.riwayat = payload;
            return state;
        },
        setRiwayatSaya(state, payload) {
            state.riwayatSaya = payload;
            return state;
        },
        setUser(state, payload) {
            state.user = payload;
            return state;
        },
        setSignIn(state, payload) {
            state.sign_in = payload;
            return state;
        },
        setOrder(state, payload) {
            state.order = payload;
            return state;
        },
        setLocation(state, payload) {
            state.location = payload;
            return state;
        },
     
    },
    effects: {}
}