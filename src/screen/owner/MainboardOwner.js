import { dispatch as Redux } from '@rematch/core';
import React, { Component } from 'react';
import { Dimensions, StyleSheet, View } from 'react-native';
import { connect } from 'react-redux';
import Placeholder from 'rn-placeholder';
import Header from '../component/Header';
import FooterTabOwner from '../component/FooterTabOwner';
import Color from '../utility/Color.js';
import Rupiah from '../utility/Rupiah.js';
import Styles from '../utility/Style.js';

const { width } = Dimensions.get('window')
class MainboardOwner extends Component {
    static navigationOptions = {
        header: null
    };
    constructor(props) {
        super(props)
        this.root = this.props.component.root;
        this.user = this.props.component.user;
        this.state = {
            isLoading: false,
        }
        Redux.component.setBeranda(this);

    }
    _refresh() {
        this.componentDidMount();
        // this.setState({ isLoading: true });
    }
    componentDidMount() {

    }
   
    placeholder() {
        let content = [{}, {}, {}]
        return (
            <View style={{ marginHorizontal: 20, marginVertical: 10 }}>
                <Placeholder.Line
                    animate="fade"
                    width="40%"
                    height={30}
                    color={Color.placeholder}
                />
                <Placeholder.Box
                    animate="fade"
                    height={170}
                    width="100%"
                    // radius={5}
                    color={Color.placeholder}
                    style={{ marginVertical: 5 }}
                />
                {content.map((i, index) => {
                    return (
                        <View key={index} style={{ marginVertical: 10 }}>
                            <Placeholder.Box
                                animate="fade"
                                height={50}
                                width="100%"
                                // radius={5}
                                color={Color.placeholder}
                                style={{ marginVertical: 5 }}
                            />
                        </View>
                    )
                })}
                <Placeholder.Line
                    animate="fade"
                    width="40%"
                    height={30}
                    color={Color.placeholder}
                />
                <Placeholder.Box
                    animate="fade"
                    height={150}
                    width="100%"
                    // radius={5}
                    color={Color.placeholder}
                    style={{ marginVertical: 5 }}
                />
            </View>
        )
    }

    render() {
        return (
            <View style={Styles.container}>
                <FooterTabOwner />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    wrapper: {
        // backgroundColor: Color.black,
        borderBottomColor: Color.primary, borderBottomWidth: 1
    },
    slide: {
        // flex: 1,
        // justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: Color.greenFill
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold'
    },

    image: {
        width, height: width / 2.5,
        // borderWidth:1, borderColor:Color.primary
    }
})
function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(MainboardOwner);