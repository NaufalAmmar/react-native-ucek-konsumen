import Geocoder from '@arkana/react-native-geocoder';
import Geolocation from '@react-native-community/geolocation';
import { dispatch as Redux } from '@rematch/core';
import moment from 'moment';
import React, { Component } from 'react';
import { Alert, Image, AsyncStorage, BackHandler, DeviceEventEmitter, Dimensions, PermissionsAndroid, RefreshControl, ScrollView, StyleSheet, Text, View } from 'react-native';
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";
import { withNavigation } from 'react-navigation';
import { connect } from 'react-redux';
import Header from '../component/Header';
import ImageContent from '../component/ImageContent';
import LoadingImage from '../component/LoadingImage';
import ModalIntro from '../component/ModalIntro';
import NoData from '../component/NoData';
import Color from '../utility/Color.js';
import Styles from '../utility/Style.js';
import Url from '../utility/Url.js';
import Rupiah from '../utility/Rupiah';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import ScrollHorizontalImage from '../component/ScrollHorizontalImage';
import LinearGradient from 'react-native-linear-gradient';
import { TouchableHighlight, TouchableOpacity } from 'react-native-gesture-handler';
const { width, height } = Dimensions.get('window')


class Dashboard extends Component {
    static navigationOptions = {
        header: null
    };
    constructor(props) {
        super(props)
        this.watchID = null
        this.root = this.props.component.root;
        this.user = this.props.component.user;
        // this.userData = this.user.user;
        this.userLocation = this.props.component.location;
        this.state = {
            isLoading: false,
            modalIntro: false,
            contentLoading: false,
            slides: [],
            profile: this.userData ? this.userData : { name: '', photo_url: '', rating: '' },
            alamatGPS: 'Mencari ..',
            region: {
                lat: -37.78825,
                lng: -122.4324,
            },
            news: [],
            menu: [
                { id: 0, image: require('../../../img/atur_layanan.png'), action: () => this.root.navigate('DataLayanan'), title: 'Atur Layanan', description: 'Atur layanan tiap cabang', color: Color.primary },
                { id: 1, image: require('../../../img/kasir.png'), action: () => this.root.navigate('KasirOwner'), title: 'Kasir', description: 'Fitur kasir tiap cabang', color: Color.primary },
                { id: 1, image: require('../../../img/wash_machine.png'), action: () => this.root.navigate('DataCabang'), title: 'Data Cabang', description: 'Atur cabang yang kamu miliki', color: Color.primary },
                { id: 1, image: require('../../../img/kurir.png'), action: () => this.root.navigate('TambahKurir'), title: 'Tambah Kurir', description: 'Tambah kurir di cabang kamu', color: Color.primary },
                { id: 1, image: require('../../../img/calendar.png'), action: () => this.root.navigate('AturHariLibur'), title: 'Atur Hari Libur', description: 'Hari libur tiap cabang', color: Color.primary },
                { id: 1, image: require('../../../img/atur_jadwal.png'), action: () => this.root.navigate('ListLaundry'), title: 'Atur Jam Operasional', description: 'Jam operasional tiap cabang', color: Color.primary },
            ],
            dataIntro: [
                {
                    step: 1,
                    height: 50,
                    topPosition: 0,
                    description: "Disini kamu bisa cari ujian yang kamu cari. "

                },
                {
                    step: 2,
                    height: width / 2.5,
                    topPosition: 50,
                    description: "Disini kamu bisa lihat berita Cukurin Education terbaru. "

                },
                {
                    step: 3,
                    height: 60,
                    topPosition: (width / 2.5) + 50 + 70,
                    description: "Jika mau Top Up atau Berlangganan klik disini. "

                },
                {
                    step: 4,
                    height: 80,
                    topPosition: (width / 2.5) + 50 + 70 + 70,
                    description: "Pilih Kategori ujian ZENITO."

                },
                {
                    step: 5,
                    height: 60,
                    topPosition: (width / 2.5) + 50 + 80 + 80 + 60,
                    description: "Pilih instansi ujian, disini adalah pilihan setelah kamu pilih kategori ujian 'ZENITO' diatas. "

                },
                {
                    step: 6,
                    height: 80,
                    topPosition: (width / 2.5) + 50 + 80 + 80 + 70 + 60,
                    description: "Pilih Paket yang kamu inginkan. "
                },
            ],
            defaultImage: require('../../../img/default_empty.png'),
        }
        Redux.component.setDashboard(this);

    }
    _refresh() {
        // this.setState({ isLoading: true, })
        // this.getProfile(this.userData.token);
        // this.getBerita();
        // this.loadGPS();
    }
    componentDidMount() {
        // if (this.userData.token == null) {
        //     this.root.forceOut()
        // }
        this.loadGPS()
        this._refresh();
        const { navigation } = this.props;
        this.focusListener = navigation.addListener('didFocus', () => {
            this.loadGPS()
            this._refresh()
        });
    }
    componentWillUnmount() {
        this.focusListener.remove();
    }

    getBerita() {
        Url.listBerita().then((response) => {
            let data = response.data.data;
            if (data != null && data.length >= 1) {
                this.setState({ news: data.berita });
            }
            this.setState({ isLoading: false });
            console.log('data get berita:', data);
        }).catch((error) => {
            // console.log(error, error.response);
            if (error.response == null) {
                this.root.message("Network Error.", 'warning')
                this.setState({ isLoading: false });
            }
            this.setState({ isLoading: false });
            this.root.message(error.response.data.message, 'warning');
        });
    }
    getProfile(token) {
        Url.getProfile(token).then((response) => {
            let data = response.data.data;
            Redux.component.setUser(data);
            this.setState({ profile: data.user, isLoading: false });
            let dataString = JSON.stringify(data)
            if (dataString.length > 1) {
                this.asyncProfile(dataString)
            }
            // console.log('data get profile:', data);
            let beranda = this.props.component.beranda;
            let saldo = data.user != null ? data.user.saldo : 0
            let phone = data.user != null ? data.user.phone : null
            if (beranda != null) {
                beranda.setSaldo(saldo)
            }
            this.cekDataDiri(phone, saldo)
        }).catch((error) => {
            // console.log(error, error.response);
            if (error.response == null) {
                this.root.message("Network Error.", 'warning')
                this.setState({ isLoading: false });
            } else {
                AsyncStorage.setItem('Capster', '');
            }
            this.setState({ isLoading: false });
            this.root.message(error.response.data.message, 'warning');
        });
    }
    async asyncProfile(dataString) {
        try {
            await AsyncStorage.setItem('Capster', dataString);
        } catch (error) {
            AsyncStorage.setItem('Capster', '');
            // Error saving data
            // console.log('Error Ternyata', error)
        }
    }
    componentWillUnmount() {
        Geolocation.clearWatch(this.watchID)
        LocationServicesDialogBox.stopListener(); // Stop the "locationProviderStatusChange" listener
    }
    loadGPS() {
        if (Platform.OS === 'android')
            LocationServicesDialogBox.checkLocationServicesIsEnabled({
                message: "<h2 style='color: #0af13e'>Use Location ?</h2>Mi Burung Dara wants to change your device settings:<br/><br/>Use GPS, Wi-Fi, and cell network for location<br/><br/>Please enable high accurate<br/><br/><a href='#'>Learn more</a>",
                ok: "YES",
                cancel: "NO",
                enableHighAccuracy: false, // true => GPS AND NETWORK PROVIDER, false => GPS OR NETWORK PROVIDER
                showDialog: true, // false => Opens the Location access page directly
                openLocationServices: true, // false => Directly catch method is called if location services are turned off
                preventOutSideTouch: false, // true => To prevent the location services window from closing when it is clicked outside
                preventBackClick: false, // true => To prevent the location services popup from closing when it is clicked back button
                providerListener: true // true ==> Trigger locationProviderStatusChange listener when the location state changes
            }).then((success) => {
                // console.log(success); // success => {alreadyEnabled: false, enabled: true, status: "enabled"}
                this.getCurrentPosition()
                this.requestLocationPermission();
            }).catch((error) => {
                // console.log(error.message); // error.message => "disabled"
            });

        BackHandler.addEventListener('hardwareBackPress', () => { //(optional) you can use it if you need it
            //do not use this method if you are using navigation."preventBackClick: false" is already doing the same thing.
            LocationServicesDialogBox.forceCloseDialog();
        });

        DeviceEventEmitter.addListener('locationProviderStatusChange', function (status) { // only trigger when "providerListener" is enabled
            // console.log(status); //  status => {enabled: false, status: "disabled"} or {enabled: true, status: "enabled"}
        });

    }
    async requestLocationPermission() {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                    'title': 'Location Permission',
                    'message': 'Mi Burung Dara needs access to your location'
                }
            )

            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                this.getCurrentPosition()
                // console.log("Location permission granted")
            } else {
                // console.log("Location permission denied")
            }
        } catch (err) {
            console.warn(err)
        }
    }
    getCurrentPosition() {
        try {
            Geolocation.getCurrentPosition(
                (position) => {
                    // console.log(position)
                    const region = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude,
                    };
                    Redux.component.setLocation({ region })
                    this.setState({ region })
                    // console.log(region)
                    this.getAddress(region)
                },
                (error) => {
                    // console.log(error)
                    //TODO: better design
                    switch (error.code) {
                        case 1:
                            if (Platform.OS === "ios") {
                                // Alert.alert("Peringatan", "Mohon cek izin Aplikasi di pengaturan.");
                            } else {
                                // Alert.alert("Peringatan", "Mohon cek izin Aplikasi di pengaturan.");
                            }
                            break;
                        default:
                        // Alert.alert("Peringatan", "Mohon cek izin Aplikasi di pengaturan.");
                    }
                },
                { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
            );
        } catch (e) {
            alert(e.message || "");
        }
    };
    getAddress() {
        let { region } = this.state
        Geocoder.geocodePosition(region).then(res => {
            // console.log(res)
            // res is an Array of geocoding object (see below)
            this.setState({ alamatGPS: res[0].formattedAddress })
            Redux.component.setLocation({ region, alamatGPS: res[0].formattedAddress })
            let locationString = JSON.stringify({ region, alamatGPS: res[0].formattedAddress });
            AsyncStorage.setItem('location', locationString);
            this.setState({ isLoading: false })
            this.saveLokasi()
        }).catch(err => {
            // console.log(err)
            this.setState({ isLoading: false })
        }
        )
    }

    async cekStorageIntro() {
        try {
            let isIntro = await AsyncStorage.getItem('DashboardIntro');
            if (isIntro == null) {
                this.setState({ modalIntro: true })
            } else {
                this.setState({ modalIntro: false })
            }
        } catch (error) {
            // Error retrieving data
            // console.log('error:', error);
        }
    }
    placeholder() {
        let content = [{}, {}, {}, {}, {}, {}]
        return (
            <LoadingImage />//Loading
        )
    }

    openNews(item) {
        // console.log('data item:', item);
        // Url.getDetailBerita(item.id).then((response) => {
        //     let data = response.data.data
        //     // console.log('data get detail berita:', data);
        //     this.setState({ isLoading: false });
        //     this.goDetail(data)
        // }).catch((error) => {
        //     // console.log(error, error.response);
        //     if (error.response == null) {
        //         this.root.message("Network Error.", 'warning')
        //         this.setState({ isLoading: false });
        //     }
        //     this.root.message(error.response.data.message, 'warning');
        //     this.setState({ isLoading: false });
        // });
    }
    goDetail(data) {
        this.root.navigate('DetailBerita', data)
    }
    render() {
        let { alamatGPS, news, defaultImage, profile } = this.state

        return (
            <View style={Styles.container}>
                <Header
                    parent={this}
                // title={"Feedback"}
                />
                {this.state.isLoading == true && (
                    this.placeholder()
                )}
                {this.state.isLoading == false && (
                    <ScrollView
                        showsHorizontalScrollIndicator={false}
                        showsVerticalScrollIndicator={false}
                        style={{ flex: 1, paddingBottom: 0, paddingVertical: 8 }}
                        keyboardShouldPersistTaps='always'
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.isLoading}
                                onRefresh={this._refresh.bind(this)}
                            />
                        }>

                        <View style={{ width: width / 1.12, alignSelf: 'center', height: 1, backgroundColor: Color.gray, marginVertical: 8 }} />
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 20, flexWrap: 'wrap' }}>
                            {this.state.menu.map((item, index) => {
                                return (
                                    <TouchableOpacity key={index} onPress={item.action}>
                                        <LinearGradient
                                            start={{ x: 0.3, y: 0.5 }} end={{ x: 1, y: 0 }}
                                            // locations={[0.5, 0.25]}
                                            colors={[Color.grayWhite, item.color]}
                                            style={{ width: width / 2.3, flexDirection: 'row', alignItems: 'flex-start', borderRadius: 8, paddingVertical: 16, paddingHorizontal: 8, marginVertical: 4 }}>
                                            <Image
                                                source={item.image}
                                                style={[Styles.center, { resizeMode: 'contain', alignSelf: 'center', width: width / 6, height: width / 6, }]}
                                            />
                                            <View style={[Styles.column, { marginHorizontal: 4 }]}>
                                                <Text style={Styles.primaryFontBold}>{item.title}</Text>
                                                <Text style={[Styles.smallPrimaryFontText, { flexWrap: 'wrap' }]}>{item.description}</Text>
                                            </View>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                )
                            })}


                        </View>
                        <Text style={[Styles.blackTextBold, { marginHorizontal: 20, marginVertical: 10 }]}>Berita Terbaru</Text>
                        {news == null || news.length < 1 && (
                            <NoData><Text>Berita masih kosong</Text></NoData>
                        )}
                        {news != null && news.length > 0 && Array.isArray(news) && news.map((item, index) => {
                            return (
                                <ImageContent key={index}
                                    onPress={() => this.openNews(item)}
                                    styleContainer={{ borderBottomColor: Color.grayWhite }}
                                    radiusImage={5}
                                    backgroundColor={Color.white}
                                    sourceImage={item.photo_url != '' && item.photo_url.search("://") >= 0 ? { uri: item.photo_url } : require('../../../img/default_empty.png')}
                                    imageSize={70}
                                    name={item.judul}
                                    otherTextColor={Color.black}
                                    nameTextColor={Color.black}
                                    childText={moment(item.created_at).format("DD MMMM YYYY")}
                                    childTextColor={Color.black}
                                />
                            )
                        })}
                    </ScrollView>
                )}
                {this.state.modalIntro == true && (
                    <ModalIntro
                        setStorage={() => AsyncStorage.setItem('DashboardIntro', 'sudah')}
                        data={this.state.dataIntro}
                    />
                )}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    textInput: {
        width: '85%', height: 40, fontSize: 13, borderWidth: 1, borderColor: Color.gray,
        backgroundColor: Color.white, paddingHorizontal: 10,
        elevation: 3
    },
    picker: {
        width: '100%', height: 40,
        //  fontSize: 13,
        // borderWidth: 1, borderColor: Color.gray,
        color: Color.primaryFontFont,
        // backgroundColor: Color.white,
        // elevation: 3
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold'
    },
})
function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
const ThisScreen = connect(
    mapStateToProps
)(Dashboard);
export default withNavigation(ThisScreen);
