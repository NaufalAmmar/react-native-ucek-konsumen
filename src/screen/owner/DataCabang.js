import moment from 'moment';
import React, { Component } from 'react';
import { ActivityIndicator, Alert, Dimensions, Modal, TouchableOpacity, RefreshControl, ScrollView, StyleSheet, Text, View } from 'react-native';
import { connect } from 'react-redux';
import Button from '../component/Button';
import FloatingButton from '../component/FloatingButton';
import Header from '../component/Header';
import ImageContent from '../component/ImageContent';
import InputWithLabel from '../component/InputWithLabel';
import LoadingImage from '../component/LoadingImage';
import ModalConfirmOrder from '../component/ModalConfirmOrder';
import NoData from '../component/NoData';
import TextButton from '../component/TextButton';
import Color from '../utility/Color.js';
import Rupiah from '../utility/Rupiah.js';
import Styles from '../utility/Style.js';
import Url from '../utility/Url.js';
import Geocoder from '@arkana/react-native-geocoder';


const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    const paddingToBottom = 0;
    return layoutMeasurement.height + contentOffset.y + 20 >=
        contentSize.height - paddingToBottom;
};
const { height, width } = Dimensions.get('window');
class DataCabang extends Component {
    static navigationOptions = {
        header: null
    };
    constructor(props) {
        super(props)

        this.root = this.props.component.root;
        this.user = this.props.component.user;
        this.order = this.props.component.order;
        this.location = this.props.component.location;
        this.userData = this.user.user;

        this.state = {
            isLoading: false,
            buttonLoading: false,
            ModalConfirm: false,
            tambahModal: false,
            edit: false,
            dateTime: false,
            selectedStartDate: new Date('DD, dd mm yyyy'),
            hours: '01',
            minutes: '10',
            morn: 'AM',
            dataCabang: [],
            id_cabang: 0,
            nama_cabang: 'nama A',
            alamat_cabang: 'alamat A',
            telepon: '089182123',
            latitude: this.location != null ? this.location.region != null ? this.location.region.lat + '' : '' : '',
            longitude: this.location != null ? this.location.region != null ? this.location.region.lng + '' : '' : '',
            jam_mulai: '',
            jam_selesai: '',
            dataLokasi: [], suggestionLokasi: false,
            username: '',
            password: '',
            dataJam: [
                { label: '00', value: '00' },
                { label: '01', value: '01' },
                { label: '02', value: '02' },
                { label: '03', value: '03' },
                { label: '04', value: '04' },
                { label: '05', value: '05' },
                { label: '06', value: '06' },
            ],
            dataMenit: [
                { label: '00', value: '00' },
                { label: '05', value: '05' },
                { label: '10', value: '10' },
                { label: '15', value: '15' },
                { label: '25', value: '25' },
                { label: '30', value: '30' },
                { label: '40', value: '40' },
                { label: '45', value: '45' },
                { label: '50', value: '50' },
                { label: '55', value: '55' },
            ],
            dataMorn: [
                { label: 'AM', value: 'AM' },
                { label: 'PM', value: 'PM' },
            ]
        }
        this.onDateChange = this.onDateChange.bind(this);
    }
    _refresh() {
        // console.log(this.order)
        this.componentDidMount();
        // this.setState({ isLoading: true });
    }
    tambahModal() {
        let { dataJam, dataMenit, edit } = this.state
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.tambahModal}
                onRequestClose={() => this.setState({ tambahModal: false })}
                // onRequestClose={() => this.setState({ modalLengkapi: false })}
                style={{ flex: 1 }}
            >
                <View style={{ flex: 1, backgroundColor: Color.blackOpacity, paddingHorizontal: 16, paddingVertical: 24 }}>
                    <ScrollView style={{ flex: 1, backgroundColor: Color.white, paddingHorizontal: 8, paddingVertical: 16 }}>
                        <Text style={[Styles.bigPrimaryFontTextBold, { marginBottom: 16 }]}>Tambah Cabang</Text>
                        <InputWithLabel
                            label={"Nama Cabang"}
                            labelColor={Color.black}
                            placeholder="Nama cabang"
                            returnKeyType={"next"}
                            defaultValue={this.state.nama_cabang}
                            onChangeText={nama_cabang => { this.setState({ nama_cabang }) }}
                            clearText={() => { this.setState({ nama_cabang: '' }) }}
                        />
                        <InputWithLabel
                            label={"Alamat Cabang"}
                            labelColor={Color.black}
                            placeholder="Lokasi cabang anda"
                            returnKeyType={"next"}
                            defaultValue={this.state.alamat_cabang}
                            onChangeText={alamat_cabang => { this.getFromAddress(alamat_cabang) }}
                            clearText={() => { this.setState({ alamat_cabang: '' }) }}
                            description={"Pastikan lokasi cabang sesuai."}

                        />
                        {this.state.dataLokasi != null && Array.isArray(this.state.dataLokasi) == true && this.state.dataLokasi.length >= 1 && this.state.suggestionLokasi == true && (
                            this.state.dataLokasi.map((item, index) => {
                                return (
                                    <TouchableOpacity style={{ borderRadius: 3, marginHorizontal: 8, padding: 8, marginBottom: 16, borderBottomColor: Color.gray, borderBottomWidth: 1 }} onPress={() => this.setLokasi(item)} key={index}>
                                        <Text>{item.formattedAddress}</Text>
                                    </TouchableOpacity>
                                )
                            })
                        )}

                        <InputWithLabel
                            label={"Telepon yang bisa di hubungi"}
                            labelColor={Color.black}
                            placeholder="08xxxx"
                            returnKeyType={"next"}
                            defaultValue={this.state.telepon}
                            onChangeText={telepon => { this.setState({ telepon }) }}
                            clearText={() => { this.setState({ telepon: '' }) }}
                            keyboardType={"number-pad"}
                        />

                        <Button
                            labelButton={edit ? "Ubah Cabang" : "Tambah Cabang"}
                            onPress={edit ? () => this.ubahCabang() : () => this.tambahCabang()}
                            backgroundColor={Color.secondaryButton}
                        // iconLeft={true}
                        // iconLeftName="md-eye"
                        />
                        {edit == true && (
                            <Button
                                containerStyle={{ elevation: 0, borderWidth: 1 }}
                                labelColor={Color.redFill}
                                borderColor={Color.redFill}
                                backgroundColor={'transparent'}
                                labelButton={"Hapus Cabang"}
                                onPress={() => this.hapusCabang()}
                            // iconLeft={true}
                            // iconLeftName="md-eye"
                            />
                        )}
                        <TextButton
                            labelButton="Batal"
                            onPress={() => this.setState({ tambahModal: false })}
                            textAlign={"center"}
                            color={Color.black}
                        />
                    </ScrollView>

                </View>
            </Modal >
        )
    }
    getFromAddress(lokasi) {
        Geocoder.geocodeAddress(lokasi).then(res => {
            console.log('lokasi', res)
            this.setState({ dataLokasi: res, suggestionLokasi: true })
            // res is an Array of geocoding object (see below)            
            this.setState({ isLoading: false })
        }).catch(err => {
            // console.log(err)
            this.setState({ isLoading: false })
        }
        )
    }
    setLokasi(item) {
        let position = item.position
        this.setState({ alamat_cabang: item.formattedAddress, suggestionLokasi: false, latitude: position.lat, longitude: position.lng })
    }


    onDateChange(date) {
        this.setState({
            selectedStartDate: date,
        });
    }
    componentDidMount() {
        console.log(this.location)
        this.getDataCabang()
    }
    getDataCabang() {
        this.setState({ isLoading: true })
        Url.getCabang(this.userData.token).then((response) => {
            let data = response.data.data;
            if (data.cabang != null) {
                this.setState({ dataCabang: data.cabang });
            }
            this.setState({ isLoading: false, });
            console.log('data get cabang:', data);
        }).catch((error) => {
            console.log(error, error.response);
            if (error.response == null) {
                this.root.message("Network Error.", 'warning')
                this.setState({ isLoading: false });
            }
            this.setState({ isLoading: false });
            this.root.message(error.response.data.message, 'warning');
        });
    }
    ubahKeMenit(str) {
        let arr = str.split(":");
        return parseInt(arr[0]) * 60 + parseInt(arr[1]);
    }
    tambahCabang() {
        let { nama_cabang, alamat_cabang, telepon, hours, minutes, latitude, longitude } = this.state

        let { token } = this.userData
        let minutesInt = parseInt(this.ubahKeMenit(hours + ':' + minutes));
        if (nama_cabang.length < 1 && alamat_cabang < 10 < telepon.length < 1) {
            alert("Isi data Cabang dengan lengkap.")
        }
        else {
            Alert.alert(
                'Konfirmasi',
                'Tambah Cabang ini?',
                [
                    { text: 'Tidak', onPress: () => { this.setState({ isLoading: false }); }, style: 'cancel' },
                    {
                        text: 'Ya', onPress: () => {
                            let lama_pengerjaan = hours + ':' + minutes
                            console.log(lama_pengerjaan)
                            this.setState({ isLoading: true });
                            Url.tambahCabang(token, nama_cabang, alamat_cabang, latitude, longitude, telepon).then((response) => {
                                let data = response.data
                                console.log('data tambah cabang:', data);
                                this.root.message(data.message, 'success');
                                this.setState({ isLoading: false, tambahModal: false });
                                this._refresh()
                            }).catch((error) => {
                                console.log(error, error.response);
                                if (error.response == null) {
                                    this.root.message("Network Error.", 'warning')
                                    this.setState({ isLoading: false, tambahModal: false });
                                }
                                this.root.message(error.response.data.message, 'warning');
                                this.setState({ isLoading: false, tambahModal: false });
                            });
                        }
                    },
                ],
                { cancelable: false }
            );
        }
    }
    ubahCabang() {
        let { id_cabang, nama_cabang, alamat_cabang, telepon, latitude, longitude, hours, minutes } = this.state
        let { token } = this.userData
        let minutesInt = parseInt(this.ubahKeMenit(hours + ':' + minutes));
        if (nama_cabang.length < 1 && alamat_cabang < 10 < telepon.length < 1) {
            alert("Isi data cabang dengan lengkap.")
        }
        else if (hours != '00' && hours != '' || minutesInt >= 30) {
            Alert.alert(
                'Informasi',
                'Apakah Anda ingin mengubah cabang ?',
                [
                    { text: 'Tidak', onPress: () => { this.setState({ isLoading: false }); }, style: 'cancel' },
                    {
                        text: 'Ya', onPress: () => {
                            this.setState({ isLoading: true });
                            Url.ubahCabang(id_cabang, nama_cabang, alamat_cabang, latitude, longitude, telepon).then((response) => {
                                let data = response.data
                                console.log('data ubah cabang:', data);
                                this.root.message(data.message, 'success');
                                this.setState({ isLoading: false, tambahModal: false });
                                this._refresh()
                            }).catch((error) => {
                                console.log(error, error.response);
                                if (error.response == null) {
                                    this.root.message("Network Error.", 'warning')
                                    this.setState({ isLoading: false, tambahModal: false });
                                }
                                this.root.message(error.response.data.message, 'warning');
                                this.setState({ isLoading: false, tambahModal: false });
                            });
                        }
                    },
                ],
                { cancelable: false }
            );
        }
        else {
            alert("Isi waktu lama pengerjaan, Minimal 30 menit")
        }
    }
    hapusCabang() {
        let { id_cabang } = this.state
        let { token } = this.userData
        Alert.alert(
            'Informasi',
            'Apakah Anda ingin menghapus cabang ini ?',
            [
                { text: 'Tidak', onPress: () => { this.setState({ isLoading: false }); }, style: 'cancel' },
                {
                    text: 'Ya', onPress: () => {

                        this.setState({ isLoading: true });
                        Url.hapusCabang(id_cabang).then((response) => {
                            let data = response.data
                            console.log('data hapus cabang:', data);
                            this.root.message(data.message, 'success');
                            this.setState({ isLoading: false, tambahModal: false });
                            this._refresh()
                        }).catch((error) => {
                            console.log(error, error.response);
                            if (error.response == null) {
                                this.root.message("Network Error.", 'warning')
                                this.setState({ isLoading: false, tambahModal: false });
                            }
                            this.root.message(error.response.data.message, 'warning');
                            this.setState({ isLoading: false, tambahModal: false });
                        });
                    }
                },
            ],
            { cancelable: false }
        );

    }

    load() {
        this.setState({ loadLoading: true })
        setTimeout(() => {
            this.setState({ loadLoading: false })
        }, 300)
    }
    placeholder() {
        let content = [{}, {}, {}]
        return (
            <LoadingImage />//Loading
        )
    }
    editThis(item) {
        // let jam = item.lama_waktu != null ? item.lama_waktu : '01:10'
        // jam = jam.substring(0, 2)
        // let menit = item.lama_waktu != null ? item.lama_waktu : '01:10'
        // menit = menit.substring(3, 5)
        // console.log(jam, menit)
        this.setState({
            isLoading: true,
            edit: true,
            id_cabang: item.id,
            nama_cabang: item.nama,
            alamat_cabang: item.alamat_cabang,
            telepon: item.telepon != null ? item.telepon : '',
            // hours: jam,
            // minutes: menit,
        })
        setTimeout(() => {
            this.setState({ tambahModal: true, isLoading: false })
        }, 2000)
    }
    datePicker() {
        this.setState({ dateTime: !this.state.dateTime })
    }
    getLocation() {
        null
    }
    cari() {
        this.root.navigate('CapsterTerdekat')
    }
    submit() {
        this.setState({ ModalConfirm: false })
    }
    render() {
        let { capster, keranjang, total, dataCabang, service_charge, selectedStartDate } = this.state
        let startDate = selectedStartDate ? selectedStartDate.toString() : '';
        return (
            <View style={[Styles.container, { backgroundColor: Color.white }]}>
                <Header parent={this}
                    title="Data Cabang"
                />
                {this.state.isLoading == true && (
                    this.placeholder()
                )}
                {this.tambahModal()}
                {this.state.isLoading == false && (
                    <ScrollView
                        showsHorizontalScrollIndicator={false}
                        showsVerticalScrollIndicator={false}
                        style={{ flex: 1, }}
                        keyboardShouldPersistTaps='always'
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.isLoading}
                                onRefresh={this._refresh.bind(this)}
                            />
                        }
                        onScroll={({ nativeEvent }) => {
                            // console.log(isCloseToBottom(nativeEvent))
                            // console.log(nativeEvent)
                            if (isCloseToBottom(nativeEvent)) {
                                this.load()
                            }
                        }}>


                        {this.state.buttonLoading == true && (
                            <ActivityIndicator />
                        )}

                        {this.state.buttonLoading == false && (



                            <View style={Styles.content}>
                                {dataCabang != null && dataCabang.length == 0 && (
                                    <NoData><Text>Cabang masih kosong</Text></NoData>
                                )}
                                {dataCabang != null && dataCabang.length != 0 &&
                                    dataCabang.map((item, index) => {
                                        return (
                                            <View key={index}>
                                                <ImageContent
                                                    styleContainer={{ marginVertical: 3 }}
                                                    onPress={() => {
                                                        this.editThis(item)
                                                    }}
                                                    backgroundColor={Color.grayWhite}
                                                    childText={item.nama}
                                                    childTextColor={Color.secondaryButton}
                                                    otherText={item.telepon}
                                                    textRight={true}
                                                    textRightValue={item.alamat_cabang}
                                                />
                                            </View>
                                        )
                                    })}

                            </View>
                        )}

                    </ScrollView>
                )}
                {this.state.ModalConfirm == true && (
                    <ModalConfirmOrder
                        dataCapster={capster}
                        title="Informasi"
                        text={"Apakah anda yakin untuk memesan ?"}
                        textAction="Iya"
                        textClose="Batal"
                        action={() => this.submit()}
                        onPressClose={() => this.setState({ ModalConfirm: false })}
                    />
                )}
                <FloatingButton
                    onPress={() => this.setState({ tambahModal: true, edit: false, nama_cabang: '', alamat_cabang: '', telepon: '', minutes: '00', hours: '00' })}
                    icon={"playlist-add"}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    containerDateTime: {
        flex: 1,
        marginHorizontal: 0,
        marginVertical: 8,
        paddingHorizontal: 16,
        backgroundColor: Color.white,
        padding: 5,
        borderRadius: 5,
        borderColor: Color.grayFill, borderWidth: 1,
    },
})
function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(DataCabang);