import moment from 'moment';
import React, { Component } from 'react';
import { ActivityIndicator, Alert, Dimensions, Modal, Picker, RefreshControl, ScrollView, StyleSheet, Text, View } from 'react-native';
import { connect } from 'react-redux';
import Button from '../component/Button';
import FloatingButton from '../component/FloatingButton';
import Header from '../component/Header';
import ImageContent from '../component/ImageContent';
import InputWithLabel from '../component/InputWithLabel';
import LoadingImage from '../component/LoadingImage';
import NoData from '../component/NoData';
import TextButton from '../component/TextButton';
import Color from '../utility/Color.js';
import Rupiah from '../utility/Rupiah.js';
import Styles from '../utility/Style.js';
import Url from '../utility/Url.js';
import PickerModal from '../component/PickerModal';

const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    const paddingToBottom = 0;
    return layoutMeasurement.height + contentOffset.y + 20 >=
        contentSize.height - paddingToBottom;
};
const { height, width } = Dimensions.get('window');
class TambahKurir extends Component {
    static navigationOptions = {
        header: null
    };
    constructor(props) {
        super(props)

        this.root = this.props.component.root;
        this.user = this.props.component.user;
        this.order = this.props.component.order;
        this.userData = this.user.user;

        this.state = {
            isLoading: false,
            buttonLoading: false,
            pickerLoading: false,
            tambahModal: false,
            edit: false,
            dateTime: false,
            selectedStartDate: new Date('DD, dd mm yyyy'),
            hours: '01',
            minutes: '10',
            morn: 'AM',
            dataKurir: [],
            dataCabang: [],
            cabangSelected: '',
            id_kurir: 0,
            nama_kurir: 'nama A',
            username: 'username',
            password: '123456',
            telepon: '088778787',
            cabang_id: '',
            latitude: '', longitude: '',
            jam_mulai: '00',
            jam_selesai: '00',

            dataJam: [
                { label: '00', value: '00' },
                { label: '01', value: '01' },
                { label: '02', value: '02' },
                { label: '03', value: '03' },
                { label: '04', value: '04' },
                { label: '05', value: '05' },
                { label: '06', value: '06' },
                { label: '07', value: '07' },
                { label: '08', value: '08' },
                { label: '09', value: '09' },
                { label: '10', value: '10' },
                { label: '11', value: '11' },
                { label: '12', value: '12' },
                { label: '13', value: '13' },
                { label: '14', value: '14' },
                { label: '15', value: '15' },
                { label: '16', value: '16' },
                { label: '17', value: '17' },
                { label: '18', value: '18' },
                { label: '19', value: '19' },
                { label: '20', value: '20' },
                { label: '21', value: '21' },
                { label: '22', value: '22' },
                { label: '23', value: '23' },
            ],
            dataMenit: [
                { label: '00', value: '00' },
                { label: '05', value: '05' },
                { label: '10', value: '10' },
                { label: '15', value: '15' },
                { label: '25', value: '25' },
                { label: '30', value: '30' },
                { label: '40', value: '40' },
                { label: '45', value: '45' },
                { label: '50', value: '50' },
                { label: '55', value: '55' },
            ],
            dataMorn: [
                { label: 'AM', value: 'AM' },
                { label: 'PM', value: 'PM' },
            ],
            jamMulai: '00',
            menitMulai: '00',
            jamAkhir: '00',
            menitAkhir: '00'
        }
        this.onDateChange = this.onDateChange.bind(this);
    }
    _refresh() {
        // console.log(this.order)
        this.componentDidMount();
        // this.setState({ isLoading: true });
    }
    showPassword() {
        if (this.state.password.length < 1) {
            Alert.alert(
                'Warning',
                'Isi Pin Password Dahulu',
                [
                    {
                        text: 'Oke', onPress: () => { null }
                    }],
                { cancelable: false }
            )
        } else {
            if (this.state.show == true) {
                this.setState({ show: false, fontPassword: 14, fontPassword2: 14 });
            } else {
                this.setState({ show: true, fontPassword: 30, fontPassword2: 35 });
            }
        }
    }
    tambahModal() {
        let { dataJam, dataMenit, edit } = this.state
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.tambahModal}
                onRequestClose={() => this.setState({ tambahModal: false })}
                // onRequestClose={() => this.setState({ modalLengkapi: false })}
                style={{ flex: 1 }}
            >
                <View style={{ flex: 1, backgroundColor: Color.blackOpacity, paddingHorizontal: 16, paddingVertical: 24 }}>
                    <ScrollView style={{ flex: 1, backgroundColor: Color.white, paddingHorizontal: 8, paddingVertical: 16 }}>
                        <Text style={[Styles.bigPrimaryFontTextBold, { marginBottom: 16 }]}>Tambah Kurir</Text>
                        <InputWithLabel
                            label={"Nama Kurir"}
                            labelColor={Color.black}
                            placeholder="Nama kurir"
                            returnKeyType={"next"}
                            defaultValue={this.state.nama_kurir}
                            onChangeText={nama_kurir => { this.setState({ nama_kurir }) }}
                            clearText={() => { this.setState({ nama_kurir: '' }) }}
                        />
                        {/* <InputWithLabel
                            label={"Username Kurir"}
                            labelColor={Color.black}
                            placeholder="Username untuk kurir anda"
                            returnKeyType={"next"}
                            defaultValue={this.state.username}
                            onChangeText={username => { this.setState({ username }) }}
                            clearText={() => { this.setState({ username: '' }) }}
                        /> */}
                        <InputWithLabel
                            label={"Telepon yang bisa di hubungi"}
                            labelColor={Color.black}
                            placeholder="08xxxx"
                            returnKeyType={"next"}
                            defaultValue={this.state.telepon}
                            onChangeText={telepon => { this.setState({ telepon }) }}
                            clearText={() => { this.setState({ telepon: '' }) }}
                            keyboardType={"number-pad"}
                        />
                        <InputWithLabel
                            // styleContainer={{ paddingLeft: 20 }}
                            labelColor={Color.black}
                            placeholder="123456"
                            maxLength={6}
                            returnKeyType={"next"}
                            // ref={(input) => this.password = input}
                            // onSubmitEditing={() => this.password.focus()}
                            onChangeText={password => { this.setState({ password }) }}
                            clearText={() => { this.setState({ password: '' }) }}
                            // style={}
                            defaultValue={this.state.password}
                            // underlineColorAndroid={}
                            iconRight={this.state.password.length >= 1 ? true : false}
                            secureTextEntry={this.state.show}
                            iconRightName={this.state.show == true ? 'md-eye' : 'md-eye-off'}
                            iconPressed={() => { this.showPassword() }}
                        />
                        <Text style={[Styles.primaryFontText, { paddingVertical: 8 }]}>Jam Operasional Kerja:</Text>
                        <View style={[Styles.rowBetween, { alignItems: 'center', borderColor: Color.gray, borderBottomWidth: 1, borderTopWidth: 1, flex: 0, height: 55 }]}>
                            <Text style={[Styles.primaryFontText, { paddingVertical: 8 }]}>Jam :</Text>
                            <Picker
                                selectedValue={this.state.jamMulai}
                                style={{ height: 50, width: width / 4 }}
                                onValueChange={(itemValue, itemIndex) =>
                                    this.setState({ jamMulai: itemValue })
                                }>
                                {dataJam.map((item, index) => {
                                    return (
                                        <Picker.Item key={index} label={item.label} value={item.value} />
                                    )
                                })}
                            </Picker>
                            <Text style={[Styles.primaryFontText, { paddingVertical: 8 }]}>Menit :</Text>
                            <Picker
                                selectedValue={this.state.menitMulai}
                                style={{ height: 50, width: width / 4 }}
                                onValueChange={(itemValue, itemIndex) =>
                                    this.setState({ menitMulai: itemValue })
                                }>
                                {dataMenit.map((item, index) => {
                                    return (
                                        <Picker.Item key={index} label={item.label} value={item.value} />
                                    )
                                })}
                            </Picker>
                        </View>
                        <Text style={[Styles.primaryFontText, { paddingVertical: 8, textAlign: 'center' }]}>Sampai dengan</Text>
                        <View style={[Styles.rowBetween, { alignItems: 'center', borderColor: Color.gray, borderBottomWidth: 1, borderTopWidth: 1, flex: 0, height: 55 }]}>
                            <Text style={[Styles.primaryFontText, { paddingVertical: 8 }]}>Jam :</Text>
                            <Picker
                                selectedValue={this.state.jamAkhir}
                                style={{ height: 50, width: width / 4 }}
                                onValueChange={(itemValue, itemIndex) =>
                                    this.setState({ jamAkhir: itemValue })
                                }>
                                {dataJam.map((item, index) => {
                                    return (
                                        <Picker.Item key={index} label={item.label} value={item.value} />
                                    )
                                })}
                            </Picker>
                            <Text style={[Styles.primaryFontText, { paddingVertical: 8 }]}>Menit :</Text>
                            <Picker
                                selectedValue={this.state.menitAkhir}
                                style={{ height: 50, width: width / 4 }}
                                onValueChange={(itemValue, itemIndex) =>
                                    this.setState({ menitAkhir: itemValue })
                                }>
                                {dataMenit.map((item, index) => {
                                    return (
                                        <Picker.Item key={index} label={item.label} value={item.value} />
                                    )
                                })}
                            </Picker>

                        </View>
                        <Button
                            labelButton={edit ? "Ubah Kurir" : "Tambah Kurir"}
                            onPress={edit ? () => this.ubahKurir() : () => this.tambahKurir()}
                            backgroundColor={Color.secondaryButton}
                        // iconLeft={true}
                        // iconLeftName="md-eye"
                        />
                        {edit == true && (
                            <Button
                                containerStyle={{ elevation: 0, borderWidth: 1 }}
                                labelColor={Color.redFill}
                                borderColor={Color.redFill}
                                backgroundColor={'transparent'}
                                labelButton={"Hapus Kurir"}
                                onPress={() => this.hapusKurir()}
                            // iconLeft={true}
                            // iconLeftName="md-eye"
                            />
                        )}
                        <TextButton
                            labelButton="Batal"
                            onPress={() => this.setState({ tambahModal: false })}
                            textAlign={"center"}
                            color={Color.black}
                        />
                    </ScrollView>

                </View>
            </Modal >
        )
    }

    onDateChange(date) {
        this.setState({
            selectedStartDate: date,
        });
    }
    componentDidMount() {
        // console.log(this.state.ringkasan)
        this.getDataKurir()
        this.getDataCabang()
    }
    getDataCabang() {
        this.setState({ isLoading: true })
        Url.getCabang(this.userData.token).then((response) => {
            let data = response.data.data;
            let dataCabang = data.cabang
            if (dataCabang != null && dataCabang.length > 0) {
                dataCabang = dataCabang.map((res) => {
                    return {
                        id: res.id,
                        label: res.nama + "(" + res.alamat_cabang + ")",
                    };
                });
                this.setState({ dataCabang, isLoading: false, pickerLoading: false });
            } else {
                this.setState({ isLoading: false, pickerLoading: false });
            }
            if (dataCabang != null && dataCabang.length != 0) {
                this.setState({ cabangSelected: dataCabang[0].id, isLoading: false, pickerLoading: false });
                this.getDataKurir(dataCabang[0].id)
            } else {
                this.setState({ isLoading: false, pickerLoading: false });
            }
            this.setState({ isLoading: false, dataCabang });
            console.log('data get cabang:', data);
        }).catch((error) => {
            console.log(error, error.response);
            if (error.response == null) {
                this.root.message("Network Error.", 'warning')
                this.setState({ isLoading: false });
            }
            this.setState({ isLoading: false });
            this.root.message(error.response.data.message, 'warning');
        });
    }
    getDataKurir(cabang_id) {
        this.setState({ isLoading: true })
        Url.getKurir(cabang_id).then((response) => {
            let data = response.data.data;
            if (data.kurir != null) {
                this.setState({ dataKurir: data.kurir });
            }
            this.setState({ isLoading: false, });
            console.log('data get kurir:', data);
        }).catch((error) => {
            console.log(error, error.response);
            if (error.response == null) {
                this.root.message("Network Error.", 'warning')
                this.setState({ isLoading: false });
            }
            this.setState({ isLoading: false });
            this.root.message(error.response.data.message, 'warning');
        });
    }
    ubahKeMenit(str) {
        let arr = str.split(":");
        return parseInt(arr[0]) * 60 + parseInt(arr[1]);
    }
    tambahKurir() {
        let { nama_kurir, telepon, password, jamMulai, menitMulai, jamAkhir, menitAkhir, cabangSelected } = this.state
        let { token } = this.userData
        let jamMulaiInt = parseInt(jamMulai)
        let jamAkhirInt = parseInt(jamAkhir)
        let jam_mulai = jamMulai + ":" + menitMulai;
        let jam_akhir = jamAkhir + ":" + menitAkhir;
        if (jamMulaiInt < jamAkhirInt) {
            Alert.alert(
                'Konfirmasi',
                'Simpan Kurir baru ?',
                [
                    { text: 'Tidak', onPress: () => { this.setState({ isLoading: false }); }, style: 'cancel' },
                    {
                        text: 'Ya', onPress: () => {
                            Url.tambahKurir(this.user.token, nama_kurir, jam_akhir, jam_mulai, password, cabangSelected, telepon).then((response) => {
                                let data = response.data
                                console.log('data save kurir:', data);
                                this.setState({ isLoading: false, tambahModal: false });
                                this.root.message(data.message, 'success');
                                this._refresh()
                            }).catch((error) => {
                                console.log(error, error.response);
                                if (error.response == null) {
                                    this.root.message("Network Error.", 'warning')
                                    this.setState({ isLoading: false, tambahModal: false });
                                }
                                this.root.message(error.response.data.message, 'warning');
                                this.setState({ isLoading: false, tambahModal: false });
                            });
                        }
                    },
                ],
                { cancelable: false }
            );
        } else {
            this.root.message("Isi jam operasional dengan benar.", 'warning');
            this.setState({ isLoading: false, tambahModal: false });
        }
    }
    ubahKurir() {
        let { id_kurir, nama_kurir, telepon, password, jamMulai, menitMulai, jamAkhir, menitAkhir, cabangSelected } = this.state
        let { token } = this.userData
        let jamMulaiInt = parseInt(jamMulai)
        let jamAkhirInt = parseInt(jamAkhir)
        let jam_mulai = jamMulai + ":" + menitMulai;
        let jam_akhir = jamAkhir + ":" + menitAkhir;
        if (jamMulaiInt < jamAkhirInt) {
            Alert.alert(
                'Konfirmasi',
                'Ubah data Kurir ?',
                [
                    { text: 'Tidak', onPress: () => { this.setState({ isLoading: false }); }, style: 'cancel' },
                    {
                        text: 'Ya', onPress: () => {
                            Url.ubahKurir(id_kurir, nama_kurir, jam_akhir, jam_mulai, password, telepon).then((response) => {
                                let data = response.data
                                console.log('data ubah kurir:', data);
                                this.setState({ isLoading: false, tambahModal: false });
                                this.root.message(data.message, 'success');
                                this._refresh()
                            }).catch((error) => {
                                console.log(error, error.response);
                                if (error.response == null) {
                                    this.root.message("Network Error.", 'warning')
                                    this.setState({ isLoading: false, tambahModal: false });
                                }
                                this.root.message(error.response.data.message, 'warning');
                                this.setState({ isLoading: false, tambahModal: false });
                            });
                        }
                    },
                ],
                { cancelable: false }
            );
        } else {
            this.root.message("Isi jam operasional dengan benar.", 'warning');
            this.setState({ isLoading: false, tambahModal: false });
        }
    }
    hapusKurir() {
        let { id_kurir } = this.state
        let { token } = this.userData
        Alert.alert(
            'Informasi',
            'Apakah Anda ingin menghapus kurir ini ?',
            [
                { text: 'Tidak', onPress: () => { this.setState({ isLoading: false }); }, style: 'cancel' },
                {
                    text: 'Ya', onPress: () => {

                        this.setState({ isLoading: true });
                        Url.hapusKurir(id_kurir).then((response) => {
                            let data = response.data
                            console.log('data hapus kurir:', data);
                            this.root.message(data.message, 'success');
                            this.setState({ isLoading: false, tambahModal: false });
                            this._refresh()
                        }).catch((error) => {
                            console.log(error, error.response);
                            if (error.response == null) {
                                this.root.message("Network Error.", 'warning')
                                this.setState({ isLoading: false, tambahModal: false });
                            }
                            this.root.message(error.response.data.message, 'warning');
                            this.setState({ isLoading: false, tambahModal: false });
                        });
                    }
                },
            ],
            { cancelable: false }
        );

    }

    load() {
        this.setState({ loadLoading: true })
        setTimeout(() => {
            this.setState({ loadLoading: false })
        }, 300)
    }
    placeholder() {
        let content = [{}, {}, {}]
        return (
            <LoadingImage />//Loading
        )
    }
    editThis(item) {
        // let jam = item.lama_waktu != null ? item.lama_waktu : '01:10'
        // jam = jam.substring(0, 2)
        // let menit = item.lama_waktu != null ? item.lama_waktu : '01:10'
        // menit = menit.substring(3, 5)
        // console.log(jam, menit)
        let jam_mulai = item.jam_kerja_kurir.jam_mulai
        let jam_selesai = item.jam_kerja_kurir.jam_selesai
        this.setState({
            isLoading: true,
            edit: true,
            id_kurir: item.id,
            nama_kurir: item.nama,
            telepon: item.telepon != null ? item.telepon + '' : '',
            jamMulai: jam_mulai.substring(0, 2),
            menitMulai: jam_mulai.substring(3, 5),
            jamAkhir: jam_selesai.substring(0, 2),
            menitAkhir: jam_selesai.substring(3, 5),
            // hours: jam,
            // minutes: menit,
        })
        setTimeout(() => {
            this.setState({ tambahModal: true, isLoading: false })
        }, 400)
    }
    datePicker() {
        this.setState({ dateTime: !this.state.dateTime })
    }
    getLocation() {
        null
    }
    setCabang(data) {
        this.getDataKurir(data)
        this.setState({ cabangSelected: data, statistic_selected: statisticsId })
    }
    render() {
        let { dataKurir, dataCabang, selectedStartDate } = this.state
        let startDate = selectedStartDate ? selectedStartDate.toString() : '';
        return (
            <View style={[Styles.container, { backgroundColor: Color.white }]}>
                <Header parent={this}
                    title="Data Kurir"
                />
                {this.state.isLoading == true && (
                    this.placeholder()
                )}
                {this.tambahModal()}

                <ScrollView
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    style={{ flex: 1, }}
                    keyboardShouldPersistTaps='always'
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.isLoading}
                            onRefresh={this._refresh.bind(this)}
                        />
                    }
                    onScroll={({ nativeEvent }) => {
                        // console.log(isCloseToBottom(nativeEvent))
                        // console.log(nativeEvent)
                        if (isCloseToBottom(nativeEvent)) {
                            this.load()
                        }
                    }}>
                    <PickerModal
                        placeholder="Pilih Cabang"
                        selectedValue={this.state.cabangSelected}
                        data={dataCabang}
                        onValueChange={(itemValue, itemIndex) => {
                            this.setCabang(itemValue, itemIndex)
                        }}
                    />

                    {this.state.buttonLoading == true && (
                        <ActivityIndicator />
                    )}

                    {this.state.buttonLoading == false && (



                        <View style={Styles.content}>
                            {dataKurir != null && dataKurir.length == 0 && (
                                <NoData><Text>Kurir masih kosong</Text></NoData>
                            )}
                            {dataKurir != null && dataKurir.length != 0 &&
                                dataKurir.map((item, index) => {
                                    return (
                                        <View key={index}>
                                            <ImageContent
                                                styleContainer={{ marginVertical: 3 }}
                                                onPress={() => {
                                                    this.editThis(item)
                                                }}
                                                backgroundColor={Color.grayWhite}
                                                childText={item.nama}
                                                childTextColor={Color.secondaryButton}
                                                otherText={item.telepon != null ? item.telepon : ''}
                                            // textRight={true}
                                            // textRightValue={item.username}
                                            />
                                        </View>
                                    )
                                })}

                        </View>
                    )}

                </ScrollView>

                <FloatingButton
                    onPress={() => this.setState({ tambahModal: true, edit: false })}
                    // onPress={() => this.setState({ tambahModal: true, edit: false, nama_kurir: '', username: '',password:',' telepon: '', minutes: '00', hours: '00' })}
                    icon={"playlist-add"}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    containerDateTime: {
        flex: 1,
        marginHorizontal: 0,
        marginVertical: 8,
        paddingHorizontal: 16,
        backgroundColor: Color.white,
        padding: 5,
        borderRadius: 5,
        borderColor: Color.grayFill, borderWidth: 1,
    },
})
function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(TambahKurir);