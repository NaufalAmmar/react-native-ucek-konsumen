import moment from 'moment';
import React, { Component } from 'react';
import { ActivityIndicator, Alert, Dimensions, Modal, Picker, RefreshControl, ScrollView, StyleSheet, Text, View } from 'react-native';
import { connect } from 'react-redux';
import Button from '../component/Button';
import FloatingButton from '../component/FloatingButton';
import Header from '../component/Header';
import ImageContent from '../component/ImageContent';
import InputWithLabel from '../component/InputWithLabel';
import LoadingImage from '../component/LoadingImage';
import NoData from '../component/NoData';
import TextButton from '../component/TextButton';
import Color from '../utility/Color.js';
import Rupiah from '../utility/Rupiah.js';
import Styles from '../utility/Style.js';
import Url from '../utility/Url.js';
import PickerModal from '../component/PickerModal';

const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    const paddingToBottom = 0;
    return layoutMeasurement.height + contentOffset.y + 20 >=
        contentSize.height - paddingToBottom;
};
const { height, width } = Dimensions.get('window');
class DataLayanan extends Component {
    static navigationOptions = {
        header: null
    };
    constructor(props) {
        super(props)

        this.root = this.props.component.root;
        this.user = this.props.component.user;
        this.order = this.props.component.order;
        this.userData = this.user.user;

        this.state = {
            isLoading: false,
            buttonLoading: false,
            pickerLoading: false,
            tambahModal: false,
            edit: false,
            dateTime: false,
            selectedStartDate: new Date('DD, dd mm yyyy'),
            hours: '01',
            minutes: '10',
            morn: 'AM',
            dataLayanan: [],
            dataCabang: [],
            cabangSelected: '',
            id_layanan: 0,
            nama_layanan: 'nama A',
            keterangan: 'keterangan A',
            satuan: 'Kg',
            lama_waktu: '8',
            harga: '8000', promo: '500',
            cabang_id: '',
            latitude: '', longitude: '',
            jam_mulai: '',
            jam_selesai: '',

            dataJam: [
                { label: '00', value: '00' },
                { label: '01', value: '01' },
                { label: '02', value: '02' },
                { label: '03', value: '03' },
                { label: '04', value: '04' },
                { label: '05', value: '05' },
                { label: '06', value: '06' },
                { label: '07', value: '07' },
                { label: '08', value: '08' },
                { label: '09', value: '09' },
                { label: '10', value: '10' },
                { label: '11', value: '11' },
                { label: '12', value: '12' },
                { label: '13', value: '13' },
                { label: '14', value: '14' },
                { label: '15', value: '15' },
                { label: '16', value: '16' },
                { label: '17', value: '17' },
                { label: '18', value: '18' },
                { label: '19', value: '19' },
                { label: '20', value: '20' },
                { label: '21', value: '21' },
                { label: '22', value: '22' },
                { label: '23', value: '23' },
            ],
            dataMenit: [
                { label: '00', value: '00' },
                { label: '05', value: '05' },
                { label: '10', value: '10' },
                { label: '15', value: '15' },
                { label: '25', value: '25' },
                { label: '30', value: '30' },
                { label: '40', value: '40' },
                { label: '45', value: '45' },
                { label: '50', value: '50' },
                { label: '55', value: '55' },
            ],
            dataMorn: [
                { label: 'AM', value: 'AM' },
                { label: 'PM', value: 'PM' },
            ],
            jamMulai: '00',
            menitMulai: '00',
            jamAkhir: '00',
            menitAkhir: '00'
        }
        this.onDateChange = this.onDateChange.bind(this);
    }
    _refresh() {
        // console.log(this.order)
        this.componentDidMount();
        // this.setState({ isLoading: true });
    }

    tambahModal() {
        let { dataJam, dataMenit, edit } = this.state
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.tambahModal}
                onRequestClose={() => this.setState({ tambahModal: false })}
                // onRequestClose={() => this.setState({ modalLengkapi: false })}
                style={{ flex: 1 }}
            >
                <View style={{ flex: 1, backgroundColor: Color.blackOpacity, paddingHorizontal: 16, paddingVertical: 24 }}>
                    <ScrollView style={{ flex: 1, backgroundColor: Color.white, paddingHorizontal: 8, paddingVertical: 16 }}>
                        <Text style={[Styles.bigPrimaryFontTextBold, { marginBottom: 16 }]}>Tambah Layanan</Text>
                        <InputWithLabel
                            label={"Nama Layanan"}
                            labelColor={Color.black}
                            placeholder="Nama layanan"
                            returnKeyType={"next"}
                            defaultValue={this.state.nama_layanan}
                            onChangeText={nama_layanan => { this.setState({ nama_layanan }) }}
                            clearText={() => { this.setState({ nama_layanan: '' }) }}
                        />
                        {/* <InputWithLabel
                            label={" Layanan"}
                            labelColor={Color.black}
                            placeholder=" untuk layanan anda"
                            returnKeyType={"next"}
                            defaultValue={this.state.}
                            onChangeText={ => { this.setState({  }) }}
                            clearText={() => { this.setState({ : '' }) }}
                        /> */}
                        <InputWithLabel
                            label={"Keterangan Layanan"}
                            labelColor={Color.black}
                            placeholder="Keterangan"
                            returnKeyType={"next"}
                            defaultValue={this.state.keterangan}
                            onChangeText={keterangan => { this.setState({ keterangan }) }}
                            clearText={() => { this.setState({ keterangan: '' }) }}
                        />
                        <InputWithLabel
                            label={"Satuan Layanan"}
                            labelColor={Color.black}
                            placeholder="Kg/Pasang/Unit"
                            returnKeyType={"next"}
                            defaultValue={this.state.satuan}
                            onChangeText={satuan => { this.setState({ satuan }) }}
                            clearText={() => { this.setState({ satuan: '' }) }}
                        />
                        <InputWithLabel
                            label={"Lama waktu / Jam"}
                            labelColor={Color.black}
                            placeholder="Lama waktu"
                            returnKeyType={"next"}
                            defaultValue={this.state.lama_waktu}
                            onChangeText={lama_waktu => { this.setState({ lama_waktu }) }}
                            clearText={() => { this.setState({ lama_waktu: '' }) }}
                            keyboardType={"number-pad"}
                        />

                        <InputWithLabel
                            label={"Harga"}
                            labelColor={Color.black}
                            placeholder="Harga"
                            returnKeyType={"next"}
                            defaultValue={this.state.harga}
                            onChangeText={harga => { this.setState({ harga }) }}
                            clearText={() => { this.setState({ harga: '' }) }}
                            keyboardType={"number-pad"}
                        />
                        <InputWithLabel
                            label={"Promo / Potongan"}
                            labelColor={Color.black}
                            placeholder="Potongan akan di potong langsung dari Harga"
                            returnKeyType={"next"}
                            defaultValue={this.state.promo}
                            onChangeText={promo => { this.setState({ promo }) }}
                            clearText={() => { this.setState({ promo: '' }) }}
                            keyboardType={"number-pad"}
                        />

                        <Button
                            labelButton={edit ? "Ubah Layanan" : "Tambah Layanan"}
                            onPress={edit ? () => this.ubahLayanan() : () => this.tambahLayanan()}
                            backgroundColor={Color.secondaryButton}
                        // iconLeft={true}
                        // iconLeftName="md-eye"
                        />
                        {edit == true && (
                            <Button
                                containerStyle={{ elevation: 0, borderWidth: 1 }}
                                labelColor={Color.redFill}
                                borderColor={Color.redFill}
                                backgroundColor={'transparent'}
                                labelButton={"Hapus Layanan"}
                                onPress={() => this.hapusLayanan()}
                            // iconLeft={true}
                            // iconLeftName="md-eye"
                            />
                        )}
                        <TextButton
                            labelButton="Batal"
                            onPress={() => this.setState({ tambahModal: false })}
                            textAlign={"center"}
                            color={Color.black}
                        />
                    </ScrollView>

                </View>
            </Modal >
        )
    }

    onDateChange(date) {
        this.setState({
            selectedStartDate: date,
        });
    }
    componentDidMount() {
        // console.log(this.state.ringkasan)
        this.getDataCabang()
    }
    getDataCabang() {
        this.setState({ isLoading: true })
        Url.getCabang(this.userData.token).then((response) => {
            let data = response.data.data;
            console.log('data get cabang:', response);
            let dataCabang = data.cabang
            if (dataCabang != null && dataCabang.length > 0) {
                dataCabang = dataCabang.map((res) => {
                    return {
                        id: res.id,
                        label: res.nama + "(" + res.alamat_cabang + ")",
                    };
                });
                this.setState({ dataCabang, isLoading: false, pickerLoading: false });
            } else {
                this.setState({ isLoading: false, pickerLoading: false });
            }
            if (dataCabang != null && dataCabang.length != 0) {
                this.setState({ cabangSelected: dataCabang[0].id, isLoading: false, pickerLoading: false });
                this.getDataLayanan(dataCabang[0].id)
            } else {
                this.setState({ isLoading: false, pickerLoading: false });
            }
            this.setState({ isLoading: false, dataCabang });
        }).catch((error) => {
            console.log(error, error.response);
            if (error.response == null) {
                this.root.message("Network Error.", 'warning')
                this.setState({ isLoading: false });
            }
            this.setState({ isLoading: false });
            this.root.message(error.response.data.message, 'warning');
        });
    }
    getDataLayanan(cabang_id) {
        this.setState({ isLoading: true })
        Url.getLayanan(cabang_id).then((response) => {
            let data = response.data.data;
            console.log('data get layanan:', data);
            if (data.layanan != null) {
                this.setState({ dataLayanan: data.layanan });
            }
            this.setState({ isLoading: false, });
            console.log('data get layanan:', data);
        }).catch((error) => {
            console.log(error, error.response);
            if (error.response == null) {
                // this.root.message("Network Error.", 'warning')
                this.setState({ isLoading: false });
            }
            this.setState({ isLoading: false });
            this.root.message(error.response.data.message, 'warning');
        });
    }
    ubahKeMenit(str) {
        let arr = str.split(":");
        return parseInt(arr[0]) * 60 + parseInt(arr[1]);
    }
    tambahLayanan() {
        let { nama_layanan, keterangan, satuan, lama_waktu, harga, promo, cabangSelected } = this.state
        let { token } = this.userData

        if (nama_layanan, keterangan, satuan, lama_waktu, harga, promo, cabangSelected) {
            Alert.alert(
                'Konfirmasi',
                'Simpan Layanan baru ?',
                [
                    { text: 'Tidak', onPress: () => { this.setState({ isLoading: false }); }, style: 'cancel' },
                    {
                        text: 'Ya', onPress: () => {
                            Url.tambahLayanan(this.user.token, cabangSelected, nama_layanan, keterangan, satuan, lama_waktu, harga, promo).then((response) => {
                                let data = response.data
                                console.log('data save layanan:', data);
                                this.setState({ isLoading: false, tambahModal: false });
                                this.root.message(data.message, 'success');
                                this._refresh()
                            }).catch((error) => {
                                console.log(error, error.response);
                                if (error.response == null) {
                                    this.root.message("Network Error.", 'warning')
                                    this.setState({ isLoading: false, tambahModal: false });
                                }
                                this.root.message(error.response.data.message, 'warning');
                                this.setState({ isLoading: false, tambahModal: false });
                            });
                        }
                    },
                ],
                { cancelable: false }
            );
        } else {
            this.root.message("Isi jam operasional dengan benar.", 'warning');
            this.setState({ isLoading: false, tambahModal: false });
        }
    }
    ubahLayanan() {
        let { id_layanan, nama_layanan, keterangan, satuan, lama_waktu, harga, promo, cabangSelected } = this.state
        let { token } = this.userData

        if (nama_layanan, keterangan, satuan, lama_waktu, harga, promo, cabangSelected) {
            Alert.alert(
                'Konfirmasi',
                'Ubah data Layanan ?',
                [
                    { text: 'Tidak', onPress: () => { this.setState({ isLoading: false }); }, style: 'cancel' },
                    {
                        text: 'Ya', onPress: () => {
                            Url.ubahLayanan(id_layanan, cabangSelected, nama_layanan, keterangan, satuan, lama_waktu, harga, promo).then((response) => {
                                let data = response.data
                                console.log('data ubah layanan:', data);
                                this.setState({ isLoading: false, tambahModal: false });
                                this.root.message(data.message, 'success');
                                this._refresh()
                            }).catch((error) => {
                                console.log(error, error.response);
                                if (error.response == null) {
                                    this.root.message("Network Error.", 'warning')
                                    this.setState({ isLoading: false, tambahModal: false });
                                }
                                this.root.message(error.response.data.message, 'warning');
                                this.setState({ isLoading: false, tambahModal: false });
                            });
                        }
                    },
                ],
                { cancelable: false }
            );
        } else {
            this.root.message("Isi jam operasional dengan benar.", 'warning');
            this.setState({ isLoading: false, tambahModal: false });
        }
    }
    hapusLayanan() {
        let { id_layanan } = this.state
        let { token } = this.userData
        Alert.alert(
            'Informasi',
            'Apakah Anda ingin menghapus layanan ini ?',
            [
                { text: 'Tidak', onPress: () => { this.setState({ isLoading: false }); }, style: 'cancel' },
                {
                    text: 'Ya', onPress: () => {

                        this.setState({ isLoading: true });
                        Url.hapusLayanan(id_layanan).then((response) => {
                            let data = response.data
                            console.log('data hapus layanan:', data);
                            this.root.message(data.message, 'success');
                            this.setState({ isLoading: false, tambahModal: false });
                            this._refresh()
                        }).catch((error) => {
                            console.log(error, error.response);
                            if (error.response == null) {
                                this.root.message("Network Error.", 'warning')
                                this.setState({ isLoading: false, tambahModal: false });
                            }
                            this.root.message(error.response.data.message, 'warning');
                            this.setState({ isLoading: false, tambahModal: false });
                        });
                    }
                },
            ],
            { cancelable: false }
        );

    }

    load() {
        this.setState({ loadLoading: true })
        setTimeout(() => {
            this.setState({ loadLoading: false })
        }, 300)
    }
    placeholder() {
        let content = [{}, {}, {}]
        return (
            <LoadingImage />//Loading
        )
    }
    editThis(item) {
        // let jam = item.lama_waktu != null ? item.lama_waktu : '01:10'
        // jam = jam.substring(0, 2)
        // let menit = item.lama_waktu != null ? item.lama_waktu : '01:10'
        // menit = menit.substring(3, 5)
        // console.log(jam, menit)
        this.setState({
            isLoading: true,
            edit: true,
            id_layanan: item.id,
            nama_layanan: item.nama,
            keterangan: item.keterangan != null ? item.keterangan : '',
            // hours: jam,
            // minutes: menit,
        })
        setTimeout(() => {
            this.setState({ tambahModal: true, isLoading: false })
        }, 400)
    }
    datePicker() {
        this.setState({ dateTime: !this.state.dateTime })
    }
    getLocation() {
        null
    }
    setCabang(data) {
        this.getDataLayanan(data)
        this.setState({ cabangSelected: data, statistic_selected: statisticsId })
    }
    render() {
        let { dataLayanan, dataCabang, selectedStartDate } = this.state
        let startDate = selectedStartDate ? selectedStartDate.toString() : '';
        return (
            <View style={[Styles.container, { backgroundColor: Color.white }]}>
                <Header parent={this}
                    title="Data Layanan"
                />
                {this.state.isLoading == true && (
                    this.placeholder()
                )}
                {this.tambahModal()}

                <ScrollView
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    style={{ flex: 1, }}
                    keyboardShouldPersistTaps='always'
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.isLoading}
                            onRefresh={this._refresh.bind(this)}
                        />
                    }
                    onScroll={({ nativeEvent }) => {
                        // console.log(isCloseToBottom(nativeEvent))
                        // console.log(nativeEvent)
                        if (isCloseToBottom(nativeEvent)) {
                            this.load()
                        }
                    }}>
                    <PickerModal
                        placeholder="Pilih Cabang"
                        selectedValue={this.state.cabangSelected}
                        data={dataCabang}
                        onValueChange={(itemValue, itemIndex) => {
                            this.setCabang(itemValue, itemIndex)
                        }}
                    />

                    {this.state.buttonLoading == true && (
                        <ActivityIndicator />
                    )}

                    {this.state.buttonLoading == false && (



                        <View style={Styles.content}>
                            {dataLayanan != null && dataLayanan.length == 0 && (
                                <NoData><Text>Layanan masih kosong</Text></NoData>
                            )}
                            {dataLayanan != null && dataLayanan.length != 0 &&
                                dataLayanan.map((item, index) => {
                                    return (
                                        <View key={index}>
                                            <ImageContent
                                                styleContainer={{ marginVertical: 3 }}
                                                onPress={() => {
                                                    this.editThis(item)
                                                }}
                                                backgroundColor={Color.grayWhite}
                                                childText={item.nama}
                                                childTextColor={Color.secondaryButton}
                                                otherText={item.keterangan != null ? item.keterangan : ''}
                                            // textRight={true}
                                            // textRightValue={item.}
                                            />
                                        </View>
                                    )
                                })}

                        </View>
                    )}

                </ScrollView>

                <FloatingButton
                    onPress={() => this.setState({ tambahModal: true, edit: false })}
                    // onPress={() => this.setState({ tambahModal: true, edit: false, nama_layanan: '', : '',:',' keterangan: '', minutes: '00', hours: '00' })}
                    icon={"playlist-add"}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    containerDateTime: {
        flex: 1,
        marginHorizontal: 0,
        marginVertical: 8,
        paddingHorizontal: 16,
        backgroundColor: Color.white,
        padding: 5,
        borderRadius: 5,
        borderColor: Color.grayFill, borderWidth: 1,
    },
})
function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(DataLayanan);