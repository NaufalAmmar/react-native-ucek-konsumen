import { dispatch as Redux } from '@rematch/core';
import React, { Component } from 'react';
import { ActivityIndicator, Alert, BackHandler, Modal, PermissionsAndroid, Text, TouchableOpacity, View, DeviceEventEmitter } from 'react-native';
import { showMessage } from "react-native-flash-message";
import AsyncStorage from '@react-native-community/async-storage';
import OneSignal from 'react-native-onesignal';
import { connect } from 'react-redux';
import Color from '../screen/utility/Color';
import Styles from '../screen/utility/Style';
import Url from '../screen/utility/Url';
import IntroSlider from './component/IntroSlider';
import SplashScreen from './component/SplashScreen';
// user
import SignIn from './user/SignIn';
import SignUp from './user/SignUp';
import Mainboard from './user/Mainboard';
// owner
import SignInOwner from './owner/SignInOwner';
import SignUpOwner from './owner/SignUpOwner';
import MainboardOwner from './owner/MainboardOwner';

//kurir
import SignInKurir from './kurir/SignInKurir';

import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";
import Geolocation from '@react-native-community/geolocation';

console.disableYellowBox = true;


let root;

class Root extends Component {
    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);

        this.state = {
            selectedPage: '',
            params: {},
            backStack: [],
            backStackParams: [],
            deviceId: null,
            deviceIdRegistered: false,
            isOpen: false,
            phoneGrant: 'denied',
            gpsGrant: 'denied',
            page: '',
            message: '',
            modalLoading: false,
            messageShow: false
        };

        Redux.component.setRoot(this);
        this.root = this.props.component.root;

        this.hardwareBackPress = () => {
            // alert(this.state.backStack);
            if (this.props.navigation.isFocused()) {

                Alert.alert(
                    'Informasi',
                    'Apakah Anda ingin keluar aplikasi ?',
                    [
                        { text: 'Tidak', onPress: () => { }, style: 'cancel' },
                        {
                            text: 'Ya', onPress: () => {
                                BackHandler.exitApp();
                            }
                        },
                    ],
                    { cancelable: false }
                );
            } else if (this.state.page == 'ZenitoQuestion') {
                Alert.alert(
                    'Peringatan',
                    'Anda tidak bisa kembali, sebelum finish.',
                    [
                        { text: 'Iya', onPress: () => { }, style: 'cancel' },
                    ],
                    { cancelable: false }
                );
            }
            else if (this.state.page == 'SnapWebView') {
                Alert.alert(
                    'Peringatan',
                    'Anda yakin untuk membatalkan pembayaran?',
                    [{
                        text: 'Tidak', onPress: () => {

                        }
                    },
                    { text: 'Iya', onPress: () => { this.goBack(); }, style: 'cancel' },
                    ],
                    { cancelable: false }
                );
            } else {
                this.root.goBack();
                return true;
            }
            return true;
        }

        root = this;

        BackHandler.addEventListener('hardwareBackPress', this.hardwareBackPress);

        this._onReceived = (notification) => { this.onReceived(notification) };
        this._onOpened = (openResult) => { this.onOpened(openResult) };
        this._onIds = (openResult) => { this.onIds(openResult) };

        OneSignal.init("759db586-addc-401d-888f-7088c9abb522");
        OneSignal.setLogLevel(6, 0);

        OneSignal.addEventListener('received', this._onReceived);
        OneSignal.addEventListener('opened', this._onOpened);
        OneSignal.addEventListener('ids', this._onIds);

        OneSignal.setSubscription(true);
        OneSignal.inFocusDisplaying(2);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.hardwareBackPress);
        Geolocation.clearWatch(this.watchID)
        LocationServicesDialogBox.stopListener();
        OneSignal.removeEventListener('received', this._onReceived);
        OneSignal.removeEventListener('opened', this._onOpened);
        OneSignal.removeEventListener('ids', this._onIds);
    }
    getCurrentPosition() {
        try {
            Geolocation.getCurrentPosition(
                (position) => {
                    console.log(position)
                    const region = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude,
                    };
                    Redux.component.setLocation({ region })
                    let locationString = JSON.stringify({ region });
                    AsyncStorage.setItem('location', locationString);
                },
                (error) => {
                    // console.log(error)
                    //TODO: better design
                    switch (error.code) {
                        case 1:
                            if (Platform.OS === "ios") {
                                // Alert.alert("Peringatan", "Mohon cek izin Aplikasi di pengaturan.");
                            } else {
                                // Alert.alert("Peringatan", "Mohon cek izin Aplikasi di pengaturan.");
                            }
                            break;
                        default:
                        // Alert.alert("Peringatan", "Mohon cek izin Aplikasi di pengaturan.");
                    }
                },
                { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
            );
        } catch (e) {
            alert(e.message || "");
        }
    };
    onReceived(notification) {
        // console.log("Notification received: ", notification);
        let data = notification.payload.additionalData
        let dashboard = this.props.component.dashboard
        let s = data
        if (s != null && dashboard != null) {
            dashboard._refresh()
        }
    }

    onOpened(openResult) {
        this.setState({ isOpen: true })
        let data = openResult.notification.payload.additionalData
        // console.log(data)
        let dashboard = this.props.component.dashboard
        let s = data
        // if (s != null && dashboard != null) {
        //     dashboard._refresh()
        // }
        // console.log('Message: ', openResult.notification.payload.body);
        // console.log('Data: ', openResult.notification.payload.additionalData);
        // console.log('isActive: ', openResult.notification.isAppInFocus);
        // console.log('openResult: ', openResult);
    }
    modalLoading() {
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.modalLoading}
                onRequestClose={() => null}>
                <TouchableOpacity style={[{ flex: 1, backgroundColor: 'rgba(0,0,0,0.3)' }, Styles.center]}>
                    <Text style={Styles.whiteTextBold}>Sedang mengambil data..</Text>
                    <ActivityIndicator color={Color.white} size={"large"} />
                </TouchableOpacity>
            </Modal>
        )
    }
    message(message, tipe) {
        let string = ''
        if (message != null) {
            if (Array.isArray(message)) {
                message.map((i) => {
                    string = string + " " + i
                })
                if (string != '') {
                    showMessage({
                        hideOnPress: true,
                        message: string,
                        // description: "My message description",
                        type: tipe,
                        animationDuration: 500,
                        duration: 7500,
                        // hideStatusBar: true,
                        // backgroundColor: "purple", // background color
                        color: Color.white, // text color
                    });
                }
            } else {
                showMessage({
                    hideOnPress: true,
                    message: message,
                    // description: "My message description",
                    type: tipe,
                    animationDuration: 500,
                    duration: 7500,
                    // hideStatusBar: true,
                    // backgroundColor: "purple", // background color
                    color: Color.white, // text color
                });
            }
        }
    }

    async requestGPSPermission() {
        try {
            const gpsGranted = await PermissionsAndroid.request(
                // PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                // PermissionsAndroid.PERMISSIONS.ACCESS_GPS,
                {
                    title: 'Cukurin',
                    message:
                        'Untuk Akses GPS,' +
                        'Anda harus Klik OK.',
                    // buttonNeutral: 'Ask Me Later',
                    buttonNegative: 'Tutup',
                    buttonPositive: 'OK',
                },
            );
            if (gpsGranted === PermissionsAndroid.RESULTS.GRANTED) {
                // console.log('You can use the GPS');
                this.setState({ gpsGrant: 'granted' })
                this.getCurrentPosition()
                this.requestCameraPermission()
            } else {
                Alert.alert(
                    'Peringatan',
                    'Mohon izinkan Akses ini',
                    [
                        {
                            text: 'Tutup', onPress: () => {
                                null
                            }
                        }, {
                            text: 'Oke', onPress: () => {
                                null
                            }
                        }],
                    { cancelable: false }
                )
                this.requestGPSPermission();
                // console.log('GPS permission denied');
            }
        } catch (err) {
            console.warn(err);
        }
    }
    async requestCameraPermission() {
        try {
            const gpsGranted = await PermissionsAndroid.request(
                // PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
                PermissionsAndroid.PERMISSIONS.CAMERA,
                // PermissionsAndroid.PERMISSIONS.ACCESS_GPS,
                {
                    title: 'Cukurin',
                    message:
                        'Untuk Akses GPS,' +
                        'Anda harus Klik OK.',
                    // buttonNeutral: 'Ask Me Later',
                    buttonNegative: 'Tutup',
                    buttonPositive: 'OK',
                },
            );
            if (gpsGranted === PermissionsAndroid.RESULTS.GRANTED) {
                // console.log('You can use the GPS');
                this.setState({ gpsGrant: 'granted' })
            } else {
                Alert.alert(
                    'Peringatan',
                    'Mohon izinkan Akses ini',
                    [
                        {
                            text: 'Tutup', onPress: () => {
                                this.requestCameraPermission()
                            }
                        }, {
                            text: 'Oke', onPress: () => {
                                this.requestCameraPermission()
                            }
                        }],
                    { cancelable: false }
                )
                this.requestGPSPermission();
                // console.log('GPS permission denied');
            }
        } catch (err) {
            console.warn(err);
        }
    }

    turnOnGPS() {
        if (Platform.OS === 'android')
            LocationServicesDialogBox.checkLocationServicesIsEnabled({
                message: "<h2 style='color: #0af13e'>Use Location ?</h2>This app wants to change your device settings:<br/><br/>Use GPS, Wi-Fi, and cell network for location<br/><br/><a href='#'>Learn more</a>",
                ok: "YES",
                cancel: "NO",
                enableHighAccuracy: false, // true => GPS AND NETWORK PROVIDER, false => GPS OR NETWORK PROVIDER
                showDialog: true, // false => Opens the Location access page directly
                openLocationServices: true, // false => Directly catch method is called if location services are turned off
                preventOutSideTouch: false, // true => To prevent the location services window from closing when it is clicked outside
                preventBackClick: false, // true => To prevent the location services popup from closing when it is clicked back button
                providerListener: true // true ==> Trigger locationProviderStatusChange listener when the location state changes
            }).then((success) => {
                // console.log(success); // success => {alreadyEnabled: false, enabled: true, status: "enabled"}
                this.requestGPSPermission();
                this.getCurrentPosition();
            }).catch((error) => {
                // console.log(error.message); // error.message => "disabled"
            });

        BackHandler.addEventListener('hardwareBackPress', () => { //(optional) you can use it if you need it
            //do not use this method if you are using navigation."preventBackClick: false" is already doing the same thing.
            LocationServicesDialogBox.forceCloseDialog();
        });

        DeviceEventEmitter.addListener('locationProviderStatusChange', function (status) { // only trigger when "providerListener" is enabled
            // console.log(status); //  status => {enabled: false, status: "disabled"} or {enabled: true, status: "enabled"}
        });
    }
    async componentDidMount() {
        // AsyncStorage.setItem('konsumen', '');
        this.changePage('SplashScreen');
        this.loadToken();
        // this.logOut();
        setTimeout(() => {
            this.turnOnGPS();
            this.requestGPSPermission();
            this.requestCameraPermission();
        }, 2000)
    }

    async cekStorageNotification() {
        try {
            let isOpen = await AsyncStorage.getItem('isOpen');
            if (isOpen == null) {
                // this.changePage('SplashScreen')
                // this.requestPhonePermission();
                // this.loadToken();

            } else {
                this.navigate(isOpen)
                setTimeout(() => {
                    AsyncStorage.setItem('isOpen', '');
                }, 3000)
            }
        } catch (error) {
            // Error retrieving data
            // console.log('error:', error);
        }
    }
    async requestPhonePermission() {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_PHONE_STATE,
                {
                    title: 'Cukurin',
                    message:
                        'Untuk Bisa Login,' +
                        'Anda harus Klik Izinkan.',
                    // buttonNeutral: 'Ask Me Later',
                    buttonNegative: 'Cancel',
                    buttonPositive: 'OK',
                },
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                // console.log('Phone PermissionAndroid.RESULTS', PermissionsAndroid.RESULTS)
                this.setState({ phoneGrant: 'granted' })
                if (this.state.gpsGrant == PermissionsAndroid.RESULTS.DENIED) {
                    // this.requestGPSPermission();
                }
            } else {
                Alert.alert(
                    'Peringatan',
                    'Anda Harus Mengizinkan Akses ini !',
                    [
                        {
                            text: 'Tutup', onPress: () => {

                            }
                        }, {
                            text: 'Oke', onPress: () => {
                                this.requestPhonePermission();
                            }
                        }],
                    { cancelable: false }
                )
            }
        } catch (err) {
            // console.warn(err);
        }
    }
    onIds(device) {
        // console.log('Device info: ', device);
        this.setState({ deviceId: device.userId });

    }
    goBack() {
        let backStack = this.state.backStack;
        let lastElement = backStack.pop();
        let backStackParams = this.state.backStackParams;
        let lastParams = backStackParams.pop();
        this.setState({
            page: lastElement,
            // selectedPage: lastElement,
            params: lastParams,
            backStack,
            backStackParams
        });
        // this.props.navigation.goBack();
        // return true;
        this.props.navigation.pop()
    }
    changePage(page, params) {
        this.setState({ selectedPage: page, params });
    }
    navigate(page, params = {}, resetBackStack = false) {

        let { backStack, backStackParams } = this.state;
        backStackParams.push(params);
        backStack.push(page);
        this.setState({ params, page, backStackParams, backStack });
        this.props.navigation.navigate(page, params);
        if (resetBackStack == true) {
            this.setState({ backStack: [], backStackParams: [] });
            this.props.navigation.navigate(page, params);
        }
    }
    resetBackStack() {
        this.setState({ backStack: [], backStackParams: [], page: '' });
    }

    logOut(token, deviceId) {
        try {
            Alert.alert(
                'Informasi',
                'Anda yakin ingin Keluar ?',
                [
                    {
                        text: 'Cancel', onPress: () => {
                            null
                        }
                    }, {
                        text: 'Oke', onPress: () => {
                            AsyncStorage.setItem('konsumen', '');
                            AsyncStorage.setItem('owner', '');
                            AsyncStorage.setItem('kurir', '');
                            this.changePage('SignIn', true);
                            // this.props.navigation.reset()
                            Url.logOut(this.state.token, this.state.deviceId).then((response) => {
                                // console.log('response logout', response)
                                AsyncStorage.setItem('konsumen', '');
                                Redux.component.setUser(null);
                                let message = response.data.message;
                                if (message != null) {
                                    this.message(message, 'info')
                                }
                            }).catch((error) => {
                                // console.log(error, error.response);
                                AsyncStorage.setItem('konsumen', '');
                                AsyncStorage.setItem('owner', '');
                                AsyncStorage.setItem('kurir', '');
                                Redux.component.setUser(null);
                                if (error.response == null) {
                                    this.message("Network Error.", 'warning')

                                    this.setState({ isLoading: false });
                                }
                                this.root.message(error.response.data.message, 'warning');
                            });

                        }
                    }],
                { cancelable: false }
            )
        } catch (error) {
            // Error saving data
        }
    }
    forceOut() {
        try {
            AsyncStorage.setItem('konsumen', '');
            Redux.component.setUser(null);
            this.setState({ page: 'SignIn' })
            Url.logOut(this.state.token, this.state.deviceId).then((response) => {
                // console.log(response)
                AsyncStorage.setItem('konsumen', '');
                let message = response.data.message;
                if (message != null) {
                    this.message(message, 'info')
                }
                Redux.component.setUser(null);
                this.setState({ page: 'SignIn' })
                this.changePage('SignIn', true);
                this.props.navigation.reset()
            })
        } catch (error) {
            // Error saving data
        }
    }
    async saveData(data) {
        if (data != null) {
            let dataString = JSON.stringify(data);
            try {
                await AsyncStorage.setItem('konsumen', dataString);
                this.loadToken();
                this.requestCameraPermission();
            } catch (error) {
                this.requestCameraPermission();
                // Error saving data
                // console.log('Error Ternyata', error)
            }
        }

    }
    async loadToken() {
        try {
            let userString = await AsyncStorage.getItem('konsumen');
            if (userString == null) {
                this.changePage('SignIn', true);
                console.log('data object :', userString);
                this.loadTokenOwner();
            } else {
                let dataObject = JSON.parse(userString);
                console.log('data object :', dataObject);
                let user = dataObject;
                let token = dataObject.user.token
                Redux.component.setUser(user);
                this.setState({ backStack: [], token });
                this.changePage('Mainboard', true);
                AsyncStorage.setItem('isOpen', '');
                this.cekStorageNotification();
            }
        } catch (error) {
            // Error retrieving data
            // console.log('error:', error);
            this.changePage('SignIn', true);
        }
    }
    async saveDataOwner(data) {
        if (data != null) {
            let dataString = JSON.stringify(data);
            try {
                await AsyncStorage.setItem('owner', dataString);
                this.loadTokenOwner();
                this.requestCameraPermission();
            } catch (error) {
                this.requestCameraPermission();
                // Error saving data
                // console.log('Error Ternyata', error)
            }
        }

    }
    async loadTokenOwner() {
        try {
            let userString = await AsyncStorage.getItem('owner');
            if (userString == null) {
                this.changePage('SignIn', true);
                console.log('data object :', userString);
            } else {
                let dataObject = JSON.parse(userString);
                console.log('data object :', dataObject);
                let user = dataObject;
                let token = dataObject.user.token
                Redux.component.setUser(user);
                this.setState({ backStack: [], token });
                this.changePage('MainboardOwner', true);
                AsyncStorage.setItem('isOpen', '');
                this.cekStorageNotification();
            }
        } catch (error) {
            // Error retrieving data
            // console.log('error:', error);
            this.changePage('SignIn', true);
        }
    }
    render() {

        return (
            <View style={{ flex: 1 }}>
                {this.modalLoading()}
                {this.state.selectedPage == 'IntroSlider' && <IntroSlider />}
                {this.state.selectedPage == 'SplashScreen' && <SplashScreen />}
                {/* user */}
                {this.state.selectedPage == 'SignIn' && <SignIn />}
                {this.state.selectedPage == 'SignUp' && <SignUp />}
                {this.state.selectedPage == 'Mainboard' && <Mainboard />}
                {/* owner */}
                {this.state.selectedPage == 'SignInOwner' && <SignInOwner />}
                {this.state.selectedPage == 'SignUpOwner' && <SignUpOwner />}
                {this.state.selectedPage == 'MainboardOwner' && <MainboardOwner />}
                {/* kurir */}
                {this.state.selectedPage == 'SignInKurir' && <SignInKurir />}
            </View>
        );
    }
}

const styles = {
    drawerStyle: {
        drawer: { shadowColor: '#000000', shadowOpacity: 0.8, shadowRadius: 3 },
        // main: {paddingLeft: 3 },
    }
};

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(Root);