import axios from 'axios';
import qs from 'qs';

export default {
    base_url: 'https://cucik.aplikasitrial.com/api',

    axiosGet(url, params = {}, token = null) {
        if (token != null) {
            let config = {
                headers: {
                    'Accept': 'application/json',
                    'Authorization': "Bearer " + token,
                }
            };
            // console.log("URL", url + "?" + qs.stringify(params));
            return axios.get(url + "?" + qs.stringify(params), config);
        } else {
            // console.log("URL", url + "?" + qs.stringify(params));
            return axios.get(url + "?" + qs.stringify(params));
        }
    },

    axiosPost(url, params = {}, token = null) {
        if (token != null) {
            let config = {
                headers: {
                    'Accept': 'application/json',
                    'Authorization': "Bearer " + token,
                }
            };
            return axios.post(url, qs.stringify(params), config);
        } else {
            return axios.post(url, qs.stringify(params));
        }
    },
    axiosPut(url, params = {}, token = null) {
        if (token != null) {
            let config = {
                headers: {
                    'Accept': 'application/json',
                    'Authorization': "Bearer " + token,
                }
            };
            return axios.put(url, qs.stringify(params), config);
        } else {
            return axios.put(url, qs.stringify(params));
        }
    },
    axiosDelete(url, params = {}, token = null) {
        if (token != null) {
            let config = {
                headers: {
                    'Accept': 'application/json',
                    'Authorization': "Bearer " + token,
                }
            };
            return axios.delete(url, qs.stringify(params), config);
        } else {
            return axios.delete(url, qs.stringify(params));
        }
    },
    //umum
    uploadFoto(file) {
        return axios.post(this.base_url + '/umum/file/upload-photo', qs.stringify({
            file
        }));
    },
    logOut(token, device_id) {
        // console.log('Keluar ', token, device_id)
        return axios.get(this.base_url + '/auth/logout?' + qs.stringify({
            token, device_id
        }));
    },
    getDataUser(token) {
        // console.log('Keluar ', token, device_id)
        return axios.get(this.base_url + '/auth/profil?token=' + token);
    },
    login(username, password, device_id, role) {
        // console.log(username, password, device_id)
        return axios.post(this.base_url + '/auth/login?device_id=' + device_id, qs.stringify({
            username, password, role
        }));
    },
    // customer
    daftarCustomer(username, password, nama, telepon, alamat, gender, tempat_lahir, tanggal_lahir) {
        // console.log(username, password, nama, telepon, alamat,  gender, tempat_lahir, tanggal_lahir)
        return axios.post(this.base_url + '/auth/daftar-customer', qs.stringify({
            username, password, nama, telepon, alamat, gender, tempat_lahir, tanggal_lahir
        }));
    },
    cabangTerdekat(token, latitude, longitude) {
        // console.log(username, password, nama, telepon, alamat,  gender, tempat_lahir, tanggal_lahir)
        return axios.post(this.base_url + '/order/cari-cabang?token=' + token, qs.stringify({
            latitude, longitude
        }));
    },
    // owner
    daftarOwner(username, password, nama, foto, telepon, alamat, gender, tempat_lahir, tanggal_lahir, nama_usaha, alamat_usaha, telepon_usaha, jenis, foto_depan) {
        console.log(username, password, nama, foto, telepon, alamat, foto, gender, tempat_lahir, tanggal_lahir, nama_usaha, alamat_usaha, telepon_usaha, jenis, foto_depan)
        return axios.post(this.base_url + '/auth/daftar-owner', qs.stringify({
            username, password, nama, foto, telepon, alamat, gender, tempat_lahir, tanggal_lahir, nama_usaha, alamat_usaha, telepon_usaha, jenis, foto_depan
        }));
    },
    getCabang(token) {
        return axios.post(this.base_url + '/owner/lihat-cabang?token=' + token);
    },
    tambahCabang(token, nama, alamat_cabang, latitude, longitude, telepon) {
        console.log(token, nama, alamat_cabang, latitude, longitude, telepon)
        return axios.post(this.base_url + '/owner/tambah-cabang?token=' + token, qs.stringify({
            nama, alamat_cabang, latitude, longitude, telepon
        }));
    },
    ubahCabang(cabang_id, nama, alamat_cabang, latitude, longitude, telepon) {
        console.log(cabang_id, nama, alamat_cabang, latitude, longitude, telepon)
        return axios.post(this.base_url + '/owner/ubah-cabang?cabang_id=' + cabang_id, qs.stringify({
            nama, alamat_cabang, latitude, longitude, telepon
        }));
    },
    detailCabang(cabang_id) {
        return axios.post(this.base_url + '/owner/detail-cabang?cabang_id=' + cabang_id);
    },
    hapusCabang(cabang_id) {
        return axios.post(this.base_url + '/owner/hapus-cabang?cabang_id=' + cabang_id);
    },
    getCabang(token) {
        return axios.post(this.base_url + '/owner/lihat-cabang?token=' + token);
    },
    // kurir
    getKurir(cabang_id) {
        return axios.post(this.base_url + '/owner/lihat-kurir-cabang?cabang_id=' + cabang_id);
    },
    tambahKurir(token, nama, jam_selesai, jam_mulai, password, cabang_id, telepon) {
        console.log(token, nama, jam_selesai, jam_mulai, password, cabang_id, telepon)
        return axios.post(this.base_url + '/owner/tambah-kurir?token=' + token, qs.stringify({
            nama, jam_selesai, jam_mulai, password, cabang_id, telepon
        }));
    },
    ubahKurir(kurir_id, nama, jam_selesai, jam_mulai, password, telepon) {
        console.log(kurir_id, nama, jam_selesai, jam_mulai, password, telepon)
        return axios.post(this.base_url + '/owner/ubah-kurir?kurir_id=' + kurir_id, qs.stringify({
            nama, jam_selesai, jam_mulai, password, telepon
        }));
    },
    detailKurir(kurir_id) {
        return axios.post(this.base_url + '/owner/detail-kurir?kurir_id=' + kurir_id);
    },
    hapusKurir(kurir_id) {
        return axios.post(this.base_url + '/owner/hapus-kurir?kurir_id=' + kurir_id);
    },
    // layanan
    getLayanan(cabang_id) {
        return axios.post(this.base_url + '/owner/lihat-layanan-cabang?cabang_id=' + cabang_id);
    },
    tambahLayanan(token, cabang_id, nama, keterangan, satuan, lama_waktu, harga, promo) {
        console.log(token, cabang_id, nama, keterangan, satuan, lama_waktu, harga, promo)
        return axios.post(this.base_url + '/owner/tambah-layanan?token=' + token, qs.stringify({
            cabang_id, nama, keterangan, satuan, lama_waktu, harga, promo
        }));
    },
    ubahLayanan(layanan_id, cabang_id, nama, keterangan, satuan, lama_waktu, harga, promo) {
        console.log(layanan_id, cabang_id, nama, keterangan, satuan, lama_waktu, harga, promo)
        return axios.post(this.base_url + '/owner/ubah-layanan?layanan_id=' + layanan_id, qs.stringify({
            cabang_id, nama, keterangan, satuan, lama_waktu, harga, promo
        }));
    },
    detailLayanan(layanan_id) {
        return axios.post(this.base_url + '/owner/detail-layanan?layanan_id=' + layanan_id);
    },
    hapusLayanan(layanan_id) {
        return axios.post(this.base_url + '/owner/hapus-layanan?layanan_id=' + layanan_id);
    },
}

