export default {
  
  primary: '#3BD0E4',
  // header: '#003948',

  button: '#3BD0E4',
  primaryButton: '#3BD0E4',
  secondaryButton: '#3BD0E4',
  primaryFont: '#545454',
  secondaryFont: '#F5F5F5',

  grayWhite:'#F9F9F9',
  gray: '#EEEEEE',
  grayFill: '#9E9E9E',
  grayDarkFill: '#606060',
  yellowFill: '#FFEB3B',
  greenFill: '#4CAF50',
  redFill: '#F44336',
  blueFill: '#2196F3',

  white: '#fff',
  whiteOpacity: 'rgba(255,255,255,0.5)',
  black: '#000',
  blackOpacity: 'rgba(0,0,0,0.3)',

  //pie chart color
  bluePie: '#09DEFD',
  redPie: '#F0582F',
  greenPie: '#71D456',

  danger:'#CC0000',
  warning:'#FF8800',
  success:'#007E33',
  info:'#0099CC',

  facebook: '#3D5A96',
  whatsapp: '#00d856',
  instagram: '#006064',
  youtube: '#F52929',
  twitter: '#00a1f8',
  placeholder: '#ddd',
}