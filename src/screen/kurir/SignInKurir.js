import { dispatch as Redux } from '@rematch/core';
import React, { Component } from 'react';
import { ActivityIndicator, Alert, AsyncStorage, Dimensions, Image, ScrollView, StatusBar, Text, View } from 'react-native';
import { connect } from 'react-redux';
import Button from '../component/Button';
import InputWithLabel from '../component/InputWithLabel';
import TextButton from '../component/TextButton';
import Color from '../utility/Color.js';
import Styles from '../utility/Style.js';
import Url from '../utility/Url.js';

const { width } = Dimensions.get('window')

class SignInKurir extends Component {
    constructor(props) {
        super(props)
        this.root = this.props.component.root;
        this.rootState = this.root.state.params;
        this.state = {
            isLoading: false,
            modalIntro: false,
            username: '',
            password: '',
            imei: null,
            show: true,
            fontPassword: 14,
            fontPassword2: 35,
            // console: null,
            currentUser: null,
            token: null,
            dataIntro: [
                {
                    step: 1,
                    height: 200,
                    topPosition: 200,
                    description: "Jika kamu sudah punya akun, silahkan masuk disini. "
                },
                {
                    step: 2,
                    height: 50,
                    topPosition: 440,
                    description: "Jika kamu belum punya akun, silahkan daftar disini. "
                },
                {
                    step: 3,
                    height: 50,
                    topPosition: 510,
                    description: "atau kamu juga bisa menggunakan Facebook dan Google untuk melanjutkan. "
                },
            ],
        }
        Redux.component.setSignIn(this);
    }

    componentDidMount() {
        // AsyncStorage.setItem('SignInIntro', '')
        this.cekStorageIntro();

    }

    async cekStorageIntro() {
        try {
            let isIntro = await AsyncStorage.getItem('SignInIntro');
            if (isIntro == null) {
                this.setState({ modalIntro: true })
            } else {
                this.setState({ modalIntro: false })
            }
        } catch (error) {
            // Error retrieving data
            // console.log('error:', error);
        }
    }

    showPassword() {
        if (this.state.password.length < 1) {
            Alert.alert(
                'Warning',
                'Isi Password Dahulu',
                [
                    {
                        text: 'Oke', onPress: () => { null }
                    }],
                { cancelable: false }
            )
        } else {
            if (this.state.show == true) {
                this.setState({ show: false, fontPassword: 14, fontPassword2: 14 });
            } else {
                this.setState({ show: true, fontPassword: 30, fontPassword2: 35 });
            }
        }
    }

    signIn() {
        this.root.changePage('Mainboard')
        // let { username, password } = this.state
        // if (username.length != '') {
        //     this.setState({ isLoading: true });
        //     Url.login(username, password, this.root.state.deviceId).then((response) => {
        //         let data = response.data.data
        //         this.setState({ isLoading: false });
        //         if (data != null) {
        //             // console.log('data login:', data);
        //             this.root.saveData(data)
        //             this.root.message("Berhasil Masuk.", 'success')
        //         }
        //     }).catch((error) => {
        //         // console.log(error, error.response);
        //         if (error.response == null) {
        //             this.root.message("Network Error.", 'warning')
        //             this.setState({ isLoading: false });
        //         }
        //         this.root.message(error.response.data.message, 'warning');
        //         this.setState({ isLoading: false });
        //     });
        // }
    }
    getProfile(token) {
        Url.getProfile(token).then((response) => {
            let data = response.data.data;
            data.token = token
            this.setState({ isLoading: false, token });
            this.root.saveData(data)
            // console.log('data profile:', data);
        }).catch((error) => {
            // console.log(error, error.response);
            this.setState({ isLoading: false, });
            if (error.response == null) {
                this.root.message("Network Error.", 'warning')
                this.setState({ isLoading: false });
            }
            this.root.message(error.response.data.message, 'warning');
        });
    }

    registerHere() {
        this.root.navigate('SignUp')
    }
    lupaPassword() {
        null
    }
    render() {
        let deviceId = this.root.state.deviceId
        return (
            <View style={[Styles.container, { paddingTop: 20 }]}>
                <StatusBar
                    backgroundColor={Color.primary}
                    barStyle="light-content"
                    style={{ opacity: 0.9 }}
                />
                <ScrollView
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    style={{ flex: 1, backgroundColor: Color.white }}
                    keyboardShouldPersistTaps='always'>
                    <Image
                        source={require('../../../img/undraw_deliveries.png')}
                        style={[Styles.center, { resizeMode: 'contain', alignSelf: 'center', width: width / 1.5, height: width / 2.5, marginVertical: 20 }]}
                    />
                    <View style={[Styles.content, { flex: 1, paddingHorizontal: 20, paddingVertical: 20 }]}>
                        <Text style={[Styles.primaryFontBold]}>Username/Email</Text>
                        <InputWithLabel
                            // styleContainer={{ paddingLeft: 20 }}
                            labelColor={Color.black}
                            placeholder="Your Username/Email"
                            returnKeyType={"next"}
                            // ref={(input) => this.password = input}
                            // onSubmitEditing={() => this.password.focus()}
                            onChangeText={username => { this.setState({ username }) }}
                            clearText={() => { this.setState({ username: '' }) }}
                            // style={}
                            defaultValue={this.state.username}
                        // underlineColorAndroid={}
                        />

                        <Text style={[Styles.primaryFontBold]}>Password</Text>
                        <InputWithLabel
                            // styleContainer={{ paddingLeft: 20 }}
                            labelColor={Color.black}
                            placeholder="Password"
                            returnKeyType={"next"}
                            // ref={(input) => this.password = input}
                            // onSubmitEditing={() => this.password.focus()}
                            onChangeText={password => { this.setState({ password }) }}
                            clearText={() => { this.setState({ password: '' }) }}
                            // style={}
                            defaultValue={this.state.password}
                            // underlineColorAndroid={}
                            iconRight={this.state.password.length >= 1 ? true : false}
                            secureTextEntry={this.state.show}
                            iconRightName={this.state.show == true ? 'md-eye' : 'md-eye-off'}
                            iconPressed={() => { this.showPassword() }}
                        />
                        {this.state.isLoading == true && (
                            <ActivityIndicator />
                        )}
                        {/* <NoData iconName={"emoticon-tongue"}><Text>Pilih Ujian</Text></NoData> */}
                        {this.state.isLoading == false && (
                            <View>
                                <Button
                                    labelButton="Masuk"
                                    onPress={() => this.signIn()}
                                    backgroundColor={Color.redPie}
                                // iconLeft={true}
                                // iconLeftName="md-eye"
                                />

                                <View style={{ width: '100%', height: 1, marginVertical: 20, borderColor: Color.gray, borderWidth: 1 }} />

                                <Button
                                    labelButton="Masuk sebagai User"
                                    onPress={() => this.root.changePage('SignIn')}
                                    backgroundColor={Color.primary}
                                    labelColor={Color.white}
                                    borderColor={Color.primary}
                                    borderRadius={30}
                                // iconLeft={true}
                                // iconLeftName="md-eye"
                                />
                                <Button
                                    labelButton="Masuk sebagai Owner"
                                    onPress={() => this.root.changePage('SignInOwner')}
                                    backgroundColor={Color.greenPie}
                                    labelColor={Color.white}
                                    borderColor={Color.white}
                                    borderRadius={30}
                                // iconLeft={true}
                                // iconLeftName="md-eye"
                                />
                            </View>
                        )}
                    </View>
                </ScrollView>
                {/* {this.state.modalIntro == true && (
                    <ModalIntro
                        setStorage={() => AsyncStorage.setItem('SignInIntro', 'sudah')}
                        data={this.state.dataIntro}
                    />
                )} */}
                <View style={{ width, height: 56, paddingHorizontal: 8, justifyContent: 'center', alignItems: 'flex-end', backgroundColor: Color.grayWhite }}>
                    <Image
                        source={require('../../../img/logo_full.png')}
                        style={[Styles.center, { resizeMode: 'contain', width: 100, height: 50 }]}
                    />
                </View>
            </View>
        )
    }
}


function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(SignInKurir);