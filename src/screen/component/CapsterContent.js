import React, { Component } from 'react';
import { Dimensions, Image, Text, TouchableOpacity, View } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import Color from '../utility/Color.js';
import Styles from '../utility/Style.js';
const { width } = Dimensions.get('window')

class CapsterContent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            show: false,
        }
        this.root = this.props.component.root;
    }
    render() {
        let { limitString } = this.state
        return (
            <TouchableOpacity activeOpacity={0.8} style={[styles.box, this.props.styleContainer, { backgroundColor: this.props.backgroundColor != null ? this.props.backgroundColor : Color.white, borderRadius: this.props.radiusImage != null ? this.props.radiusImage : 10 }]} onPress={this.props.onPress}>
                {this.props.sourceImage != null && (
                    <View style={[Styles.column, { width: '100%', height: 150 }]}>
                        <Image
                            // source={require('../../../img/background.jpg')}
                            resizeMode="contain"
                            source={this.props.sourceImage}
                            style={[styles.circlePhoto, { width: this.props.imageSize, height: this.props.imageSize, borderRadius: this.props.radiusImage != null ? this.props.radiusImage : this.props.imageSize }]}
                        />
                    </View>
                )}
                <View style={[Styles.rowBetween, { paddingHorizontal: 10 }]}>
                    <View>
                        {this.props.name != null && (
                            <Text style={[Styles.whiteText, { fontSize: 12, textAlign: this.props.textAlign != null ? this.props.textAlign:'center', color: this.props.nameTextColor != null ? this.props.nameTextColor : Color.white }]}>{this.props.name.length >= Math.round(width / 20) && this.props.cutName == null ? this.props.name.substring(0, Math.round(width / 20)) + ".." : this.props.name}</Text>
                        )}
                        {this.props.childText != null && (
                            <Text style={[Styles.whiteText, { fontSize: 10, textAlign: this.props.textAlign, color: this.props.childTextColor != null ? this.props.childTextColor : Color.white }]}>{this.props.childText.length >= Math.round(width / 8) ? this.props.childText.substring(0, Math.round(width / 8)) + ".." : this.props.childText}</Text>
                        )}
                        {this.props.rateText != null && (
                            <View style={[Styles.row, { marginVertical: 3, justifyContent: this.props.rateJustify }]}>
                                <View style={{ marginRight: 7, paddingHorizontal: 5, alignItems: 'center', flexDirection: 'row', backgroundColor: this.props.rateTextBackColor, borderRadius: 10, }}>
                                    <Ionicons name="ios-star"
                                        color={Color.white}
                                        size={10}
                                        style={{ alignSelf: 'center', marginRight: 5 }}
                                    />
                                    <Text style={[Styles.whiteText, { fontSize: 10, textAlign: 'center', textAlign: this.props.textAlign, color: this.props.rateTextColor != null ? this.props.rateTextColor : Color.white }]}>{this.props.rateText}</Text>
                                </View>
                                <Text style={[Styles.blackText, { fontSize: 9, textAlign: 'left', color:this.props.jarakTextColor != null? this.props.jarakTextColor: Color.grayDarkFill }]}>{this.props.jarakText}</Text>
                            </View>
                        )}
                        {this.props.otherText != null && (
                            <Text style={[Styles.blackText, { fontSize: 10, textAlign: this.props.textAlign, color: this.props.otherTextColor != null ? this.props.otherTextColor : Color.grayDarkFill }]}>{this.props.otherText.length >= Math.round(width / 9) ? this.props.otherText.substring(0, Math.round(width / 9)) + ".." : this.props.otherText}</Text>
                        )}
                        {this.props.subChildText != null && (
                            <Text style={[Styles.blackText, { flexWrap: "wrap", fontSize: 12, textAlign: this.props.textAlign, color: this.props.subChildTextColor != null ? this.props.subChildTextColor : Color.white }]}>{this.props.subChildText}</Text>
                        )}
                        {this.props.contact != null && (
                            <View style={styles.contact}>
                                <Ionicons name="md-person"
                                    color={Color.white}
                                    size={12}
                                    style={{ alignSelf: 'center', marginRight: 3 }}
                                />
                                <Text style={[Styles.whiteText, { fontSize: 12 }]}>{this.props.contact.substr(0, 7)}..</Text>
                            </View>
                        )}
                    </View>
                    {this.props.iconRight == true && (
                        <View style={styles.icon}>
                            {this.props.iconRightName != null && (
                                <Ionicons name={this.props.iconRightName}
                                    color={this.props.iconColor != null ? this.props.iconColor : Color.white}
                                    size={23}
                                    style={{ alignSelf: 'center' }}
                                />
                            )}
                            {this.props.materialIconRightName != null && (
                                <View style={[styles.circlePhoto, { backgroundColor: this.props.iconBackgroundColor != null ? this.props.iconBackgroundColor : null }]}>
                                    <MaterialIcons name={this.props.materialIconRightName}
                                        color={this.props.iconColor != null ? this.props.iconColor : Color.white}
                                        size={25}
                                        style={{ alignSelf: 'center' }}
                                    />
                                </View>
                            )}
                        </View>
                    )}
                    {this.props.textRight == true && (
                        <View style={styles.textRight}>
                            {this.props.textRightValue != null && (
                                <Text style={[Styles.whiteText, { padding: this.props.textRightBackground != null ? 5 : 0, backgroundColor: this.props.textRightBackground != null ? this.props.textRightBackground : Color.white, color: this.props.textRightColor != null ? this.props.textRightColor : Color.black, fontSize: 12 }]}>{this.props.textRightValue}</Text>
                            )}
                        </View>
                    )}
                </View>
            </TouchableOpacity>
        )
    }
}
const styles = {
    box: {
        width: '45%', flexDirection: 'column',
        backgroundColor: Color.grayWhite,
        paddingBottom: 15,
        margin: 2,
        // marginBottom: 2,
        borderColor: Color.grayWhite, borderWidth: 1,
        // justifyContent: 'center', alignItems: 'center'
    },
    contact: {
        width: 70,
        flexDirection: 'row', borderRadius: 5,
        backgroundColor: Color.secondaryButton, padding: 2,
        marginVertical: 2,
        justifyContent: 'center', alignItems: 'center'
    },
    circlePhoto: {
        width: 55, height: 55,
        borderRadius: 10,
        //  borderWidth: 1, borderColor: Color.gray,
        marginBottom: 5,
        justifyContent: 'center', alignItems: 'center'
    },
    icon: {
        width: 30, height: 30, marginHorizontal: 5,
        justifyContent: 'center', alignItems: 'center', alignSelf: 'center'
    },
    textRight: {
        flex: 1,
        justifyContent: 'center', alignItems: 'flex-end', alignSelf: 'center'
    }
}

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(CapsterContent);