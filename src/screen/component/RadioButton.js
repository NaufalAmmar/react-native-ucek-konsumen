import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux';
import Color from '../utility/Color';
import Styles from '../utility/Style';

class RadioButton extends Component {
    constructor(props) {
        super(props)
        this.state = {
            selected: this.props.selectedValue
        }
        this.root = this.props.component.root;
    }
    pressed(value) {
        this.props.onSelected(value)
        this.setState({ selected: value })
    }
    render() {
        return (
            <View style={{ flex: 1, marginBottom: 12, }}>
                {this.props.label != null && (
                    <Text style={[Styles.bigPrimaryFontTextBold, { marginBottom: 12 }]}>{this.props.label}</Text>
                )}
                <View style={Styles.column}>
                    {this.props.data.map((item, index) => {
                        return (
                            <TouchableOpacity key={index} style={[Styles.rowCenter, { justifyContent: 'flex-start', marginHorizontal: 10, marginVertical: 8 }]} onPress={() => this.pressed(item.value)}>
                                <View style={styles.circle}>
                                    {
                                        this.state.selected == item.value ?
                                            < View style={{
                                                height: 8,
                                                width: 8,
                                                borderRadius: 4,
                                                backgroundColor: Color.primary,
                                            }} />
                                            : null
                                    }
                                </View>
                                <Text style={[Styles.blackText, { fontSize: 12 }]}>{item.label}</Text>
                            </TouchableOpacity>
                        )
                    })}
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    circle: {
        height: 16,
        width: 16,
        borderRadius: 8,
        borderWidth: 2,
        borderColor: Color.primary,
        alignItems: 'center',
        justifyContent: 'center', marginRight: 5
    }
})

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(RadioButton);