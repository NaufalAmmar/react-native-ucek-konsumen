import React, { Component } from 'react';
import { ScrollView, Text, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux';
import Color from '../utility/Color.js';
import Styles from '../utility/Style.js';

class FootScroll extends Component {
    constructor(props) {
        super(props)
        this.state = {
            clicked: 0,
        }
        this.root = this.props.component.root;
    }
    render() {
        let { clicked } = this.state;
        let footer = this.props.data.map((item, index) => {
            let warna = Color.white
            if (item.visited != null && item.selected_id != null) {
                warna = Color.primary
                if (this.state.clicked == index && item.visited != null && item.selected_id != null) {
                    warna = Color.primaryButton
                }
            }
            else if (this.state.clicked == index && item.visited != null && item.selected_id == null) {
                warna = Color.primaryButton
            } else if (this.state.clicked == index && item.visited == null && item.selected_id == null) {
                warna = Color.primaryButton
            }
            else if (item.visited != null && item.selected_id == null) {
                warna = Color.redPie
            } else {
                warna = Color.white
            }
            return (
                <TouchableOpacity activeOpacity={0.8} key={index} style={[styles.box, { backgroundColor: warna }]}
                    onPress={() => {
                        this.setState({ clicked: index });
                        this.props.onChange(index, index, item);
                        // alert(this.state.clicked, item.visited, item.selected_id)
                    }}>
                    <Text style={[Styles.blackText, { fontSize: 14, color: this.state.clicked == index ? Color.white : Color.button }]}>{index + 1}</Text>
                </TouchableOpacity>
            )
        })       
        // console.log('footeer data',footer, footer.length)
        return (
            <View style={{ flex: 1, position: 'absolute', bottom: 0, right: 0, left: 0, borderTopColor: Color.gray, borderTopWidth: 1 }}>
                <ScrollView style={styles.menu} showsHorizontalScrollIndicator={false} horizontal={true}>
                    {footer}
                </ScrollView>
                <View style={Styles.row}>
                    <TouchableOpacity
                        style={[styles.buttonPrev, { backgroundColor: this.state.clicked == 0 ? Color.white : Color.grayFill }]}
                        disabled={this.state.clicked == 0 ? true : false}
                        onPress={() => {
                            this.setState({ clicked: clicked - 1 });
                            this.props.onChange(clicked - 1, clicked);
                        }}>
                        <Text style={[Styles.whiteText, { textAlign: 'left' }]}>Previous</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.buttonNext, { backgroundColor: this.state.clicked + 1 == this.props.data.length ? Color.white : Color.greenPie }]}
                        disabled={this.state.clicked + 1 == this.props.data.length ? true : false}
                        onPress={() => {
                            this.setState({ clicked: clicked + 1 });
                            this.props.onChange(clicked + 1, clicked);
                        }}>
                        <Text style={[Styles.whiteText, { textAlign: 'right' }]}>Next</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}
const styles = {
    menu: {
        backgroundColor: Color.white
    },
    box: {
        flex: 1, height: 40,
        flexDirection: 'row',
        paddingVertical: 5,
        paddingHorizontal: 15,
        // marginHorizontal: 5,
        // borderRadius: 5,
        justifyContent: 'center', alignItems: 'center'
    },
    buttonPrev: {
        flex: 1, padding: 20, backgroundColor: Color.grayFill,
    },
    buttonNext: {
        flex: 1, padding: 20, backgroundColor: Color.greenPie,
    }
}

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(FootScroll);