import React, { Component } from 'react';
import { Dimensions, Text, View } from 'react-native';
import SearchInput from 'react-native-search-filter';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import Color from '../utility/Color';
import Styles from '../utility/Style';
import NoData from './NoData';
const { width } = Dimensions.get('window')

const KEYS_TO_FILTERS = ['name'];
class ListLeaderboard extends Component {
    constructor(props) {
        super(props)
        this.root = this.props.component.root;
        this.state = {
            text: ''
        }
    }
    searchText(input) {
        this.setState({ text: input })
        if (input.length == 0) {
            this.props.search(this.state.text)
        }
    }
    searchSubmit(input) {
        this.props.search(this.state.text)
    }
    render() {
        return (
            <View style={[Styles.column, { borderTopColor: Color.gray, borderTopWidth: 1, backgroundColor: Color.white, paddingVertical: 5, marginVertical: 5 }]}>
                <View style={{ paddingHorizontal: 20, }}>
                    <View style={styles.search}>
                        <MaterialIcons
                            name="search"
                            color="gray"
                            size={24}
                        />
                        <View style={styles.searchInput}>
                            <SearchInput
                                onSubmitEditing={() => this.searchSubmit()}
                                onChangeText={(input) => { this.searchText(input) }}
                                placeholder="Cari Nama"
                                clearIconViewStyles={{ position: 'absolute', right: 10, top: 10 }}
                                clearIcon={this.state.text !== '' && <MaterialIcons name="close" size={20} />}
                            />
                        </View>
                    </View>
                </View>
                <View style={[Styles.rowBetween, { paddingVertical: 5, paddingHorizontal: 10, }]}>
                    {/* <Text style={[Styles.primaryTextBold]}>No. </Text> */}
                    <Text style={[Styles.primaryTextBold, { flex: 1, fontSize: width / 50, textAlign: 'center' }]}>User</Text>
                    <Text style={[Styles.primaryTextBold, { flex: 1, fontSize: width / 50, textAlign: 'center' }]}>PG</Text>
                    <Text style={[Styles.primaryTextBold, { flex: 1, fontSize: width / 50, textAlign: 'center' }]}>Rank PG</Text>
                    <Text style={[Styles.primaryTextBold, { flex: 1, fontSize: width / 50, textAlign: 'center' }]}>Pilihan 1</Text>
                    <Text style={[Styles.primaryTextBold, { flex: 1, fontSize: width / 50, textAlign: 'center' }]}>Pilihan 2</Text>
                </View>
                {this.props.data.length == 0 && (
                    <NoData><Text>Data Kosong</Text></NoData>
                )}
                {this.props.data.map((item, index) => {
                    return (
                        <View key={index} style={[Styles.rowBetween, { backgroundColor: index % 2 == 0 ? Color.gray : Color.white, paddingHorizontal: 10, paddingVertical: 10, }]}>
                            {/* <Text style={[Styles.primaryText]}>{index + 1}</Text> */}
                            <Text style={[Styles.primaryFontText, { flex: 1, fontSize: width / 50, textAlign: 'center' }]}>{item.user.name.length > 27 ? item.user.name.substr(0, 28) + ".." : item.user.name}</Text>
                            <Text style={[Styles.primaryFontText, { flex: 1, fontSize: width / 50, textAlign: 'center' }]}>{item.persen}</Text>
                            <Text style={[Styles.primaryFontText, { flex: 1, fontSize: width / 50, textAlign: 'center' }]}>{item.rank}</Text>
                            <View style={{ flex: 1 }}>
                                {item.passing_grade.length > 0 &&
                                    item.passing_grade.map((i, index) => {
                                        if (index == 0)
                                            return (
                                                <Text key={index} style={[Styles.primaryFontText, { backgroundColor: i.percentage != null ? i.percentage > item.persen ? Color.redFill : Color.greenPie : 'transparent', padding: 3, flex: 1, fontSize: width / 50, textAlign: 'center' }]}>{i.name}</Text>
                                            )
                                    })}
                            </View>
                            <View style={{ flex: 1,marginLeft:5 }}>
                                {item.passing_grade.length > 0 &&
                                    item.passing_grade.map((i, index) => {
                                        if (index == 1)
                                            return (
                                                <Text key={index} style={[Styles.primaryFontText, { backgroundColor: i.percentage != null ? i.percentage > item.persen ? Color.redFill : Color.greenPie : 'transparent', padding: 3, flex: 1, fontSize: width / 50, textAlign: 'center' }]}>{i.name}</Text>
                                            )
                                    })}
                            </View>
                        </View>
                    )
                })}
            </View>
        );
    }
}

const styles = {
    search: {
        width: '100%', flexDirection: 'row',
        height: 50,
        alignItems: 'center',
        alignSelf: 'center',
        borderWidth: 1, borderColor: Color.blackOpacity,
        marginHorizontal: 30, marginVertical: 10,
        paddingLeft: 5
    },
    searchInput: {
        flex: 1,
        padding: 5,
        color: Color.black,
    },
}
function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(ListLeaderboard);