import React, { Component } from 'react';
import { Animated, Easing, Modal, ScrollView, Text, TouchableOpacity, View, ActivityIndicator } from 'react-native';
import SearchInput, { createFilter } from 'react-native-search-filter';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import NoData from '../component/NoData';
import Color from '../utility/Color';
import Styles from '../utility/Style';

const KEYS_TO_FILTERS = ['name'];

const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    const paddingToBottom = 0;
    return layoutMeasurement.height + contentOffset.y + 20 >=
        contentSize.height - paddingToBottom;
};
class MultiSelect extends Component {
    constructor(props) {
        super(props)
        this.animatedValue = new Animated.Value(0)
        this.vendor = this.props.component.vendor;
        this.state = {
            show: false,
            searchText: '',
            selectedData: [],
            text: '',
            loadLoading: false,
            searching: false
        }
    }
    searchText(input) {
        this.setState({ text: input })
        if (input.length == 0) {
            this.props.search(input)
            this.setState({ searching: false })
        }
    }
    close() {
        this.setState({ show: false,searching: false });
        if (this.state.searching == true) {
            this.props.search('')
            this.setState({ searching: false })
        }
    }
    searchSubmit(input) {
        this.props.search(this.state.text)
        this.setState({ searching: true })
    }
    componentDidMount() {

        // console.log('selected multi', this.state.selectedData)
    }
    renderSelected() {
        let stringArr = [];
        this.props.data.forEach((item) => {
            let found = false;
            this.state.selectedData.forEach((uid) => {
                if (item.uid == uid) {
                    found = true;
                }
            });

            if (found) {
                stringArr.push(item.name);
            }
        });

        if (stringArr.length == 0) {
            return "Pilih Salah Satu Produk";
        }

        return stringArr.join(", ");
    }

    isUidIsSelected(uid) {
        let found = false;
        this.state.selectedData.forEach((__uid) => {
            // console.log('__uid', __uid);
            if (uid == __uid) {
                found = true;
            }
        });

        return found;
    }
    refreshScroll(event) {
        this.setState({ y: event.nativeEvent.contentOffset.y, scrollEnd: false, event })
    }
    animate() {
        this.animatedValue.setValue(0)
        Animated.timing(
            this.animatedValue,
            {
                toValue: 1,
                duration: 2000,
                useNativeDriver: true, 
                easing: Easing.linear
            }
        ).start(() => this.animate())
    }
    onScrollEnd() {
        let { y, scrollEnd, event, limit } = this.state
        this.setState({ scrollEnd: true })
        this.animate.bind(this)
        if (y == event.nativeEvent.contentOffset.y && scrollEnd == true) {
            this.setState({ limit: limit + 10 })
            alert("scroll end")
        }
    }
    load() {
        this.props.loadMore()
        this.setState({ loadLoading: true })
        setTimeout(() => {
            this.setState({ loadLoading: false })
        }, 300)
    }
    render() {
        let data = this.props.data
        const filteredData = data.filter(createFilter(this.state.searchText, KEYS_TO_FILTERS))
        let { searchText, selectedData } = this.state

        return (
            <View style={{ flex: 1, marginVertical: 5 }}>
                {this.props.show == null && (
                    <View>
                        <Text style={[Styles.blackText, { marginBottom: 5, color: this.props.labelColor != null ? this.props.labelColor : Color.black }]}>{this.props.label}</Text>
                        <TouchableOpacity style={[styles.button, this.props.styleContainer]} onPress={() => {
                            this.setState({ show: true });
                        }}>
                            <Text style={styles.buttonText} numberOfLines={1} ellipsizeMode='tail'>{this.renderSelected()}</Text>
                            <Ionicons name='md-arrow-dropdown' size={20} color='#000' />
                        </TouchableOpacity>
                    </View>
                )}
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.props.show != null ? this.props.show : this.state.show}
                    onRequestClose={this.props.onPressClose}>

                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <TouchableOpacity style={styles.modalRoot} activeOpacity={1} onPress={() => {
                            this.close()
                        }} />
                        <View style={{ flex: 1, backgroundColor: Color.white }}>
                            <ScrollView style={{ marginBottom: 40, paddingHorizontal: 20, paddingVertical: 10 }}
                                onScroll={({ nativeEvent }) => {
                                    // console.log(isCloseToBottom(nativeEvent))
                                    // console.log(nativeEvent)
                                    if (isCloseToBottom(nativeEvent)) {
                                        this.load()
                                    }
                                }}>

                                <TouchableOpacity onPress={this.props.onPressClose != null ? this.props.onPressClose : () => {
                                    this.close()
                                }}>
                                    <Text style={styles.close} > Close </Text>
                                </TouchableOpacity>
                                <View style={{ paddingHorizontal: 20 }}>
                                    <View style={styles.search}>
                                        <MaterialIcons
                                            name="search"
                                            color="gray"
                                            size={24}
                                        />
                                        <View style={styles.searchInput}>
                                            <SearchInput
                                                onSubmitEditing={() => this.searchSubmit()}
                                                onChangeText={(input) => { this.searchText(input) }}
                                                placeholder="Cari"
                                            // clearIconViewStyles={{ position: 'absolute', right: 10, top: 10 }}
                                            // clearIcon={this.state.text !== '' && <MaterialIcons name="close" size={20} />}
                                            />
                                        </View>
                                    </View>
                                </View>
                                {filteredData.length == 0 && (
                                    <NoData><Text>Belum ada data</Text></NoData>
                                )}
                                {filteredData.map((item, index) => {
                                    let icon = this.isUidIsSelected(item.id) ? 'md-checkmark-circle' : 'md-radio-button-off';
                                    return (

                                        <TouchableOpacity style={styles.item} key={index} onPress={() => {
                                            let selectedData = this.state.selectedData;
                                            // console.log(selectedData);
                                            if (selectedData.length >= this.props.maxSelected) {
                                                alert("Maksimal pilih " + this.props.maxSelected)
                                            } else {
                                                if (this.isUidIsSelected(item.id)) {
                                                    //remove
                                                    let index = -1;
                                                    selectedData.forEach((__uid, __index) => {
                                                        if (item.id == id) {
                                                            index = __index;
                                                        }
                                                    });
                                                    selectedData.splice(index, 1);
                                                    this.setState({ selectedData });
                                                } else {
                                                    //add
                                                    selectedData.push(item.id);
                                                    this.setState({ selectedData });
                                                }
                                                this.props.onChange(selectedData);
                                            }
                                        }}>
                                            <Ionicons name={icon} size={30} color={Color.primary} />
                                            <Text style={styles.itemText}>{item.name}</Text>
                                        </TouchableOpacity>
                                    );
                                })}
                                {this.state.loadLoading == true && (
                                    <ActivityIndicator color={Color.primary} />
                                )}
                                <View style={{ height: 20 }} />
                            </ScrollView>

                            <TouchableOpacity disabled={this.props.disabled != null ? this.props.disabled : false} onPress={this.props.selesaiPress != null ? this.props.selesaiPress : () => {
                                this.close()
                            }}>
                                <Text style={[styles.selesai, { backgroundColor: this.props.buttonColor != null ? this.props.buttonColor : Color.blue }]} > Selesai </Text>
                            </TouchableOpacity>
                        </View>
                        <TouchableOpacity style={styles.modalRoot} activeOpacity={1} onPress={() => null} />

                    </View>
                </Modal>
            </View>
        );
    }
}

const styles = {
    search: {
        width: '100%', flexDirection: 'row',
        height: 50,
        alignItems: 'center',
        alignSelf: 'center',
        borderWidth: 1, borderColor: Color.blackOpacity,
        marginHorizontal: 30, marginVertical: 10,
        paddingLeft: 5
    },
    searchInput: {
        flex: 1,
        padding: 5,
        color: Color.black,
    },
    button: {
        borderWidth: 1,
        borderColor: Color.gray,
        borderRadius: 5,
        paddingHorizontal: 24,
        height: 50,
        marginBottom: 10,
        alignItems: 'center',
        flexDirection: 'row'
    },
    buttonText: {
        color: '#000',
        fontSize: 14,
        flex: 1
    },
    modalRoot: {
        backgroundColor: 'rgba(0,0,0,0.3)',
        width: 30,
        // justifyContent: 'center',
        // alignItems: 'center'
    },
    modalContent: {
        // position: 'absolute', left: 30, right: 30,
        backgroundColor: '#fff',
        borderRadius: 8,
        padding: 10,
        flex: 1, paddingBottom: 100, paddingVertical: 10
        // marginHorizontal: 30
    },
    item: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 2
    },
    close: {
        textAlign: 'right'
    },
    selesai: {
        // position: 'absolute', bottom: 10, right: 50, left: 50,
        marginHorizontal: 20,
        marginBottom: 10,
        color: Color.white,
        backgroundColor: Color.primary, padding: 15,
        borderRadius: 10,
        textAlign: 'center'
    },
    itemText: {
        flex: 1,
        color: '#000',
        marginLeft: 10,
        fontSize: 15
    }
};

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(MultiSelect);