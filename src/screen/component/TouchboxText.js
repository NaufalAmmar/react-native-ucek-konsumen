import React, { Component } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux';
import Color from '../utility/Color.js';
import Styles from '../utility/Style.js';

class TouchboxText extends Component {
    constructor(props) {
        super(props)
        this.state = {
            show: false
        }
        this.root = this.props.component.root;
    }
    render() {
        return (
            <TouchableOpacity activeOpacity={0.8} style={[styles.buttonTransparent, Styles.rowBetween, { backgroundColor: this.props.backgroundColor }]} onPress={this.props.onPress}>
                <View style={{ width: this.props.circleSize, height: this.props.circleSize, borderRadius: this.props.circleSize, backgroundColor: this.props.circleColor == null ? Color.button : this.props.circleColor }} />
                <View style={Styles.column}>
                    <Text style={[Styles.blackText, { marginLeft: 10, fontSize: 10, fontWeight: 'bold', textAlign: this.props.textAlign, color: this.props.textColor }]}>{this.props.text}</Text>
                    {this.props.detailText != null && (
                        <Text style={[Styles.blackText, {marginLeft: 10, fontSize: 10, fontWeight: 'bold', textAlign: this.props.textAlign, color: this.props.detailColor }]}>{this.props.detailText}</Text>
                    )}
                </View>
                {this.props.status != null && (
                    <Text style={{ paddingHorizontal: 10, paddingVertical: 5, borderRadius: 5, fontSize: 10, textAlign: 'center', color: this.props.statusColor, backgroundColor: this.props.statusBackgroundColor }}>{this.props.status}</Text>
                )}
            </TouchableOpacity>
        )
    }
}
const styles = {
    buttonTransparent: {
        paddingVertical: 15,
        paddingHorizontal: 10,
        // marginHorizontal: 20,
        // borderRadius: 5,
        justifyContent: 'center', alignItems: 'center',
        borderBottomWidth: 1, borderBottomColor: Color.gray
    },
}

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(TouchboxText);