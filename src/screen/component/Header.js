import { dispatch as Redux } from '@rematch/core';
import React, { Component } from 'react';
import { ActivityIndicator, Image, Modal, StatusBar, StyleSheet, Text, TouchableOpacity, View, Dimensions } from 'react-native';
import SearchInput, { createFilter } from 'react-native-search-filter';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import Color from '../utility/Color.js';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Styles from '../utility/Style.js';
import Url from '../utility/Url.js';
const KEYS_TO_FILTERS = ['nama', 'alamat_lengkap', 'username'];
const { width, height } = Dimensions.get('window')

class Header extends Component {
    static navigationOptions = {
        header: null
    };
    constructor(props) {
        super(props)
        this.state = {
            show: false,
            isLoading: false,
            selected: 'Dashboard',
            modalLoading: false,

        }
        this.root = this.props.parent.root;
        this.user = this.props.component.user;
        this.sideMenu = this.props.component.sidemenu;
    }
    modalLoading() {
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.modalLoading}
                onRequestClose={() => null}>
                <TouchableOpacity style={[{ flex: 1, backgroundColor: 'rgba(0,0,0,0.3)' }, Styles.center]}>
                    <Text style={Styles.whiteTextBold}>Sedang mengambil data..</Text>
                    <ActivityIndicator color={Color.white} size={"large"} />
                </TouchableOpacity>
            </Modal>
        )
    }
    componentDidMount() {
        // alert(this.root.state.page)
    }
    onPressRight() {
        this.setState({ show: true });
    }
    searchTopik(input) {
        this.setState({ searchTopik: input })
    }
    cariUjian() {
        let { searchTopik } = this.state
        this.setState({ modalLoading: true });
        Url.getPencarianUjian(this.user.token, searchTopik).then((response) => {
            let data = response.data.data;
            let message = response.data.message;
            if (message != null) {
                this.root.message(message, 'info')
            }
            let hasil_cari = this.props.component.hasil_cari
            hasil_cari = data
            Redux.component.setHasilCari(hasil_cari)

            this.root.navigate('JudulPencarian');
            this.setState({ modalLoading: false });
            // console.log('data hasil cari ujian:', hasil_cari);
        }).catch((error) => {
            // console.log(error, error.response);
            this.setState({ modalLoading: false, });
            alert(error.response.data.message);
            if (error == null) {
                this.root.message("Cek Koneksi internet", 'info');
            }
        });
    }
    render() {

        return (
            <View style={[Styles.HeaderContainer, { height: 56 }]}>
                <StatusBar
                    backgroundColor={Color.primary}
                    barStyle="light-content"
                    style={{ opacity: 0.9 }}
                />
                {this.modalLoading()}
                {this.state.show == true && (this.modalRight())}

                {this.props.title == null && (
                    <View style={{ height: 56, width, flexDirection: 'row', justifyContent: "space-between", backgroundColor: Color.white, elevation: 3 }}>
                        <View style={{ flex: 1, marginVertical: 8, marginHorizontal: 8, justifyContent: 'center' }}>
                            <Image
                                source={require('../../../img/logo_full.png')}
                                style={[{ resizeMode: 'contain', width: 80, height: 40, }]}
                            />
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', marginVertical: 8, marginHorizontal: 8 }}>
                            <Text style={Styles.primaryFontText}>account name</Text>
                            <TouchableOpacity style={{ marginLeft: 4, borderRadius: 20, borderWidth: 1, borderColor: Color.primaryFont }}>
                                <Image
                                    source={require('../../../img/logo.png')}
                                    style={[{ resizeMode: 'contain', width: 36, height: 36, }]}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>

                )}

                {this.props.title != null && (
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingVertical: 16 }}>
                        <TouchableOpacity style={{ flex: 1, flexDirection: 'row', paddingLeft: 20, width: 60, alignItems: 'flex-start' }} onPress={this.props.backPress != null ? this.props.backPress : () => this.root.goBack()}>
                            <Ionicons
                                name={this.props.iconRight != null ? this.props.iconRight : "md-arrow-back"}
                                size={24}
                                style={{ marginRight: 16, }}
                                color={Color.primaryFont}
                            />
                            <Text style={Styles.veryBigBlackTextBold}>{this.props.title}</Text>
                        </TouchableOpacity>
                        <Image
                            source={require('../../../img/logo_full.png')}
                            style={[Styles.center, { resizeMode: 'contain', width: 80, height: 40, marginHorizontal: 8 }]}
                        />
                    </View>
                )}

            </View>
        )
    }
}

const styles = StyleSheet.create({
    atas: {
        flexDirection: 'row', backgroundColor: Color.grayWhite, height: 60,
        alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: 20,
    },
    bawah: {
        flex: 1, flexDirection: 'row', backgroundColor: Color.white, alignSelf: 'flex-end',
        alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: 10
    },
    button: {
        flex: 1, flexDirection: 'column',
        justifyContent: 'center', alignItems: 'center',
        padding: 5, marginHorizontal: 5
    },
    textButton: {
        fontSize: 10,
        color: 'white'
    }
})

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(Header);