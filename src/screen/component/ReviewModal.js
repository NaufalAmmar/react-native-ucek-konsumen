import React, { Component } from 'react';
import { Modal, Text, View } from 'react-native';
import StarRating from 'react-native-star-rating';
import { connect } from 'react-redux';
import Color from '../utility/Color.js';
import Styles from '../utility/Style.js';
import Button from './Button';
import InputWithLabel from './InputWithLabel';

class ReviewModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            starCount: 2.5
        };
    }

    onStarRatingPress(rating) {
        this.setState({
            starCount: rating
        });
    }

    render() {
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.props.visible}
                onRequestClose={this.props.requestClose}
                style={{ flex: 1 }}
            >
                <View style={{ flex: 1, marginVertical: 5, marginHorizontal: 20, padding: 10, backgroundColor: Color.blackOpacity }}>
                    <InputWithLabel
                        styleContainer={{ paddingLeft: 20, backgroundColor: Color.whiteOpacity }}
                        label={"Feedback"}
                        labelColor={Color.black}
                        placeholder="Your feedback"
                        returnKeyType={"next"}
                        // ref={(input) => this.password = input}
                        // onSubmitEditing={() => this.password.focus()}
                        onChangeText={feedback => { this.props.feedbackChange({ feedback }), this.setState({ feedback }) }}
                        clearText={() => { this.setState({ feedback: '' }) }}
                        // style={}
                        defaultValue={this.state.feedback}
                    // underlineColorAndroid={}
                    />
                    <Text style={[Styles.blackText, { marginBottom: 5, color: this.props.labelColor != null ? this.props.labelColor : Color.black }]}>{this.props.label}</Text>
                    <StarRating
                        disabled={this.props.disabled}
                        emptyStar={'ios-star-outline'}
                        fullStar={'ios-star'}
                        halfStar={'ios-star-half'}
                        iconSet={'Ionicons'}
                        maxStars={this.props.maxStars}
                        rating={this.props.rating}
                        selectedStar={(rating) => this.props.onStarRatingPress(rating)}
                        fullStarColor={this.props.fullStarColor}
                        containerStyle={{ width: 200 }}
                    />
                    <Button
                        labelButton="Kirim"
                        labelColor={Color.white}
                        onPress={() => this.props.send()}
                        backgroundColor={Color.primary}
                    // iconLeft={true}
                    // iconLeftName="md-eye"
                    />
                </View>
            </Modal>
        );
    }
}

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(ReviewModal);