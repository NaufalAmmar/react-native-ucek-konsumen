import React, { Component } from 'react';
import { Modal, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux';
import Color from '../utility/Color';

class ModalConfirm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: true,
        };
    }
    action() {
        this.setState({ modalVisible: false })
        this.props.action();
    }
    close() {
        this.setState({ modalVisible: false })
    }
    render() {
        return (
            <Modal
                animationType="slide"
                visible={this.props.visible == null ? this.state.modalVisible : this.props.visible}
                transparent={true}
                onRequestClose={() => { this.setState({ modalVisible: !this.state.modalVisible }) }}
            >
                {/* <TouchableOpacity onPress={() => {  this.props.onPressClose == null ? this.setState({ modalVisible: false }) : this.props.onPressClose() }} style={styles.modalBackground} > */}
                <View style={styles.modalBackground} >
                    
                    <View style={styles.modal}>
                        <Text style={styles.title}>{this.props.title}</Text>

                        <Text style={styles.text}>{this.props.text}</Text>
                        {/* <View style={styles.action}> */}
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', marginTop: 10, marginBottom: 5 }}>

                            <TouchableOpacity style={{ flex: 1, backgroundColor: Color.redFill, padding: 5, borderRadius:3, height:40 }} onPress={() => this.props.onPressClose == null ? this.setState({ modalVisible: false }) : this.props.onPressClose()}>
                                <Text style={styles.textClose}>{this.props.textClose}</Text>
                            </TouchableOpacity>
                            <View style={{width:20}}/>
                            <TouchableOpacity style={{ flex: 1, backgroundColor: Color.greenFill, padding: 5, borderRadius:3, height:40 }} onPress={() => this.action()}>
                                <Text style={[styles.textAction]}>{this.props.textAction}</Text>
                            </TouchableOpacity>
                            {/* </View> */}
                        </View>
                    </View>
                </View>
                {/* </TouchableOpacity> */}
            </Modal >
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    modalBackground: {
        flex: 1,
        backgroundColor: 'rgba(255,255,255,0.8)', paddingVertical: 150,
        // alignItems: 'center', justifyContent: 'center'
    },
    modal: {
        flex: 1,
        paddingHorizontal: 20,
        // backgroundColor: Color.white,
        padding: 5,
    },
    button: {
        // flex: 1,
        backgroundColor: Color.primary,
        padding: 5,
        // justifyContent: 'center', 
        alignItems: 'center'
    },
    title: {
        backgroundColor:Color.secondaryButton,
        borderTopRightRadius:10,
        borderTopLeftRadius:10,
        paddingLeft:10,
        padding:5,
        fontSize: 15, fontWeight: 'bold',
        color: Color.white,
        borderBottomWidth: 1,
        borderColor: Color.grayFill,
        marginBottom: 5
    },
    text: {
        fontSize: 13,
        color: Color.white,
        textAlign: 'left',
        backgroundColor: Color.secondaryButton, 
        padding: 15,
        borderRadius:3,
    },
    action: {
        marginVertical: 5, marginHorizontal: 10,
        flex: 1,
        // flexDirection: 'row',
        // justifyContent: 'space-between',
        position: 'absolute',
        bottom: 15, right: 15
    },
    textAction: {
        padding:5,
        fontSize: 12,
        color: Color.white,
        textAlign: 'center'
    },
    textClose: {
        fontSize: 12,
        padding:5,
        // backgroundColor: Color.redFill,
        color: Color.white,
        textAlign: 'center'
    }
});
function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(ModalConfirm);