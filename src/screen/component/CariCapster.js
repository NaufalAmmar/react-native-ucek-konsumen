import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import Color from '../utility/Color.js';
import Styles from '../utility/Style.js';
import Button from './Button';
import TextButton from './TextButton';

class FloatingButton extends Component {
    constructor(props) {
        super(props)
        this.state = {
            // show: false
        }
        this.root = this.props.component.root;
    }
    render() {
        return (
            <View style={[Styles.column, { backgroundColor: Color.grayWhite, padding: 20, marginHorizontal: 0, marginVertical: 5, borderRadius: 5, }]}>
                <Text style={Styles.blackTextBold}>Lokasi Saya</Text>
                <View style={[Styles.row, { alignItems: 'center', borderRadius: 5, backgroundColor: Color.white, padding: 5, marginVertical: 10 }]}>
                    <MaterialIcons
                        name={"gps-fixed"}
                        size={15}
                        color={Color.primaryFont}
                    />
                    <Text style={[Styles.blackText, { marginLeft: 5 }]}>{this.props.locationLabel != null ? this.props.locationLabel : "Mencari Lokasi .."}</Text>
                </View>
                {this.props.activateNow != null && (
                    <Button
                        labelButton={this.props.labelButton}
                        onPress={this.props.activateNow}
                        backgroundColor={this.props.buttonBackgroundColor}
                    // iconLeft={true}
                    // iconLeftName="md-eye"
                    />
                )}
                <View style={Styles.rowBetween}>
                    {this.props.aturJadwal != null && (
                        <TextButton
                            labelButton="Atur Jadwal"
                            onPress={this.props.aturJadwal}
                            textAlign={"left"}
                            color={Color.black}
                        />
                    )}
                    {this.props.aturLayanan != null && (
                        <TextButton
                            labelButton="Atur Layanan"
                            onPress={this.props.aturLayanan}
                            textAlign={"right"}
                            color={Color.black}
                        />
                    )}
                </View>
            </View>
        )
    }
}
const styles = {
    float: {
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.2)',
        alignItems: 'center',
        justifyContent: 'center',
        width: 60,
        height: 60,
        position: 'absolute',
        bottom: 10,
        right: 15,
        backgroundColor: Color.primary,
        borderRadius: 60,
    }
}

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(FloatingButton);