import React, { Component } from 'react';
import { View, Text, Alert, Image, ActivityIndicator, Modal, TouchableOpacity, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import Color from '../utility/Color.js';
import Styles from '../utility/Style.js';
import Url from '../utility/Url';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { dispatch as Redux } from '@rematch/core';
const { height, width } = Dimensions.get('window');
import Lightbox from 'react-native-lightbox';
import FitImage from 'react-native-fit-image';
import Button from './Button';
import InputWithLabel from './InputWithLabel';

class VoucherContent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            clicked: 0,
            defaultImage: require('../../../img/photo.png'),
            isLoading: false,
            modalRedeem: false,
            modalBerhasil: false,
            kode_outlet: ''
        }
        this.root = this.props.component.root;
        this.data_user = this.props.component.user;
    }
    confirmClick(data) {
        Alert.alert(
            'Peringatan',
            'Konfirmasi Kedatangan PE ?',
            [
                {
                    text: 'Batal', onPress: () => {
                        null
                    }
                }, {
                    text: 'Konfirmasi', onPress: () => {
                        let log = data.pe_log_id;
                        // console.log('pe log', log)
                        this.setState({ isLoading: true })
                        Url.konfirmasiVisit(log).then((response) => {
                            // let dataKonfirmasi = response.data.data;
                            this.setState({ isLoading: false });
                            Alert.alert("Informasi", "Terima Kasih atas konfirmasi anda");
                            this.inbox = this.props.component.inbox
                            this.setState({ isLoading: false })
                            this.inbox.loadInbox()
                            // console.log('data konfirmasi:', dataKonfirmasi);
                        }).catch((error) => {
                            // console.log(error, error.response);
                            if (error.response == null) {
                                alert("Network Error.")
                                this.setState({ isLoading: false });
                            }
                            alert(error.response.data.data);
                            this.setState({ isLoading: false });
                        });
                    }
                }],
            { cancelable: false }
        )

    }
    modalRedeem() {
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.modalRedeem}
                onRequestClose={() => this.setState({ modalRedeem: false })}
                style={{ flex: 1 }}
            >
                <View style={{ flex: 1, marginVertical: 5, marginHorizontal: 20, padding: 10, backgroundColor: Color.blackOpacity }}>
                    <Text style={[Styles.blackText, { marginBottom: 5, color: Color.black }]}>Masukkan Kode Outlet</Text>
                    <InputWithLabel
                        styleContainer={{ paddingLeft: 20, backgroundColor: Color.whiteOpacity }}
                        // label={"Kode Outlet"}
                        labelColor={Color.black}
                        placeholder="kode_outlet"
                        returnKeyType={"next"}
                        // ref={(input) => this.password = input}
                        // onSubmitEditing={() => this.password.focus()}
                        onChangeText={kode_outlet => { this.props.kodeChange({ kode_outlet }), this.setState({ kode_outlet }) }}
                        clearText={() => { this.setState({ kode_outlet: '' }) }}
                        // style={}
                        defaultValue={this.state.kode_outlet}
                    // underlineColorAndroid={}
                    />
                    <Button
                        labelButton="Redeem"
                        labelColor={Color.white}
                        onPress={() => this.props.redeem != null ? this.props.redeem() : this.setState({ modalBerhasil: true })}
                        backgroundColor={Color.primary}
                    // iconLeft={true}
                    // iconLeftName="md-eye"
                    />
                </View>
            </Modal>
        );
    }
    modalBerhasil() {
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.modalBerhasil}
                onRequestClose={() => this.setState({ modalBerhasil: false })}
                style={{ flex: 1 }}
            >
                <View style={{ flex: 1, marginVertical: 5, marginHorizontal: 10, padding: 10, backgroundColor: Color.blackOpacity }}>
                    <Image
                        source={require('../../../img/undraw_discount.png')}
                        style={[Styles.center, { resizeMode: 'contain', alignSelf: 'center', width: width / 1.3, height: 200, }]}
                    />
                    <Text style={[Styles.blackTextBold, { fontSize: 21, textAlign: 'center', marginVertical: 5, color: Color.black }]}>Berhasil!</Text>
                    <Text style={[Styles.blackText, { fontSize: 15, textAlign: 'center', marginVertical: 5, color: Color.black }]}>Kamu sudah berhasil menggunakan vouchermu. Selamat!!</Text>
                    <Image
                        source={require('../../../img/photo.png')}
                        style={[Styles.center, { resizeMode: 'contain', alignSelf: 'center', width: 200, height: 100, }]}
                    />
                    <Text style={[Styles.blackText, { fontSize: 15, textAlign: 'center', marginVertical: 5, color: Color.black }]}>Potongan Rp 20.000 hanya di Coffee Toffee.</Text>
                    <Button
                        labelButton="Kembali"
                        labelColor={Color.white}
                        onPress={() => this.props.kembaliPress != null ? this.props.kembaliPress() : this.setState({ modalBerhasil: false })}
                        backgroundColor={Color.primary}
                    // iconLeft={true}
                    // iconLeftName="md-eye"
                    />
                </View>
            </Modal>
        );
    }
    abortClick(data) {
        Alert.alert(
            'Peringatan',
            'Apakah PE tidak mengunjungi anda ?',
            [
                {
                    text: 'Batal', onPress: () => {
                        null
                    }
                }, {
                    text: 'Iya', onPress: () => {
                        let log = data.pe_log_id;
                        // console.log('pe log', log)
                        this.setState({ isLoading: true })
                        Url.rejectVisit(log).then((response) => {
                            // let dataKonfirmasi = response.data.data;
                            this.setState({ isLoading: false });
                            Alert.alert("Informasi", "Terima Kasih atas konfirmasi anda");
                            this.inbox = this.props.component.inbox
                            this.setState({ isLoading: false })
                            this.inbox.loadInbox()
                            // console.log('data konfirmasi:', dataKonfirmasi);
                        }).catch((error) => {
                            // console.log(error, error.response);
                            if (error.response == null) {
                                alert("Network Error.")
                                this.setState({ isLoading: false });
                            }
                            alert(error.response.data.data);
                            this.setState({ isLoading: false });
                        });
                    }
                }],
            { cancelable: false }
        )
    }
    gunakanVoucher(item) {
        this.setState({ modalRedeem: true })
    }
    render() {
        if (this.props.data.length != 0 && this.props.data != null) {
            return (
                <View style={styles.menu} >
                    {this.props.data.map((item, index) => {
                        let page = null
                        if (item.type == 'INBOX' && item.type == 'CONFIRM') {
                            page = null
                        } else if (item.type == 'NEWS') {
                            page = 'Dashboard'
                        } else if (item.type == 'HADIAH') {
                            page = 'LihatHadiah'
                        } else {
                            page = null
                        }
                        // console.log(item, page)
                        return (
                            <TouchableOpacity key={index} style={styles.box} onPress={() => page != null ? this.root.changePage(page) : null}>
                                {item.imageUrl == null && item != null && (
                                    <View>
                                        <View style={Styles.row}>
                                            <View style={{ flex: 1 }}>
                                                <View style={Styles.rowBetween}>
                                                    <Text style={[Styles.blackTextBold, { fontSize: 12, marginBottom: 5, flexWrap: 'wrap' }]}>{item.label.length > 34 ? item.label.substr(0, 34) + ".." : item.label}</Text>
                                                </View>
                                                <Text style={[Styles.blackText, { fontSize: 12, marginBottom: 5 }]}>{item.value}</Text>
                                                <Text style={[Styles.blackTextBold, { alignSelf: 'flex-end', fontSize: 10 }]}>{item.time}</Text>
                                            </View>
                                        </View>
                                    </View>
                                )}
                                {item.imageUrl != null && item != null && (
                                    <TouchableOpacity style={{ flex: 1, borderRadius: 20 }}>
                                        {item.imageUrl != null && (
                                            <Lightbox
                                                underlayColor="white"
                                                backgroundColor={Color.white}
                                                style={{ backgroundColor: Color.white }}
                                                springConfig={{ tension: 15, friction: 7 }}
                                                renderHeader={() =>
                                                    // <Text style={[Styles.blackText, { fontSize: 15, flexWrap: 'wrap', position:'absolute', right:10, top:10 }]}>Tutup</Text>
                                                    null
                                                }
                                                renderContent={() =>
                                                    <View style={{ flex: 1, paddingHorizontal: 10, paddingTop: 20 }}>
                                                        <Image
                                                            // borderRadius={20}
                                                            resizeMode="contain"
                                                            source={{ uri: item.imageUrl }}
                                                            style={styles.image}
                                                        />
                                                        <View style={{ flex: 1, paddingHorizontal: 10, backgroundColor: Color.grayWhite }}>
                                                            <Text style={[Styles.blackText, { fontSize: 15, flexWrap: 'wrap' }]}>{item.label.length > 34 ? item.label.substr(0, 34) + ".." : item.label}</Text>
                                                            <Text style={[Styles.blackText, { fontSize: 12, marginBottom: 5 }]}>{item.value}</Text>
                                                            <Text style={[Styles.primaryFontText, { alignSelf: 'flex-start', fontSize: 10 }]}>{item.time}</Text>
                                                            <Button
                                                                labelButton="Gunakan"
                                                                onPress={() => this.gunakanVoucher(item)}
                                                                backgroundColor={Color.secondaryButton}
                                                            // iconLeft={true}
                                                            // iconLeftName="md-eye"
                                                            />
                                                        </View>
                                                    </View>
                                                }
                                            >
                                                <FitImage
                                                    borderRadius={20}
                                                    resizeMode="contain"
                                                    source={{ uri: item.imageUrl }}
                                                    style={styles.inboxImage}
                                                />

                                            </Lightbox>
                                        )
                                        }
                                        <View style={[Styles.rowBetween, { alignItems: 'center', backgroundColor: Color.white, paddingHorizontal: 10, paddingVertical: 10, position: 'absolute', bottom: 0, borderBottomStartRadius: 20, borderBottomEndRadius: 20 }]}>
                                            <View style={{ flex: 1 }}>
                                                <Text style={[Styles.blackText, { fontSize: 15, flexWrap: 'wrap' }]}>{item.label.length > 34 ? item.label.substr(0, 34) + ".." : item.label}</Text>
                                                <Text style={[Styles.primaryFontText, { alignSelf: 'flex-start', fontSize: 10 }]}>{item.time}</Text>
                                            </View>
                                            <Button
                                                height={35}
                                                labelButton="Gunakan"
                                                onPress={() => this.gunakanVoucher(item)}
                                                backgroundColor={Color.secondaryButton}
                                            // iconLeft={true}
                                            // iconLeftName="md-eye"
                                            />
                                        </View>
                                    </TouchableOpacity>
                                )
                                }
                            </TouchableOpacity >
                        )
                    })
                    }
                    {this.modalBerhasil()}
                    {this.modalRedeem()}
                </View>
            )
        }
    }
}
const styles = {
    menu: {
        backgroundColor: Color.white,
        flex: 1,
        // paddingHorizontal: 2
    },
    icon: {
        width: 40, height: 40,
        padding: 10,
        backgroundColor: Color.primary,
        borderRadius: 10
    },
    redButton: {
        padding: 5, borderRadius: 5,
        borderColor: Color.redFill, borderWidth: 1,
        marginRight: 5,
    },
    greenButton: {
        padding: 5, borderRadius: 5,
        borderColor: Color.greenFill, borderWidth: 1
    },
    inboxImage: {
        // height:400,
        // resizeMode:'contain',
        // borderRadius: 20,
        // borderWidth: 1, borderColor: 'rgba(0,0,0,0.2)'
    },
    image: {
        // flex:1,
        // width,
        height: width / 2,
        marginVertical: 10
        // backgroundColor: Color.primaryFont,
    },
    box: {
        flex: 1,
        // backgroundColor: Color.white,
        // paddingVertical: 5,
        // paddingHorizontal: 10,
        marginVertical: 5,
        borderRadius: 20,
        elevation: 3,
        // borderBottomWidth: 1, borderColor: Color.gray,

    },
}

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(VoucherContent);