import React, { Component } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux';
import Color from '../utility/Color.js';
import Styles from '../utility/Style.js';

class ButtonContent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            show: false
        }
        this.root = this.props.component.root;
    }
    render() {
        return (
            <TouchableOpacity activeOpacity={0.8} style={[styles.box, { backgroundColor: this.props.backgroundColor != null ? this.props.backgroundColor : Color.primary }]} onPress={this.props.onPress}>
                {/* {this.props.sourceImage != null && (
                    <Image
                        // source={require('../../../img/background.jpg')}
                        source={this.props.sourceImage}
                        style={[styles.circlePhoto, { width: this.props.imageSize, height: this.props.imageSize, borderRadius: this.props.imageSize }]}
                    />
                )} */}
                <View style={[Styles.rowBetween, { alignItems: 'center' }]}>
                    <View>
                        <Text style={[Styles.whiteTextBold, { fontSize: 15, textAlign: this.props.textAlign, color: this.props.nameTextColor != null ? this.props.nameTextColor : Color.white }]}>{this.props.name}</Text>
                        {this.props.otherText != null && (
                            <Text style={[Styles.whiteText, { fontWeight: this.props.weightOtherText != null ? this.props.weightOtherText : 'normal', fontSize: 12, textAlign: this.props.textAlign, color: this.props.otherTextColor != null ? this.props.otherTextColor : Color.white }]}>{this.props.otherText}</Text>
                        )}
                    </View>
                    <Text style={{fontWeight: this.props.weightButtonText != null ? this.props.weightButtonText : 'normal', paddingHorizontal: 10, paddingVertical: 5, borderRadius: 5, fontSize: this.props.fontButtonSize != null ? this.props.fontButtonSize : 10, textAlign: 'center', color: this.props.buttonTextColor, backgroundColor: this.props.buttonBackgroundColor }}>{this.props.buttonText}</Text>
                </View>
            </TouchableOpacity>
        )
    }
}
const styles = {
    box: {
        flex: 1, flexDirection: 'row',
        backgroundColor: Color.white,
        paddingVertical: 10,
        paddingHorizontal: 10,
        // marginHorizontal: 20,
        marginVertical: 3,
        borderRadius: 5,
        borderColor: Color.whiteOpacity, borderBottomWidth: 1, borderTopWidth: 1,
        justifyContent: 'center', alignItems: 'center'
    },
    contact: {
        width: 70,
        flexDirection: 'row', borderRadius: 5,
        backgroundColor: Color.secondaryButton, padding: 2,
        marginVertical: 2,
        justifyContent: 'center', alignItems: 'center'
    },
    circlePhoto: {
        width: 55, height: 55,
        borderRadius: 55, borderWidth: 1, borderColor: Color.gray,
        marginRight: 15,
        justifyContent: 'center', alignItems: 'center'
    },
    icon: {
        width: 30, height: 30, marginHorizontal: 5,
        justifyContent: 'center', alignItems: 'center', alignSelf: 'center'
    },
    textRight: {
        flex: 1,
        justifyContent: 'center', alignItems: 'flex-end', alignSelf: 'center'
    }
}

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(ButtonContent);