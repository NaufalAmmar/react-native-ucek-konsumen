import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import Color from '../utility/Color.js';
import Styles from '../utility/Style.js';

class WarningText extends Component {
    constructor(props) {
        super(props)
        this.state = {
            // show: false
        }
        this.root = this.props.component.root;
    }
    render() {
        if (this.props.visible == true) {
            return (
                <TouchableOpacity onPress={this.props.onPress} style={[Styles.row, { paddingHorizontal: 24, paddingVertical: 12, borderWidth: 1, borderColor: Color.warning, alignItems: 'center', marginVertical: 8 }]} >
                    <MaterialIcons
                        name={"warning"}
                        color={Color.warning}
                        size={24}
                    />
                    <View>
                        <Text style={[Styles.blackTextBold, { color: Color.warning, marginHorizontal: 20 }]}>{this.props.text}</Text>
                        <Text style={[Styles.blackText, { fontSize: 10, color: Color.warning, marginHorizontal: 20 }]}>{'Wajib! agar dapat dipesan oleh customer.'}</Text>
                    </View>
                </TouchableOpacity>
            )
        } else {
            return (
                <View />
            )
        }
    }
}
const styles = {
    container: {
        flexDirection: 'row',
        height: 30,
        backgroundColor: 'transparent',
        justifyContent: 'center', alignItems: 'center', marginVertical: 4,
        // elevation: 3
    },
    icon: {
        width: 30, height: 30, marginRight: 10,
        justifyContent: 'center', alignItems: 'center'
    }
}

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(WarningText);