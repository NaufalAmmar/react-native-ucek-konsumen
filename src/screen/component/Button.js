import React, { Component } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import HTML from 'react-native-render-html';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import Color from '../utility/Color.js';
import Styles from '../utility/Style.js';

class Button extends Component {
    constructor(props) {
        super(props)
        this.state = {
            // show: false
        }
        this.root = this.props.component.root;
    }
    render() {
        return (
            <TouchableOpacity disabled={this.props.disabled} activeOpacity={0.8} style={[styles.buttonPrimary, this.props.containerStyle, { height: this.props.height != null ? this.props.height : 50, backgroundColor: this.props.backgroundColor == null ? Color.primary : this.props.backgroundColor, justifyContent: this.props.justifyContent != null ? this.props.justifyContent : 'center', paddingHorizontal: this.props.paddingHorizontal != null ? this.props.paddingHorizontal : 15, borderRadius: this.props.radiusContainer != null ? this.props.radiusContainer : 5, borderColor: this.props.borderColor == null ? 'transparent' : this.props.borderColor, }]} onPress={this.props.onPress}>
                {this.props.iconLeft == true && (
                    <View style={[styles.icon, this.props.styleIconLeft]}>
                        {this.props.iconLeftName != null && (
                            <Ionicons name={this.props.iconLeftName}
                                color={this.props.labelColor != null ? this.props.labelColor : Color.white}
                                size={this.props.iconSize != null ? this.props.iconSize : 27}
                            />
                        )}
                        {this.props.materialCommunityIconLeftName != null && (
                            <MaterialCommunityIcons name={this.props.materialCommunityIconLeftName}
                                color={this.props.labelColor != null ? this.props.labelColor : Color.white}
                                size={this.props.iconSize != null ? this.props.iconSize : 27}
                            />
                        )}
                        {this.props.materialIconLeftName != null && (
                            <MaterialIcons name={this.props.materialIconLeftName}
                                color={this.props.labelColor != null ? this.props.labelColor : Color.white}
                                size={this.props.iconSize != null ? this.props.iconSize : 27}
                            />
                        )}
                    </View>

                )}
                {this.props.labelHTML != null && (
                    <HTML ignoredStyles={['display']} tagsStyles={{ p: { fontSize: 13, textAlign: this.props.labelAlign, color: this.props.labelColor } }} style={{ fontSize: 10, fontWeight: 'bold', color: Color.black }} html={this.props.labelHTML} />
                )}
                {this.props.labelButton != null && (
                    <Text style={[Styles.whiteTextBold, { fontSize: 14, fontWeight: this.props.fontWeight != null ? this.props.fontWeight : 'bold', textAlign: this.props.labelAlign, color: this.props.labelColor != null ? this.props.labelColor : Color.white }]}>{this.props.labelButton}</Text>
                )}
                {this.props.iconRight == true && (
                    <View style={styles.icon}>
                        {this.props.iconRightName != null && (
                            <Ionicons name={this.props.iconRightName}
                                color={Color.white}
                                size={this.props.iconSize != null ? this.props.iconSize : 27}
                            />
                        )}
                        {this.props.materialCommunityIconRightName != null && (
                            <MaterialCommunityIcons name={this.props.materialCommunityIconRightName}
                                color={this.props.labelColor != null ? this.props.labelColor : Color.white}
                                size={this.props.iconSize != null ? this.props.iconSize : 27}
                            />
                        )}
                        {this.props.materialIconRightName != null && (
                            <MaterialIcons name={this.props.materialIconRightName}
                                color={Color.white}
                                size={this.props.iconSize != null ? this.props.iconSize : 27}
                            />
                        )}
                    </View>
                )}
            </TouchableOpacity>
        )
    }
}
const styles = {
    buttonPrimary: {
        flexDirection: 'row',
        height: 40, borderRadius: 5,
        backgroundColor: Color.button,
        // paddingHorizontal: 15, 
        padding: 10,
        justifyContent: 'center', alignItems: 'center', marginVertical: 7,
        elevation: 3, borderWidth: 1
    },
    icon: {
        width: 30, height: 30,
        // marginLeft: 10,
        justifyContent: 'center',
        alignItems: 'center',
        // alignItems: 'center'
    }
}

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(Button);