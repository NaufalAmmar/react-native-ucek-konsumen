import React, { Component } from 'react';
import { Text, View, Image } from 'react-native';
import { createAppContainer } from 'react-navigation';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Color from '../utility/Color.js';
import DashboardOwner from '../owner/DashboardOwner';
import Notifikasi from '../user/Notifikasi';
import Aktivitas from '../user/Aktivitas';
import RiwayatTransaksi from '../user/RiwayatTransaksi';
import Profile from '../user/Profile';
import Styles from '../utility/Style.js';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';

class DashboardScreen extends Component {
    render() {
        return (
            <View style={{ flex: 1 }}>
                <DashboardOwner gotoProfile={() => this.props.navigation.navigate('EditProfile')} />
            </View>
        );
    }
}

class NotifikasiScreen extends Component {
    render() {
        return (
            <View style={{ flex: 1 }}>
                <Notifikasi />
            </View>
        );
    }
}
class ProfileScreen extends Component {
    render() {
        return (
            <View style={{ flex: 1 }}>
                <Profile />
            </View>
        );
    }
}
class AktivitasScreen extends Component {
    render() {
        return (
            <View style={{ flex: 1 }}>
                <Aktivitas />
            </View>
        );
    }
}
class RiwayatTransaksiScreen extends Component {
    render() {
        return (
            <View style={{ flex: 1 }}>
                <RiwayatTransaksi />
            </View>
        );
    }
}


export default createAppContainer(createMaterialTopTabNavigator({
    Beranda: { screen: DashboardScreen, },
    Riwayat: { screen: RiwayatTransaksiScreen, },
    Notifikasi: { screen: NotifikasiScreen, },
    Profil: { screen: ProfileScreen, },
},

    // { initialRouteName: 'Aktivitas' },
    {
        defaultNavigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused, tintColor }) => {
                const { routeName } = navigation.state;
                let iconName;
                // console.log(routeName, focused)
                if (routeName === 'Beranda') {
                    iconName = 'cupcake';
                } else if (routeName === 'Notifikasi') {
                    iconName = 'bell'
                } else if (routeName === 'Profil') {
                    iconName = 'account-box'
                } else if (routeName === 'Riwayat') {
                    iconName = 'coffee'
                }
                // You can return any component that you like here! We usually use an
                // icon component from react-native-vector-icons
                return (
                    <View style={Styles.center}>
                        <MaterialCommunityIcons name={iconName} size={22} color={tintColor} />
                    </View>
                );
            },
            tabBarPosition: 'bottom'
        }),
        tabBarOptions: {
            showIcon: true,
            // showLabel: true,
            keyboardHidesTabBar: true,
            activeTintColor: Color.primary,
            inactiveTintColor: Color.blackOpacity,
            upperCaseLabel: false,
            // labelPosition: 'below-icon',
            indicatorStyle: {
                backgroundColor: Color.primary
            },
            labelStyle: {
                fontSize: 10,
            },
            tabStyle: {
                flex: 1, height: 60,
            },
            style: {
                backgroundColor: 'white',
            },
        },

    }
));