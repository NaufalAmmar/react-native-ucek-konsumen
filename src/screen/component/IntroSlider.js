import React, { Component } from 'react';
import { ActivityIndicator, AsyncStorage, Image, StyleSheet, View } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import { Ionicons } from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import Color from '../utility/Color';

const slides = [
  {
    key: 'somethun',
    title: 'Title 1',
    text: 'Description.\nSay something cool',
    image: require('../../../img/1.png'),
    backgroundColor: Color.primary,
  },
  {
    key: 'somethun-dos',
    title: 'Title 2',
    text: 'Other cool stuff',
    image: require('../../../img/2.png'),
    backgroundColor: Color.primary,
  },
  {
    key: 'somethun1',
    title: 'Rocket guy',
    text: 'I\'m already out of descriptions\n\nLorem ipsum bla bla bla',
    image: require('../../../img/3.png'),
    backgroundColor: Color.primary,
  },
  {
    key: 'somethun1',
    title: 'Rocket guy',
    text: 'I\'m already out of descriptions\n\nLorem ipsum bla bla bla',
    image: require('../../../img/4.png'),
    backgroundColor: Color.primary,
  }
];

class IntroSlider extends Component {
  constructor(props) {
    super(props)
    this.root = this.props.component.root;
    this.state = {
      showRealApp: false
    }
  }
  _renderItem = (item) => {
    return (
      <View style={styles.slide}>
        <Image style={styles.image} source={item.image} />
        {/* <Text style={styles.title}>{item.title}</Text>
        <Text style={styles.text}>{item.text}</Text> */}
      </View>
    );
  }
  _onDone = () => {
    // User finished the introduction. Show real app through
    // navigation or simply by controlling state
    AsyncStorage.setItem('intro', 'Sudah Intro');
    this.root.cekStorageUser();
    this.setState({ showRealApp: true });
  }
  _renderNextButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Ionicons
          name="md-arrow-round-forward"
          color="rgba(255, 255, 255, .9)"
          size={24}
          style={{ backgroundColor: 'transparent' }}
        />
      </View>
    );
  }
  _renderDoneButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Ionicons
          name="md-checkmark"
          color="rgba(255, 255, 255, .9)"
          size={24}
          style={{ backgroundColor: 'transparent' }}
        />
      </View>
    );
  }
  render() {
    if (this.state.showRealApp) {
      return <ActivityIndicator />
    } else {
      return (
        <AppIntroSlider
          // renderDoneButton={this._renderDoneButton}
          // renderNextButton={this._renderNextButton}
          buttonTextStyle={styles.buttonCircle}
          renderItem={this._renderItem}
          slides={slides}
          onDone={this._onDone}
        />
      )
    }
  }
}
const styles = StyleSheet.create({
  buttonCircle: {
    padding:10, borderRadius:25,
    backgroundColor: Color.primary,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: '100%',
    height: '100%',position: 'absolute',
    resizeMode: 'contain'
  },
  slide: {
    flex: 1, backgroundColor: Color.primary, 
    // paddingHorizontal:'10%', 
    paddingVertical:50,
    alignItems:'center', justifyContent:'center'
  },
  title: {
    fontSize: 25, color: Color.white, padding: 5,
    textAlign:'center', position:'absolute', top:100, left:50,right:50,
    
  },
  text: {
    fontSize: 25, color: Color.white, padding: 5,
    textAlign:'center', position:'absolute', bottom:150, left:50,right:50,
  }
});

function mapStateToProps(state) {
  return {
    component: state.component,
  }
}
export default connect(
  mapStateToProps
)(IntroSlider);