import React, { Component } from 'react';
import { Modal, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import Color from '../utility/Color';

class ModalManual extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: true,
        };
    }
    action() {
        this.setState({ modalVisible: false })
        this.props.action();
    }
    close() {
        this.setState({ modalVisible: false })
    }
    render() {
        return (
            <Modal
                animationType="slide"
                visible={this.props.modalVisible}
                transparent={true}
                onRequestClose={() => { !this.props.modalVisible }}
            >
                <TouchableOpacity onPress={() => this.props.close()} style={styles.modalBackground} />
                <View style={styles.modal}>
                    <Text style={styles.title}>{this.props.title}</Text>
                    <MaterialCommunityIcons
                        name="qrcode" color={Color.blackOpacity}
                        size={85} style={{ alignSelf: 'center', marginVertical: 10 }}
                    />
                    <View style={styles.textInput}>
                        <TextInput
                            style={{ flex: 1, height: 300 }}
                            editable={this.props.editable}
                            ref={(input) => this.props.ref = input}
                            returnKeyType={this.props.returnKeyType}
                            onSubmitEditing={this.props.onSubmitEditing}
                            placeholderTextColor="#ddd"
                            placeholder={"Kode"}
                            onChangeText={text => { this.props.onChangeText(text) }}
                            defaultValue={this.props.defaultValue}
                            underlineColorAndroid={this.props.underlineColorAndroid}
                            secureTextEntry={this.props.secureTextEntry}
                            multiline={true}
                            keyboardType={this.props.keyboardType}
                        />
                    </View>
                    <View style={{ position: 'absolute', bottom: 10, right: 10, justifyContent: 'center', marginVertical: 10, marginHorizontal: 10 }}>
                        <TouchableOpacity style={{ backgroundColor: Color.primary }} onPress={() => this.action()}>
                            <Text style={[styles.textAction, styles.button]}>{this.props.textAction}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ position: 'absolute', bottom: 10, left: 10, justifyContent: 'center', marginVertical: 10, marginHorizontal: 10 }}>
                        <TouchableOpacity onPress={() => this.props.close()}>
                            <Text style={styles.textClose}>Cancel</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal >
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center', justifyContent: 'center'
    },
    modalBackground: {
        flex: 1,
        backgroundColor: '#000', opacity: 0.9,
        position: 'absolute'
    },
    modal: {
        width: '70%', height: 250,
        marginVertical: '40%',
        // marginHorizontal: 30,
        // paddingHorizontal: 5, paddingVertical: 5,
        backgroundColor: Color.white,
        padding: 5,
        alignSelf: 'center'
    },
    button: {
        // flex: 1,
        backgroundColor: Color.primary,
        padding: 5,
        // justifyContent: 'center', 
        alignItems: 'center'
    },
    title: {
        fontSize: 17, fontWeight: 'bold',
        color: '#000',
        borderBottomWidth: 1,
        borderColor: Color.grayFill,
        marginTop: 10
    },
    textInput: {
        width: '90%',
        fontSize: 15,
        color: Color.black,
        textAlign: 'left',
        borderRadius: 10, borderColor: Color.gray, borderWidth: 1,
        padding: 10, alignSelf: 'center',
    },
    action: {
        marginVertical: 5, marginHorizontal: 10,
        flex: 1,
        // flexDirection: 'row',
        // justifyContent: 'space-between',
        position: 'absolute',
        bottom: 15, right: 15
    },
    textAction: {
        fontSize: 15,
        color: Color.white,
    },
    textClose: {
        fontSize: 15,
        // backgroundColor: Color.redFill,
        color: Color.gray,
        padding: 5,
        textAlign: 'center'
    }
});
function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(ModalManual);