import React, { Component } from 'react';
import { ActivityIndicator, Dimensions, StyleSheet, TouchableOpacity, View } from 'react-native';
import FitImage from 'react-native-fit-image';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
const { height, width } = Dimensions.get('window');

class PhotoFrame extends Component {
    constructor(props) {
        super(props)

        this.state = {
            icon: this.props.icon ? this.props.icon : 'save',
            title: this.props.title ? this.props.title : 'Save'
        };
    }

    render() {
        if (this.props.loadingStatus) {
            return (
                <View style={styles.foto}>
                    <ActivityIndicator />
                </View>
            );
        }
        return (
            <TouchableOpacity style={styles.foto} onPress={this.props.onPress}>
                {this.props.imageUrl == "" && (
                    <MaterialIcons
                        name="camera-alt"
                        color="#000"
                        size={25}
                    />
                )}
                {this.props.imageUrl != "" && (
                    <FitImage
                        resizeMode="contain"
                        source={this.props.imageUrl}
                        style={{ width: '100%', height: '100%', borderRadius: 10, resizeMode: 'contain' }}
                    />
                )}
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    foto: {
        width: '100%', height: height / 3,
        alignSelf: 'center',
        alignItems: 'center', justifyContent: 'center',
        borderRadius: 10,
        padding: 3
        //borderWidth: 1, borderColor: '#ccc'
    },
});

export default PhotoFrame;