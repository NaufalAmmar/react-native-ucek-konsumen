import React, { Component } from 'react';
import { Text, Modal, TouchableOpacity, View, Picker, Dimensions } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import Color from '../utility/Color.js';
import Button from './Button';
import Styles from '../utility/Style.js';
import CalendarPicker from 'react-native-calendar-picker';
const { height, width } = Dimensions.get('window');

class DateTimePicker extends Component {
    constructor(props) {
        super(props)
        this.state = {
            show: false,
            modalVisible: true,
            selectedStartDate: new Date(),
            hours: '01',
            minutes: '10',
            morn: 'AM',
            dataJam: [
                { label: '01', value: '01' },
                { label: '02', value: '02' },
                { label: '03', value: '03' },
                { label: '04', value: '04' },
                { label: '05', value: '05' },
                { label: '06', value: '06' },
                { label: '07', value: '07' },
                { label: '08', value: '08' },
                { label: '09', value: '09' },
                { label: '10', value: '10' },
                { label: '11', value: '11' },
                { label: '12', value: '12' },
            ],
            dataMenit: [
                { label: '10', value: '10' },
                { label: '15', value: '15' },
                { label: '25', value: '25' },
                { label: '30', value: '30' },
                { label: '40', value: '40' },
                { label: '45', value: '45' },
                { label: '50', value: '50' },
                { label: '55', value: '55' },
            ],
            dataMorn: [
                { label: 'AM', value: 'AM' },
                { label: 'PM', value: 'PM' },
            ]
        }
        this.onDateChange = this.onDateChange.bind(this);
        this.root = this.props.component.root;
    }
    onDateChange(date) {
        this.setState({
            selectedStartDate: date,
        });
    }
    selesai(){
        null
    }
    render() {
        let { dataJam, dataMenit, dataMorn,selectedStartDate } = this.state
        let startDate = selectedStartDate ? selectedStartDate.toString() : '';
        return (
            // <Modal
            //     animationType="slide"
            //     visible={this.props.visible == null ? this.state.modalVisible : this.props.visible}
            //     transparent={true}
            //     onRequestClose={() => { this.setState({ modalVisible: !this.state.modalVisible }) }}
            // >
            //     <View style={[this.props.style, styles.modalBackground, { paddingHorizontal: 20 }]}>
                    <View style={styles.modal}>
                        
                        <Button
                            labelButton={startDate}
                            onPress={() => this.selesai()}
                            backgroundColor={Color.secondaryButton}
                        />
                    </View>
            //     </View>
            // </Modal>
        )
    }
}
const styles = {
    modalBackground: {
        flex: 1,
        backgroundColor: 'rgba(255,255,255,0.2)',
        paddingVertical: 150,
        // alignItems: 'center', justifyContent: 'center'
    },
    modal: {
        flex: 1,
        marginHorizontal: 20,
        paddingHorizontal: 20,
        backgroundColor: Color.white,
        padding: 5,
        borderRadius: 20,
        borderColor: Color.gray, borderWidth: 1,
        elevation: 3
    },
}

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(DateTimePicker);