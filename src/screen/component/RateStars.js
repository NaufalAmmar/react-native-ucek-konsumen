import React, { Component } from 'react';
import { Text, View } from 'react-native';
import StarRating from 'react-native-star-rating';
import { connect } from 'react-redux';
import Color from '../utility/Color.js';
import Styles from '../utility/Style.js';

class RateStars extends Component {

    constructor(props) {
        super(props);
        this.state = {
            starCount: 2.5
        };
    }

    onStarRatingPress(rating) {
        this.setState({
            starCount: rating
        });
    }

    render() {
        return (
            <View style={{ flex: 1, marginVertical: 5 }}>
                <Text style={[Styles.blackText, { marginBottom: 5, color: this.props.labelColor != null ? this.props.labelColor : Color.black }]}>{this.props.label}</Text>
                <StarRating
                    disabled={this.props.disabled}
                    emptyStar={'ios-star-outline'}
                    fullStar={'ios-star'}
                    halfStar={'ios-star-half'}
                    iconSet={'Ionicons'}
                    maxStars={this.props.maxStars}
                    rating={this.props.rating}
                    selectedStar={(rating) => this.props.onStarRatingPress(rating)}
                    fullStarColor={this.props.fullStarColor}
                    containerStyle={{width:200}}
                />
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(RateStars);