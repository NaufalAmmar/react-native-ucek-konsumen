import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import Color from '../utility/Color.js';
import Styles from '../utility/Style.js';

class FloatingButton extends Component {
    constructor(props) {
        super(props)
        this.state = {
            // show: false
        }
        this.root = this.props.component.root;
    }
    render() {
        return (
            <TouchableOpacity
                onPress={this.props.onPress}
                style={styles.float}
            >
                <MaterialIcons name={this.props.icon} size={30} color={Color.white} />
            </TouchableOpacity>
        )
    }
}
const styles = {
    float: {
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.2)',
        alignItems: 'center',
        justifyContent: 'center',
        width: 60,
        height: 60,
        position: 'absolute',
        bottom: 10,
        right: 15,
        backgroundColor: Color.primary,
        borderRadius: 60,
    }
}

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(FloatingButton);