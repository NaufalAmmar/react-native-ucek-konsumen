import React, { Component } from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import Color from '../utility/Color.js';
import Styles from '../utility/Style.js';

class ImageContentDown extends Component {
    constructor(props) {
        super(props)
        this.state = {
            detail: false,
        }
        this.root = this.props.component.root;
    }
    render() {
        return (
            <View style={[styles.box, { backgroundColor: this.props.backgroundColor != null ? this.props.backgroundColor : Color.white }]}>
                <TouchableOpacity style={{ flex: 1 }} activeOpacity={0.8} onPress={this.props.onPress}>
                    {this.props.sourceImage != null && (
                        <Image
                            // source={require('../../../img/background.jpg')}
                            source={this.props.sourceImage}
                            style={[styles.circlePhoto, { width: this.props.imageSize, height: this.props.imageSize, borderRadius: this.props.imageSize }]}
                        />
                    )}
                    <View style={Styles.rowBetween}>
                        <View>
                            <Text style={[Styles.blackText, { fontSize: 15, textAlign: this.props.textAlign, color: this.props.nameTextColor != null ? this.props.nameTextColor : Color.black }]}>{this.props.name}</Text>
                            {this.props.otherText != null && (
                                <Text style={[Styles.blackText, { fontSize: 12, textAlign: this.props.textAlign, color: this.props.otherTextColor != null ? this.props.otherTextColor : Color.black }]}>{this.props.otherText}</Text>
                            )}
                            {this.props.childText != null && (
                                <Text style={[Styles.blackText, { fontSize: 12, textAlign: this.props.textAlign, color: this.props.childTextColor != null ? this.props.childTextColor : Color.black }]}>{this.props.childText}</Text>
                            )}
                            {this.props.subChildText != null && (
                                <Text style={[Styles.blackText, { flexWrap: "wrap", fontSize: 12, textAlign: this.props.textAlign, color: this.props.subChildTextColor != null ? this.props.subChildTextColor : Color.black }]}>{this.props.subChildText}</Text>
                            )}
                            {this.props.contact != null && (
                                <View style={styles.contact}>
                                    <Ionicons name="md-person"
                                        color={Color.white}
                                        size={12}
                                        style={{ alignSelf: 'center', marginRight: 3 }}
                                    />
                                    <Text style={[Styles.whiteText, { fontSize: 12 }]}>{this.props.contact.substr(0, 7)}..</Text>
                                </View>
                            )}
                        </View>
                        {this.props.iconRight == true && (
                            <View style={styles.icon}>
                                {this.props.iconRightName != null && (
                                    <Ionicons name={this.props.iconRightName}
                                        color={this.props.iconColor != null ? this.props.iconColor : Color.black}
                                        size={23}
                                        style={{ alignSelf: 'center' }}
                                    />
                                )}
                                {this.props.materialIconRightName != null && (
                                    <View style={[styles.circlePhoto, { backgroundColor: this.props.iconBackgroundColor != null ? this.props.iconBackgroundColor : null }]}>
                                        <MaterialIcons name={this.props.materialIconRightName}
                                            color={this.props.iconColor != null ? this.props.iconColor : Color.black}
                                            size={25}
                                            style={{ alignSelf: 'center' }}
                                        />
                                    </View>
                                )}
                            </View>
                        )}
                        {this.props.textRight == true && (
                            <View style={styles.textRight}>
                                {this.props.textRightValue != null && (
                                    <Text style={[Styles.whiteText, { color: this.props.textRightColor != null ? this.props.iconColor : Color.black, fontSize: 15 }]}>{this.props.textRightValue}</Text>
                                )}
                            </View>
                        )}
                    </View>
                </TouchableOpacity>
                {this.state.detail == true && (
                    <View>
                        <Text>{this.props.detailA}</Text>
                        <Text>{this.props.detailB}</Text>
                        <Text>{this.props.detailC}</Text>
                        <Text>{this.props.detailD}</Text>
                        <Text>{this.props.detailE}</Text>
                    </View>
                )}
                {this.props.detail == true && (
                    <TouchableOpacity style={{ flex: 1 }} activeOpacity={0.8} onPress={() => this.setState({ detail: !this.state.detail })}>
                        <View style={{ height: 20, borderTopWidth: 1, borderTopColor: Color.gray }}>
                            <Text style={Styles.center}>{this.state.detail != true ? "Show Detail" : "Hide"}</Text>
                        </View>
                    </TouchableOpacity>
                )}
            </View>
        )
    }
}
const styles = {
    box: {
        flex: 1, flexDirection: 'row',
        backgroundColor: Color.white,
        paddingVertical: 15,
        paddingLeft: 20, paddingRight: 10,
        marginBottom: 2,
        justifyContent: 'center', alignItems: 'center'
    },
    contact: {
        width: 70,
        flexDirection: 'row', borderRadius: 5,
        backgroundColor: Color.secondaryButton, padding: 2,
        marginVertical: 2,
        justifyContent: 'center', alignItems: 'center'
    },
    circlePhoto: {
        width: 35, height: 35,
        borderRadius: 35, borderWidth: 1, borderColor: Color.gray,
        marginRight: 15,
        justifyContent: 'center', alignItems: 'center'
    },
    icon: {
        width: 30, height: 30, marginHorizontal: 10,
        justifyContent: 'center', alignItems: 'center', alignSelf: 'center'
    },
    textRight: {
        flex: 1,
        justifyContent: 'center', alignItems: 'flex-end', alignSelf: 'center'
    }
}

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(ImageContentDown);