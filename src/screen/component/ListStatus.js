import React, { Component } from 'react';
import { Dimensions, Text, View } from 'react-native';
import SearchInput from 'react-native-search-filter';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import Color from '../utility/Color';
import Styles from '../utility/Style';
import NoData from './NoData';
const { width } = Dimensions.get('window')

const KEYS_TO_FILTERS = ['name'];
class ListStatus extends Component {
    constructor(props) {
        super(props)
        this.root = this.props.component.root;
        this.state = {
            text: ''
        }
    }
    searchText(input) {
        this.setState({ text: input })
        if (input.length == 0) {
            this.props.search(this.state.text)
        }
    }
    searchSubmit(input) {
        this.props.search(this.state.text)
    }
    render() {
        return (
            <View style={[Styles.column, { borderTopColor: Color.gray, borderTopWidth: 1, backgroundColor: Color.white, paddingVertical: 5, marginVertical: 10 }]}>
                <View style={{ paddingHorizontal: 20, }}>
                    <View style={styles.search}>
                        <MaterialIcons
                            name="search"
                            color="gray"
                            size={24}
                        />
                        <View style={styles.searchInput}>
                            <SearchInput
                                onSubmitEditing={() => this.searchSubmit()}
                                onChangeText={(input) => { this.searchText(input) }}
                                placeholder="Cari Nama"
                                clearIconViewStyles={{ position: 'absolute', right: 10, top: 10 }}
                                clearIcon={this.state.text !== '' && <MaterialIcons name="close" size={20} />}
                            />
                        </View>
                    </View>
                </View>
                <View style={[Styles.rowBetween, { paddingVertical: 5, paddingHorizontal: 10, }]}>
                    <Text style={[Styles.primaryTextBold, { flex: 1, fontSize: width / 50, textAlign: 'center' }]}>Rank</Text>
                    <Text style={[Styles.primaryTextBold, { flex: 1, fontSize: width / 50, textAlign: 'center' }]}>User</Text>
                    <Text style={[Styles.primaryTextBold, { flex: 1, fontSize: width / 50, textAlign: 'center' }]}>Nilai TPS</Text>
                    <Text style={[Styles.primaryTextBold, { flex: 1, fontSize: width / 50, textAlign: 'center' }]}>Nilai TKA</Text>
                    <Text style={[Styles.primaryTextBold, { flex: 1, fontSize: width / 50, textAlign: 'center' }]}>Total Nilai</Text>
                    <Text style={[Styles.primaryTextBold, { flex: 1, fontSize: width / 50, textAlign: 'center' }]}>Rata-Rata</Text>
                </View>
                {this.props.data.length == 0 && (
                    <NoData><Text>Data Kosong</Text></NoData>
                )}
                {this.props.data.length != 0 &&
                    this.props.data.map((item, index) => {
                        return (
                            <View key={index} style={[Styles.rowBetween, { backgroundColor: index % 2 == 0 ? Color.gray : Color.white, paddingHorizontal: 10, paddingVertical: 10, }]}>
                                <Text style={[Styles.primaryFontText, { flex: 1, fontSize: width / 50, textAlign: 'center' }]}>{item.rank}</Text>
                                <Text style={[Styles.primaryFontText, { flex: 1, fontSize: width / 50, textAlign: 'center' }]}>{item.user.name.length > 27 ? item.user.name.substr(0, 28) + ".." : item.user.name}</Text>
                                <Text style={[Styles.primaryFontText, { flex: 1, fontSize: width / 50, textAlign: 'center' }]}>{item.nilai_tps}</Text>
                                <Text style={[Styles.primaryFontText, { flex: 1, fontSize: width / 50, textAlign: 'center' }]}>{item.nilai_tka}</Text>
                                <Text style={[Styles.primaryFontText, { flex: 1, fontSize: width / 50, textAlign: 'center' }]}>{item.tot_nilai}</Text>
                                <Text style={[Styles.primaryFontText, { flex: 1, fontSize: width / 50, textAlign: 'center' }]}>{item.rata_rata}</Text>
                            </View>
                        )
                    })}
            </View>
        );
    }
}


const styles = {
    search: {
        width: '100%', flexDirection: 'row',
        height: 50,
        alignItems: 'center',
        alignSelf: 'center',
        borderWidth: 1, borderColor: Color.blackOpacity,
        marginHorizontal: 30, marginVertical: 10,
        paddingLeft: 5
    },
    searchInput: {
        flex: 1,
        padding: 5,
        color: Color.black,
    },
}
function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(ListStatus);