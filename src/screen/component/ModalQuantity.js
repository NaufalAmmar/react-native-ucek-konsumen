import React, { Component } from 'react';
import { Modal, StyleSheet, Text, TouchableOpacity, View, Dimensions, TextInput } from 'react-native';
import { connect } from 'react-redux';
import Color from '../utility/Color';
import Styles from '../utility/Style.js';
import Rupiah from '../utility/Rupiah';
import InputWithLabel from './InputWithLabel';
import Button from './Button';
const { height, width } = Dimensions.get('window');

class ModalQuantity extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: true,
            instruksi: '',
            qty: 1,
            layanan: this.props.layanan,
            keranjang: this.props.keranjang,
            total: this.props.total,
            id: this.props.id,
            price: this.props.price,
            index: this.props.index
        };
    }
    componentDidMount() {
        let { layanan, keranjang, total, id, price, index, qty } = this.props
        // console.log('DID', layanan)
        qty = layanan[index].counting != null ? layanan[index].counting : 1
        total = layanan[index].total != null ? layanan[index].total : parseInt(price)
        this.setState({ layanan, keranjang, total: parseInt(total), id, price, index, qty })
        // console.log('DID 2', layanan)
    }
    action() {
        let { layanan, keranjang, total, id, price, index, qty } = this.state
        if (qty == 1) {
            layanan[index].counting = layanan[index].counting != null ? layanan[index].counting : 1
            layanan[index].total = layanan[index].total != null ? layanan[index].total : parseInt(price)
            this.props.action(layanan, total);
        } else {
            this.props.action(layanan, total);
        }
        // console.log('INI', layanan, total)
    }
    close() {
        this.setState({ modalVisible: false })
    }
    plus() {
        let { layanan, keranjang, total, id, price, index, qty } = this.state
        this.setState({ layanan, keranjang, total: parseInt(total), id, price, index })
        layanan[index].selected_id = id
        layanan[index].counting = layanan[index].counting != null ? layanan[index].counting : 1
        layanan[index].counting = layanan[index].counting + 1
        layanan[index].total = layanan[index].total != null ? layanan[index].total : parseInt(price)
        layanan[index].total = layanan[index].total + parseInt(price)
        qty = layanan[index].counting
        total = parseInt(total) + parseInt(price)
        // console.log(layanan[index])
        keranjang.push(layanan[index])
        this.setState({ layanan: layanan, keranjang, total, qty });
    }
    minus() {
        let { layanan, keranjang, total, id, price, index, qty } = this.state
        this.setState({ layanan, keranjang, total: parseInt(total), id, price, index })

        layanan[index].selected_id = id
        layanan[index].counting = layanan[index].counting - 1
        layanan[index].total = layanan[index].total != null ? layanan[index].total : parseInt(price)
        layanan[index].total = layanan[index].total - parseInt(price)
        total = layanan[index].total
        qty = layanan[index].counting
        // console.log(layanan[index])
        if (qty == 0) {
            layanan[index].total = 0
            layanan[index].counting = 0
            this.setState({ layanan: layanan, keranjang, total, qty });
        }
        this.setState({ layanan: layanan, keranjang, total, qty });
    }
    render() {
        let capster = this.props.dataCapster
        return (
            <Modal
                animationType="slide"
                visible={this.props.visible == null ? this.state.modalVisible : this.props.visible}
                transparent={true}
                onRequestClose={() => { this.setState({ modalVisible: !this.state.modalVisible }) }}
            >
                <View style={styles.modalBackground} />

                <View style={styles.modal}>

                    <View style={Styles.rowBetween}>
                        <View>
                            <Text style={styles.title}>{this.props.title.toUpperCase()}</Text>
                            <Text style={styles.description}>{this.props.description}</Text>
                        </View>
                        <Text style={styles.title}>{Rupiah.format(this.props.price)}</Text>
                    </View>

                    {/* <View style={{ flex: 1, paddingHorizontal: 20, paddingVertical: 10, backgroundColor: Color.white, borderTopWidth: 1, borderBottomWidth: 1, borderColor: Color.primary }} >
                                <InputWithLabel
                                    label={"Instruksi"}
                                    // styleContainer={{ paddingLeft: 20 }}
                                    labelColor={Color.black}
                                    placeholder="Tidak Wajib diisi"
                                    returnKeyType={"next"}
                                    // ref={(input) => this.password = input}
                                    // onSubmitEditing={() => this.password.focus()}
                                    onChangeText={instruksi => { this.setState({ instruksi }) }}
                                    // style={}
                                    defaultValue={this.state.instruksi}
                                // underlineColorAndroid={}
                                />
                            </View> */}
                    <View style={[Styles.row, { justifyContent: 'center', alignItems: 'center', marginVertical: 5 }]}>
                        <Button
                            disabled={this.state.qty == 0 ? true : false}
                            backgroundColor={this.state.qty == 0 ? Color.grayFill : Color.white}
                            radiusContainer={5}
                            onPress={() => this.minus()}
                            iconSize={20}
                            iconRight={false}
                            iconLeft={true}
                            materialCommunityIconLeftName={"minus"}
                            labelColor={Color.primary}
                            justifyContent={"center"}
                            height={35}
                            containerStyle={{ marginVertical: 5, width: 35 }}
                        />
                        <Text style={[Styles.whiteText, { marginHorizontal: 20, fontSize: 15 }]}>{this.state.qty}</Text>
                        <Button
                            backgroundColor={Color.white}
                            radiusContainer={5}
                            onPress={() => this.plus()}
                            iconSize={20}
                            iconRight={false}
                            iconLeft={true}
                            materialCommunityIconLeftName={"plus"}
                            labelColor={Color.primary}
                            justifyContent={"center"}
                            height={35}
                            containerStyle={{ marginVertical: 5, width: 35 }}
                        />
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', marginTop: 10, marginBottom: 5 }}>

                        <TouchableOpacity style={{ padding: 5, borderRadius: 3, height: 40 }} onPress={this.props.onPressClose}>
                            <Text style={styles.textClose}>{this.props.textClose}</Text>
                        </TouchableOpacity>
                        <View style={{ width: 20 }} />
                        <TouchableOpacity style={{ flex: 1, marginRight: 10, backgroundColor: Color.primary, padding: 5, borderRadius: 3, height: 40 }} onPress={() => this.action()}>
                            <Text style={[styles.textAction]}>Tambahkan Ke keranjang - {Rupiah.format(this.state.total)}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                {/* </TouchableOpacity> */}
            </Modal >
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    modalBackground: {
        flex: 1,
        backgroundColor: 'rgba(255,255,255,0.8)',
        // alignItems: 'flex-end',
        justifyContent: 'flex-end'
    },
    modal: {
        width,
        height: width / 1.7,
        // marginHorizontal: 20,
        // paddingHorizontal: 10,
        backgroundColor: Color.primaryFont,
        padding: 5,
        borderTopStartRadius: 20,
        borderTopEndRadius: 20,
        // position: 'absolute',
        // bottom:0
    },
    button: {
        // flex: 1,
        backgroundColor: Color.primary,
        padding: 5,
        // justifyContent: 'center', 
        alignItems: 'center'
    },
    title: {
        paddingLeft: 8,
        padding: 6,
        fontSize: 16, fontWeight: 'bold',
        color: Color.white,
        textAlign: 'left'
    },
    description: {
        paddingLeft: 16,        
        fontSize: 12,
        color: Color.white,
        textAlign: 'left'
    },
    priceTotal: {
        padding: 5,
        fontSize: 15, fontWeight: 'bold',
        color: Color.white,
        // marginVertical: 15,
        textAlign: 'center'
    },
    text: {
        fontSize: 13,
        color: Color.white,
        textAlign: 'left',
        // backgroundColor: Color.secondaryButton,
        padding: 15,
        borderRadius: 3,
    },
    action: {
        marginVertical: 5, marginHorizontal: 10,
        flex: 1,
    },
    textAction: {
        padding: 5,
        fontSize: 12,
        color: Color.white,
        textAlign: 'center'
    },
    textClose: {
        fontSize: 12,
        padding: 5,
        // backgroundColor: Color.redFill,
        color: Color.white,
        textAlign: 'center'
    }
});
function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(ModalQuantity);