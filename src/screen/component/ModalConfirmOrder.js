import React, { Component } from 'react';
import { Modal, StyleSheet, Text, TouchableOpacity, View, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import Color from '../utility/Color';
import CapsterContent from './CapsterContent';
const { height, width } = Dimensions.get('window');

class ModalConfirmOrder extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: true,
        };
    }
    action() {
        this.setState({ modalVisible: false })
        this.props.action();
    }
    close() {
        this.setState({ modalVisible: false })
    }
    render() {
        let capster = this.props.dataCapster
        return (
            <Modal
                animationType="slide"
                visible={this.props.visible == null ? this.state.modalVisible : this.props.visible}
                transparent={true}
                onRequestClose={() => { this.setState({ modalVisible: !this.state.modalVisible }) }}
            >
                {/* <TouchableOpacity onPress={() => {  this.props.onPressClose == null ? this.setState({ modalVisible: false }) : this.props.onPressClose() }} style={styles.modalBackground} > */}
                <View style={styles.modalBackground} >

                    <View style={styles.modal}>
                        {capster != null && (
                            <CapsterContent
                                styleContainer={{ position: 'absolute', alignSelf: 'center', top:10 }}
                                onPress={null}
                                backgroundColor={'transparent'}
                                nameTextColor={Color.white}
                                sourceImage={capster.pic_thumb_url}
                                imageSize={'100%'}
                                radiusImage={10}
                                name={capster.name}
                                cutName={true}
                                textAlign="center"
                                rateTextBackColor={Color.primary}
                                rateText={capster.rating}
                                rateJustify={"center"}
                                jarakText={"0.5 km"}
                                jarakTextColor={Color.white}
                            />
                        )}
                        <View style={{ flex: 1, position: 'absolute', bottom: 10, alignSelf: 'center', width:'100%' }}>
                            <View style={{ width: '100%', height: 1, backgroundColor: Color.white }} />
                            <Text style={styles.title}>{this.props.title}</Text>
                            <View style={{ width: '100%', height: 1, backgroundColor: Color.white }} />

                            <Text style={styles.text}>{this.props.text}</Text>
                            {/* <View style={styles.action}> */}
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', marginTop: 10, marginBottom: 5 }}>

                                <TouchableOpacity style={{ flex: 1, padding: 5, borderRadius: 3, height: 40 }} onPress={() => this.props.onPressClose == null ? this.setState({ modalVisible: false }) : this.props.onPressClose()}>
                                    <Text style={styles.textClose}>{this.props.textClose}</Text>
                                </TouchableOpacity>
                                <View style={{ width: 20 }} />
                                <TouchableOpacity style={{ flex: 1, backgroundColor: Color.primary, padding: 5, borderRadius: 3, height: 40 }} onPress={() => this.action()}>
                                    <Text style={[styles.textAction]}>{this.props.textAction}</Text>
                                </TouchableOpacity>
                                {/* </View> */}
                            </View>
                        </View>
                    </View>
                </View>
                {/* </TouchableOpacity> */}
            </Modal >
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    modalBackground: {
        flex: 1,
        backgroundColor: 'rgba(255,255,255,0.8)',
        paddingVertical: 150,
        // alignItems: 'center', justifyContent: 'center'
    },
    modal: {
        flex: 1,
        marginHorizontal: 20,
        paddingHorizontal: 20,
        backgroundColor: Color.primaryFont,
        padding: 5,
        borderRadius: 20
    },
    button: {
        // flex: 1,
        backgroundColor: Color.primary,
        padding: 5,
        // justifyContent: 'center', 
        alignItems: 'center'
    },
    title: {
        paddingLeft: 10,
        padding: 5,
        fontSize: 15, fontWeight: 'bold',
        color: Color.white,
        marginVertical: 15,
        textAlign: 'center'
    },
    text: {
        fontSize: 13,
        color: Color.white,
        textAlign: 'left',
        // backgroundColor: Color.secondaryButton,
        padding: 15,
        borderRadius: 3,
    },
    action: {
        marginVertical: 5, marginHorizontal: 10,
        flex: 1,
    },
    textAction: {
        padding: 5,
        fontSize: 12,
        color: Color.white,
        textAlign: 'center'
    },
    textClose: {
        fontSize: 12,
        padding: 5,
        // backgroundColor: Color.redFill,
        color: Color.white,
        textAlign: 'center'
    }
});
function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(ModalConfirmOrder);