import React, { Component } from 'react';
import { Dimensions, Image, Text, TouchableOpacity, View } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import Color from '../utility/Color.js';
import Styles from '../utility/Style.js';
const { width } = Dimensions.get('window')

class ImageContent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            show: false,
            detailText: ''
        }
        this.root = this.props.component.root;
    }
    componentDidMount() {
        let detailText = ''
        this.props.detailArray != null && (
            this.props.detailArray.map((item, index) => {
                detailText = detailText + item.judul + (index + 1 == this.props.detailArray.length ? ". " : ", ")
            })
        )
        // console.log(detailText)
        this.setState({ detailText })
    }
    render() {
        let { limitString } = this.state
        return (
            <View style={{ flex: 1 }}>
                <TouchableOpacity activeOpacity={0.8} style={[styles.box, this.props.styleContainer, { backgroundColor: this.props.backgroundColor != null ? this.props.backgroundColor : Color.white }]} onPress={this.props.onPress}>
                    <View style={[Styles.center, { marginRight: 8 }]}>
                        {this.props.sourceImage != null && (
                            <Image
                                // source={require('../../../img/background.jpg')}
                                source={this.props.sourceImage}
                                style={[styles.circlePhoto, { width: this.props.imageSize, height: this.props.imageSize, borderRadius: this.props.radiusImage != null ? this.props.radiusImage : this.props.imageSize }]}
                            />
                        )}

                    </View>
                    {this.props.itemCountVisible != false && this.props.itemCountVisible != null && (
                        <View style={{ backgroundColor: Color.primary, width: 24, height: 24, borderRadius: 24, justifyContent: 'center', alignItems: 'center', marginRight: 5 }}>
                            <Text style={[Styles.whiteTextBold, { textAlign: 'center', fontSize: 12, textAlign: this.props.itemCountAlign, color: this.props.itemCountColor != null ? this.props.itemCountColor : Color.white }]}>{this.props.itemCount + "x"}</Text>
                        </View>
                    )}
                    <View style={Styles.rowBetween}>
                        <View>
                            {this.props.name != null && (
                                <Text style={[Styles.whiteTextBold, { fontSize: 16, textAlign: this.props.textAlign, color: this.props.nameTextColor != null ? this.props.nameTextColor : Color.white }]}>{this.props.name.length >= Math.round(width / 7) ? this.props.name.substring(0, Math.round(width / 7)) + ".." : this.props.name}</Text>
                            )}
                            {this.props.rateText != null && (
                                <View style={{ width: 40, marginVertical: 2, paddingHorizontal: 5, alignItems: 'center', flexDirection: 'row', backgroundColor: this.props.rateTextBackColor, borderRadius: 10, }}>
                                    <Ionicons name="ios-star"
                                        color={Color.white}
                                        size={10}
                                        style={{ alignSelf: 'center', marginRight: 3 }}
                                    />
                                    <Text style={[Styles.whiteText, { fontSize: 10, width: 40, textAlign: 'center', textAlign: this.props.textAlign, color: this.props.rateTextColor != null ? this.props.rateTextColor : Color.white }]}>{this.props.rateText}</Text>
                                </View>
                            )}
                            {this.props.childText != null && (
                                <Text style={[Styles.whiteText, { fontSize: 12, textAlign: this.props.textAlign, color: this.props.childTextColor != null ? this.props.childTextColor : Color.white }]}>{this.props.childText.length >= Math.round(width / 8) ? this.props.childText.substring(0, Math.round(width / 8)) + ".." : this.props.childText}</Text>
                            )}
                            {this.props.otherText != null && (
                                <Text style={[Styles.blackText, { fontSize: 10, textAlign: this.props.textAlign, color: this.props.otherTextColor != null ? this.props.otherTextColor : Color.grayDarkFill }]}>{this.props.otherText.length >= Math.round(width / 9) ? this.props.otherText.substring(0, Math.round(width / 9)) + ".." : this.props.otherText}</Text>
                            )}



                            {this.props.contact != null && (
                                <View style={styles.contact}>
                                    <Ionicons name="md-person"
                                        color={Color.white}
                                        size={12}
                                        style={{ alignSelf: 'center', marginRight: 3 }}
                                    />
                                    <Text style={[Styles.whiteText, { fontSize: 12 }]}>{this.props.contact.substr(0, 7)}..</Text>
                                </View>
                            )}
                        </View>
                        {this.props.iconRight == true && (
                            <View style={styles.icon}>
                                {this.props.iconRightName != null && (
                                    <Ionicons name={this.props.iconRightName}
                                        color={this.props.iconColor != null ? this.props.iconColor : Color.white}
                                        size={23}
                                        style={{ alignSelf: 'center' }}
                                    />
                                )}
                                {this.props.materialIconRightName != null && (
                                    <MaterialIcons name={this.props.materialIconRightName}
                                        color={this.props.iconColor != null ? this.props.iconColor : Color.white}
                                        size={25}
                                        style={{ alignSelf: 'center' }}
                                    />
                                )}
                            </View>
                        )}
                        {this.props.textRight == true && (
                            <View style={styles.textRight}>
                                {this.props.textRightValue != null && (
                                    <View style={Styles.rowCenter}>
                                        <Text style={[Styles.whiteText, { fontWeight: this.props.textRightWeight, padding: this.props.textRightBackground != null ? 5 : 0, backgroundColor: this.props.textRightBackground != null ? this.props.textRightBackground : 'transparent', color: this.props.textRightColor != null ? this.props.textRightColor : Color.black, fontSize: 12 }]}>{this.props.textRightValue.length >= Math.round(width / 28) ? this.props.textRightValue.substring(0, Math.round(width / 28)) + ".." : this.props.textRightValue}</Text>
                                        {this.props.circleRight == true && (
                                            <View style={{ marginLeft: 4, borderRadius: 16, width: 16, height: 16, backgroundColor: this.props.textRightColor != null ? this.props.textRightColor : Color.white }} />
                                        )}
                                    </View>
                                )}
                                {this.props.subChildText != null && (
                                    <Text style={[Styles.blackText, { flexWrap: "wrap", fontSize: 8, textAlign: this.props.textAlign, color: this.props.subChildTextColor != null ? this.props.subChildTextColor : Color.primaryFont }]}>{this.props.subChildText}</Text>
                                )}
                            </View>
                        )}

                    </View>
                </TouchableOpacity>
                <View style={Styles.rowBetween}>
                    {this.state.detailText.length > 0 && (
                        <Text style={[Styles.primaryText, { fontSize: 12, borderBottomWidth: 1, borderBottomColor: Color.grayWhite, padding: 5, backgroundColor: 'transparent', color: Color.grayFill }]}>{this.state.detailText.length >= Math.round(width / 9) ? this.state.detailText.substring(0, Math.round(width / 9)) + ".." : this.state.detailText}</Text>
                    )}
                    {this.props.priceLayanan != null && (
                        <Text style={[Styles.primaryText, { fontSize: 12, borderBottomWidth: 1, borderBottomColor: Color.grayWhite, padding: 5, backgroundColor: 'transparent', color: Color.grayFill }]}>{this.props.priceLayanan.length >= Math.round(width / 7) ? this.props.priceLayanan.substring(0, Math.round(width / 7)) + ".." : this.props.priceLayanan}</Text>
                    )}
                </View>
            </View >
        )
    }
}
const styles = {
    box: {
        flex: 1, flexDirection: 'row',
        backgroundColor: Color.white,
        paddingVertical: 15,
        paddingHorizontal: 10,
        // marginBottom: 2,
        borderColor: Color.whiteOpacity, borderBottomWidth: 1, borderTopWidth: 1,
        justifyContent: 'center', alignItems: 'center'
    },
    contact: {
        width: 70,
        flexDirection: 'row', borderRadius: 5,
        backgroundColor: Color.secondaryButton, padding: 2,
        marginVertical: 2,
        justifyContent: 'center', alignItems: 'center'
    },
    circlePhoto: {
        width: 55, height: 55,
        borderRadius: 10, borderWidth: 1, borderColor: Color.gray,
        justifyContent: 'center', alignItems: 'center'
    },
    icon: {
        width: 30, height: 30, marginHorizontal: 5,
        justifyContent: 'center', alignItems: 'center', alignSelf: 'center'
    },
    textRight: {
        flex: 1,
        justifyContent: 'center', alignItems: 'flex-end', alignSelf: 'center'
    }
}

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(ImageContent);